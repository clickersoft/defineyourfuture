import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NavController, ToastController, ModalController } from '@ionic/angular';
import { LocalStorageService } from '../auth/local-storage.service';
import { Account } from '../shared/models/account';
import { Plugins } from '@capacitor/core';
import { FacebookLoginResponse } from '@rdlabo/capacitor-facebook-login';
import { UsersService } from '../services/users.service';
import { OAuthService } from 'angular-oauth2-oidc';
import { HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { ContentViewPage } from '../content-view/content-view.page';
import { GsCountry } from 'src/app/shared/models/gs-country';
import { SharedApiService } from 'src/app/services/shared-api.service';
import { ClientRide } from '../shared/models/client-ride';
import { NavExtrasService } from '../services/nav-extras.service';

declare var FB: any;
//import "@codetrix-studio/capacitor-google-auth";
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  tourId : string
  pageRedirect : string
  account: Account
  error: string;
  isRegisterView:boolean;
  loadingOperation = new BehaviorSubject<boolean>(false);
  countries:GsCountry[] = [];
  ride:ClientRide
  constructor(protected navCtrl: NavController,protected router:Router,
    private toastController:ToastController,
    private localStorageService: LocalStorageService,private activatedRoute:ActivatedRoute,
    private usersService: UsersService,
    private oauthService: OAuthService,
    private translate: TranslateService,
    private modalController: ModalController,
    private sharedApiService: SharedApiService,
    private navExtrasService: NavExtrasService)
  { 
    localStorageService.clearAccount();
    this.tourId = this.activatedRoute.snapshot.paramMap.get('id');
    this.ride = this.navExtrasService.getExtras2() as ClientRide;
    this.pageRedirect = this.activatedRoute.snapshot.paramMap.get('pageRedirect');
    this.account = {} as Account

    this.sharedApiService.GetCountries().subscribe(res=>{
      this.loadingOperation.next(false);
      this.countries = res;
    },err=>{
      this.loadingOperation.next(false);
    });
    /*
     // Load the SDK asynchronously
     (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "https://connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    window.fbAsyncInit = function() {
      FB.init({
        appId: '377587716460266',
        cookie: true, // enable cookies to allow the server to access the session
        xfbml: true, // parse social plugins on this page
        version: 'v2.8' // use graph api version 2.8
      });
    };
    */
  }
  ngOnInit() {
  }
 
  async loginFacebook(){
    const FACEBOOK_PERMISSIONS = ['email'];
    const result = await <FacebookLoginResponse>Plugins.FacebookLogin.login({ permissions: FACEBOOK_PERMISSIONS });

    console.log(result);
    if (result.accessToken) {
      // Login successful.
      //userId
      this.loadingOperation.next(true);
      this.error = null;
      this.oauthService.customQueryParams = {
        "grant_type":"facebookAuth",
        "id_token":result.accessToken.token
      };
      this.oauthService.fetchTokenUsingPasswordFlow("", "").then((resp) => {
        //console.log("success");
            // Loading data about the user
            console.log("Loading data about the user");
            return this.oauthService.loadUserProfile();
  
      }).then(() => {
        console.log("Using the loaded user data");
       // console.log("success");
            // Using the loaded user data
            let claims = this.oauthService.getIdentityClaims();
            
            //if (claims) console.debug('given_name', claims.given_name);
           // console.log(JSON.stringify(claims));
            //this.router.navigate(['/']);
            this.nextPage();
      }).catch((reason)=>{
        this.error = "Username or password is incorrect"
        console.log("Error");
        console.log(JSON.stringify(reason));
      }).finally(()=>{
        this.loadingOperation.next(false);
      });

      //this.usersService.facebookLogin(result.accessToken.token).subscribe(res=>{
     //   console.log('fb_login:' + res);
     // });
      //this.presentToast(`Facebook access token is ${result.accessToken.token}`,'success');
      
      console.log(`Facebook access token is ${result.accessToken.token}`);
    } else {
      // Cancelled by user.
      this.presentToast('Cancelled by user.','danger');
    }
  }
  async loginGoogle(){
    let googleUser = await Plugins.GoogleAuth.signIn();
    if (googleUser && googleUser.authentication && googleUser.authentication.idToken) {
      // Login successful.
      this.loadingOperation.next(true);
      this.error = null;
      this.oauthService.customQueryParams = {
        "grant_type":"googleAuth",
        "id_token":googleUser.authentication.idToken
      };
      this.oauthService.fetchTokenUsingPasswordFlow("", "").then((resp) => {
        //console.log("success");
            // Loading data about the user
            console.log("Loading data about the user");
            return this.oauthService.loadUserProfile();
  
      }).then(() => {
        console.log("Using the loaded user data");
       // console.log("success");
            // Using the loaded user data
            
            //if (claims) console.debug('given_name', claims.given_name);
           // console.log(JSON.stringify(claims));
           this.nextPage();
      }).catch((reason)=>{
        this.error = this.translate.instant('email_or_password_is_incorrect')
        console.log("Error");
        console.log(JSON.stringify(reason));
      }).finally(()=>{
        this.loadingOperation.next(false);
      });
     // this.presentToast(`Google access token is ${googleUser.authentication.idToken}`,'success');
      console.log(`Google access token is ${googleUser.authentication.idToken}`);
    } else {
      // Cancelled by user.
      this.presentToast('Cancelled by user.','danger');
    }
    
    //const credential = auth.GoogleAuthProvider.credential(googleUser.authentication.idToken);
    //return this.afAuth.auth.signInAndRetrieveDataWithCredential(credential);
  }
  nextPage(){
    if(this.isRegisterView){
      this.presentToast(this.translate.instant('registered_successfully'),'success')
    }else{
      this.presentToast(this.translate.instant('logged_in_successfully'),'success')
    }
    
    if(this.oauthService.hasValidAccessToken()){
    
    }
   
    if(this.tourId && this.tourId != '-1'){
      if(this.pageRedirect && this.pageRedirect == '2'){
        this.navCtrl.navigateRoot(`/tour-feature/${this.tourId}`)
      }else{
        this.navCtrl.navigateRoot(`/tour-pax/${this.tourId}`)
      }
     }else if(this.ride && this.tourId == '-1'){
        this.navExtrasService.setExtras2(this.ride);
        this.router.navigateByUrl(`/tour-reserve`)
     }else {
      let claims = this.oauthService.getIdentityClaims()
      if(claims){
        switch(claims['role'] || '--'){
          case 'admin':
            this.router.navigate(['/admin/accounts']);
            console.log("Admin")
            return
            case 'driver':
              this.router.navigate(['/admin/accounts']);
            console.log("driver")
            return
            default:
              this.navCtrl.navigateRoot('/dashboard')
            return
        }
      }
     
     }
  }
  login(){

    if(!this.account.Email || !this.account.Password){
      this.presentToast(this.translate.instant('please_enter_the_required_fields'),'danger');
      return;
    }

    if(this.isRegisterView){
      if(!this.account.FullName){
        this.presentToast(this.translate.instant('please_enter_your_name'),'danger');
        return;
      }
      this.oauthService.customQueryParams = {
        "grant_type":"registerAuth",
        "name":this.account.FullName,
        "mobile": this.account.Mobile,
        "country":this.account.CountryCode,
      };
    }else{
      //Login
      this.oauthService.customQueryParams = {
        "grant_type":"password",
      };
    }

    this.loadingOperation.next(true);
    //this.error = null;
    this.oauthService.fetchTokenUsingPasswordFlow(this.account.Email, this.account.Password).then((resp) => {
      //console.log("success");
          // Loading data about the user
          return this.oauthService.loadUserProfile();

    }).then(() => {
     // console.log("success");
          // Using the loaded user data
         this.nextPage();
        // this.localStorageService.getAccountBehaviorSubject().next(this.localStorageService.getAccount());
          //if (claims) console.debug('given_name', claims.given_name);
         // console.log(JSON.stringify(claims));
          //this.router.navigate(['']);
    }).catch((reason)=>{
      if(this.isRegisterView){
        if(reason.status == 400){
          if(reason.error.error_description == "username already exists"){
            this.presentToast(this.translate.instant('this_email_already_registered'),"danger");
          }else{
            this.presentToast(this.translate.instant('please_enter_the_required_fields'),"danger");
          }
         
        }else{
          this.presentToast(this.translate.instant('something_went_wrong_please_try_again'),"danger");
        }
      }
      else{
        this.presentToast(this.translate.instant('email_or_password_is_incorrect'),"danger");
      }
      console.log("Error");
      console.log(JSON.stringify(reason));
    }).finally(()=>{
      this.loadingOperation.next(false);
    });
  }
  async presentToast(msg:string, color: string) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
      color: color
    });
    toast.present();
  }
  backClick(){
    if(this.isRegisterView){
      this.isRegisterView = false;
      return;
    }
    this.navCtrl.navigateBack('dashboard');
  }

  async aboutUsClick(){
    const modal = await this.modalController.create({
      component : ContentViewPage,
      componentProps : {
        event: event
      }
    });
    modal.present();
  }
}
/*
export class LoginTabPage extends LoginPage {

  ngOnInit() {
    this.navCtrl.pop();
  }
}
*/