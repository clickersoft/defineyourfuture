import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController, ModalController } from '@ionic/angular';
import { NavExtrasService } from '../services/nav-extras.service';
import { Tour } from '../shared/models/tour';
import { ReplaySubject, BehaviorSubject } from 'rxjs';
import { ToursService } from '../services/tours.service';
import { OAuthService } from 'angular-oauth2-oidc';
import { LocalStorageService } from '../auth/local-storage.service';
import { MapViewLocationPage } from '../map-view-location/map-view-location.page';
import { ClientRide } from '../shared/models/client-ride';
import { RidesService } from '../services/rides.service';

@Component({
  selector: 'app-tour',
  templateUrl: './my-order.page.html',
  styleUrls: ['./my-order.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MyOrderPage implements OnInit {
  rideId:string
  tour: Tour;
  ride: ClientRide;
  loadingOperation = new BehaviorSubject<boolean>(false);
  selectedTab = new BehaviorSubject<string>("order_info");
  tourReplaySubject: ReplaySubject<Tour> = new ReplaySubject(1);
  constructor(private activatedRoute: ActivatedRoute,private router:Router,private navCtrl:NavController,private navExtrasService: NavExtrasService
    ,private ridesService:RidesService,
    private oauthService: OAuthService,private localStorageService: LocalStorageService,
    private modalController: ModalController) {
      this.rideId = this.activatedRoute.snapshot.paramMap.get('id');
      if(!this.rideId){
        this.navCtrl.navigateBack('dashboard');
        return;
      }
      if(this.navExtrasService.getExtras()){
        this.ride = this.navExtrasService.getExtras() as ClientRide;
      }

    if(this.ride && this.ride.RideId && this.ride.Tour){
      this.tour = this.ride.Tour;
      this.tourReplaySubject.next(this.tour);
    }else{
      this.tour = {} as Tour;
      this.ride = {} as ClientRide;

      if(this.rideId){
        this.loadingOperation.next(true);
        this.ridesService.MyRide(this.rideId).subscribe(res=>{
          this.ride = res;
          if(res){
            this.tour = res.Tour;
          }
          
          this.loadingOperation.next(false);
          this.tourReplaySubject.next(this.tour);
        },err=>{
          this.loadingOperation.next(false);
          this.tourReplaySubject.next(null);
  
        }); 
      }
    }
   }
  backClick(){
    this.navCtrl.navigateBack('dashboard');
  }
  bookTour(){
    let navUrl = `/tour-pax/${this.tour.TourId}`
    if(!this.oauthService.hasValidAccessToken()){
      navUrl = `/login/${this.tour.TourId}`
    }
   
    this.navExtrasService.setExtras(this.tourReplaySubject);
    this.router.navigateByUrl(navUrl)
  }
  ngOnInit() {
  }
  tabChanged(ev: any){
    this.selectedTab.next(ev.detail.value);
  }
  async openMapPage(name,lat,lng){
    const modal = await this.modalController.create({
      component : MapViewLocationPage,
      componentProps : {
        title: name,
        lat: lat,
        lng: lng
      }
    });
   
    modal.present();
  
 
  // this.router.navigateByUrl(`/map-view-location/${name}/${lat}/${lng}`)
  }
}
