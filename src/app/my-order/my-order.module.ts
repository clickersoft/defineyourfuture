import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MyOrderPage } from './my-order.page';
import { MomentModule } from 'ngx-moment';
import { TranslateModule } from '@ngx-translate/core';
import { MapViewLocationPageModule } from '../map-view-location/map-view-location.module';
import { NgxQRCodeModule } from 'ngx-qrcode2'
const routes: Routes = [
  {
    path: '',
    component: MyOrderPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MomentModule,
    TranslateModule,
    MapViewLocationPageModule,
    NgxQRCodeModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MyOrderPage]
})
export class MyOrderPageModule {}
