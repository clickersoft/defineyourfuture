import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NavController, ToastController } from '@ionic/angular';
import { LocalStorageService } from '../auth/local-storage.service';
import { Account } from '../shared/models/account';


@Component({
  selector: 'app-login-mobile',
  templateUrl: './login-mobile.page.html',
  styleUrls: ['./login-mobile.page.scss'],
})
export class LoginMobilePage implements OnInit {
  tourId : string
  constructor(protected navCtrl: NavController,protected router:Router,
    private toastController:ToastController,
    private localStorageService: LocalStorageService,private activatedRoute:ActivatedRoute)
  { 
    localStorageService.clearAccount();
    this.tourId = this.activatedRoute.snapshot.paramMap.get('id');
  }
  mobileNumber:string;
  ngOnInit() {
  }
  
  async login(){
    if(!this.mobileNumber || this.mobileNumber.length != 10){
      this.presentToast('Wrong Mobile Number!','danger');
      return;
    }
    this.router.navigate(['verification'],{
      queryParams: {
        mobile: this.mobileNumber,
        tourId: this.tourId || ''
      }
    })
  }
  async presentToast(msg:string, color: string) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
      color: color
    });
    toast.present();
  }
}
/*
export class LoginTabPage extends LoginPage {

  ngOnInit() {
    this.navCtrl.pop();
  }
}
*/