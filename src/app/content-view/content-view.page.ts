import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { SharedApiService } from '../services/shared-api.service';
import { AppContent } from '../shared/models/app-content';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-content-view',
  templateUrl: './content-view.page.html',
  styleUrls: ['./content-view.page.scss'],
})
export class ContentViewPage implements OnInit {

  title: string
  bodyText: string
  loadingOperation = new BehaviorSubject<boolean>(false);
  constructor(private modalController: ModalController,private params: NavParams,private translate: TranslateService,
    private sharedApiService: SharedApiService) 
  { 
    

    if(this.params.get("page")){
      this.loadingOperation.next(true);
      this.sharedApiService.GetAppContent(this.params.get("page")).subscribe(res=>{
        this.loadingOperation.next(false);
        this.title = (res as AppContent).AppContentTitle;
        this.bodyText = (res as AppContent).AppContentText;
      },err=>{
        this.loadingOperation.next(false);
      });
    }else{
      this.title = this.translate.instant('about_us')
      this.bodyText = this.translate.instant('about_us_content')
    }
  }

  ngOnInit() {
  }
  backClick(){
    this.modalController.dismiss();
  }
}
