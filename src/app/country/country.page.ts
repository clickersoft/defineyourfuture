import { Component, OnInit, ViewChild, ElementRef, Renderer2, AfterViewInit, Inject, Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Search } from '../shared/models/search';
import { IonContent, ToastController } from '@ionic/angular';
import { DOCUMENT } from '@angular/common';
import { BehaviorSubject, Observable, ReplaySubject } from 'rxjs';
import { AccountsService } from '../services/accounts.service';
import { Account } from '../shared/models/account';
import { NavExtrasService } from '../services/nav-extras.service';
import * as moment from 'moment';
import { GsCountry } from '../shared/models/gs-country';
import { SharedApiService } from '../services/shared-api.service';
import { Univ } from '../shared/models/univ';

@Injectable({
  providedIn: 'root'
})
@Component({
  selector: 'app-country',
  templateUrl: './country.page.html',
  styleUrls: ['./country.page.scss'],
})
export class CountryPage implements OnInit,AfterViewInit {
  selectedTab = 'details';
  itemsList: Univ[] = [];
  loadingOperation = new BehaviorSubject<boolean>(false);

  constructor(@Inject(DOCUMENT) private document: Document,
  private toastController: ToastController,
  private sharedApiService: SharedApiService,
  private router: Router) 
  {
    this.loadingOperation.next(true);
    this.sharedApiService.GetUniversities().subscribe(res=>{
      this.itemsList = res;
      this.loadingOperation.next(false);
    },err=>{
      this.loadingOperation.next(false);
    }); 
  }

  selectedItem: Univ;
  createItem(){
    this.selectedItem = {
      
    } as Univ
  }
  submitItem(){
    
    const submitedItem = this.selectedItem;
    let msg : string;
    if(submitedItem.UniversityId){
      msg = 'Edited Successfully'
    }else{
      msg = 'Created Successfully'
      this.itemsList = [...this.itemsList,submitedItem];
    }
    

    this.loadingOperation.next(true);
    this.sharedApiService.createOrUpdateUniv(submitedItem)
    .subscribe(res=>{
      this.loadingOperation.next(false);
      this.presentToast(msg,'success')
      this.selectedItem = null;
      submitedItem.UniversityId = res;
    },err=>{
      let errMsg: string;
      if(err){
        errMsg = err.error;
        console.log(err.error);
      }
      if(errMsg){
        errMsg = `Error, ${errMsg}`;
      }else{
        errMsg = 'Error, Try again later';
      }
      this.presentToast(errMsg,'danger')
      this.loadingOperation.next(false);
      if(!submitedItem.UniversityId){
        this.itemsList = this.itemsList.filter(event => event !== submitedItem);
      }
    });    
    
  }
  discardItem(){
    this.selectedItem = null;
  }
  selectItem(selected){
    this.selectedItem = selected;
  }
  deleteItem(){
    const deletedItem = this.selectedItem;
    this.loadingOperation.next(true);

    this.itemsList = this.itemsList.filter(event => event !== deletedItem);

    this.sharedApiService.deleteUniv(deletedItem)
    .subscribe(res=>{
      this.presentToast('Deleted Successfully','success')
      this.loadingOperation.next(false);
      this.selectedItem = null;
    },err=>{
      let errMsg: string;
      if(err){
        errMsg = err.error;
        console.log(err.error);
      }
      if(errMsg){
        errMsg = `Error, ${errMsg}`;
      }else{
        errMsg = 'Error, Try again later';
      }
      this.presentToast(errMsg,'danger')
      this.loadingOperation.next(false);
      this.itemsList = [...this.itemsList,deletedItem];
    });    
  }
  ngAfterViewInit() {
  }

 
  
  ngOnInit() {
  }
  async presentToast(msg:string, color: string) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
      color: color
    });
    toast.present();
  }
}
