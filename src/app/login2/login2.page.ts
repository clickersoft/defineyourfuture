import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NavController, ToastController } from '@ionic/angular';
import { LocalStorageService } from '../auth/local-storage.service';
import { Account } from '../shared/models/account';
import { Plugins } from '@capacitor/core';
import { FacebookLoginResponse } from '@rdlabo/capacitor-facebook-login';
//import "@codetrix-studio/capacitor-google-auth";
const { FacebookLogin } = Plugins;
@Component({
  selector: 'app-login2',
  templateUrl: './login2.page.html',
  styleUrls: ['./login2.page.scss'],
})
export class Login2Page implements OnInit {
  tourId : string
  constructor(protected navCtrl: NavController,protected router:Router,
    private toastController:ToastController,
    private localStorageService: LocalStorageService,private activatedRoute:ActivatedRoute)
  { 
    localStorageService.clearAccount();
    this.tourId = this.activatedRoute.snapshot.paramMap.get('id');
    this.account = {} as Account
  }
  mobileNumber:string;
  ngOnInit() {
  }
  account: Account
  
  async loginFacebook(){
    const FACEBOOK_PERMISSIONS = ['email', 'user_birthday', 'user_photos', 'user_gender'];
    const result = await <FacebookLoginResponse>FacebookLogin.login({ permissions: FACEBOOK_PERMISSIONS });

    if (result.accessToken) {
      // Login successful.
      this.presentToast(`Facebook access token is ${result.accessToken.token}`,'success');
      console.log(`Facebook access token is ${result.accessToken.token}`);
    } else {
      // Cancelled by user.
      this.presentToast('Cancelled by user.','danger');
    }
  }
  async loginGoogle(){
    let googleUser = await Plugins.GoogleAuth.signIn();
    if (googleUser && googleUser.authentication && googleUser.authentication.idToken) {
      // Login successful.
      this.presentToast(`Google access token is ${googleUser.authentication.idToken}`,'success');
      console.log(`Google access token is ${googleUser.authentication.idToken}`);
    } else {
      // Cancelled by user.
      this.presentToast('Cancelled by user.','danger');
    }
    
    //const credential = auth.GoogleAuthProvider.credential(googleUser.authentication.idToken);
    //return this.afAuth.auth.signInAndRetrieveDataWithCredential(credential);
  }
  login(){
    if(!this.mobileNumber || this.mobileNumber.length != 10){
      this.presentToast('Wrong Mobile Number!','danger');
      return;
    }
    this.router.navigate(['verification'],{
      queryParams: {
        mobile: this.mobileNumber,
        tourId: this.tourId || ''
      }
    })
  
  }
  async presentToast(msg:string, color: string) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
      color: color
    });
    toast.present();
  }
}
/*
export class LoginTabPage extends LoginPage {

  ngOnInit() {
    this.navCtrl.pop();
  }
}
*/