import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MapViewLocationPage } from './map-view-location.page';
import { AgmCoreModule } from '@agm/core';

const routes: Routes = [
  {
    path: 'mapView',
    component: MapViewLocationPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    AgmCoreModule.forRoot({
      // please get your own API key here:
      // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en
      apiKey: 'AIzaSyDYQTD3KNoa-vmtvhW8yPdIlT0e9e_--XY'
    })
  ],
  declarations: [MapViewLocationPage]
})
export class MapViewLocationPageModule {}
