import { Component, OnInit } from '@angular/core';
import { MouseEvent } from '@agm/core';
import { NavParams, ModalController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { marker } from '../shared/models/marker';

@Component({
  selector: 'app-map-view-location',
  templateUrl: './map-view-location.page.html',
  styleUrls: ['./map-view-location.page.scss'],
})
export class MapViewLocationPage implements OnInit {
  // google maps zoom level
  zoom: number = 8;
  
  // initial center position for the map
  title: string = "Map";
  lat: number = 51.673858;
  lng: number = 7.815982;n
  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`)
  }
  
  mapClicked($event: MouseEvent) {
    /*
    this.markers.push({
      lat: $event.coords.lat,
      lng: $event.coords.lng,
      draggable: true
    });
    */
  }
  
  markerDragEnd(m: marker, $event: MouseEvent) {
   // console.log('dragEnd', m, $event);
  }
  
  markers: marker[] = [
    /*
	  {
		  lat: 51.673858,
		  lng: 7.815982,
		  label: 'A',
		  draggable: true
	  },
	  {
		  lat: 51.373858,
		  lng: 7.215982,
		  label: 'B',
		  draggable: false
	  },
	  {
		  lat: 51.723858,
		  lng: 7.895982,
		  label: 'C',
		  draggable: true
    }
    */
  ]
  //private modalController: ModalController,private params: NavParams,
  constructor(private modalController: ModalController,private params: NavParams
    ) {
      //private activatedRoute: ActivatedRoute
    //this.title = this.activatedRoute.snapshot.paramMap.get("title");
    //this.lat = +this.activatedRoute.snapshot.paramMap.get("lat");
    //this.lng = +this.activatedRoute.snapshot.paramMap.get("lng");
    
    this.title = this.params.get("title");
    this.lat = +this.params.get("lat");
    this.lng = +this.params.get("lng");
    
    this.markers.push(
      {
        lat: this.lat,
        lng: this.lng,
        label: this.title,
        draggable: false

    } as marker)
   }

  ngOnInit() {
  }

  back(){
    this.modalController.dismiss();
  }

}
