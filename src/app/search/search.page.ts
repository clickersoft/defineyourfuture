import { Component, OnInit, ViewChild, Optional } from '@angular/core';
import { IonInfiniteScroll, ModalController, NavParams, IonSearchbar } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { NavExtrasService } from '../services/nav-extras.service';
import { RidesService } from '../services/rides.service';
import * as moment from 'moment';
import { RideStatuss } from '../shared/enums/ride-statuss.enum';
import { SortModes } from '../shared/enums/sort-modes.enum';
import { SortDirections } from '../shared/enums/sort-directions.enum';

@Component({
  selector: 'app-search',
  templateUrl: 'search.page.html',
  styleUrls: ['search.page.scss']
})

export class SearchPage implements OnInit {
  offset = 0;
  searchList = [];
  searchTxt: string = '';
  view: string;
  loading = new BehaviorSubject<boolean>(false);

  @ViewChild(IonInfiniteScroll,{static : false}) infinite : IonInfiniteScroll;
  @ViewChild(IonSearchbar,{static : false}) searchbar : IonSearchbar; 
  
  constructor(private ridesService : RidesService,private router: Router,
    private navExtrasService: NavExtrasService,
    @Optional() private modalController?: ModalController,@Optional() private params?: NavParams) 
    {
      if(params){
        this.view = params.get("view");
      }
    }
  
  ngOnInit(){
    this.loadSearch();
  }

  dateFrom:moment.Moment = null;
  dateTo:moment.Moment = null;
  rideStatus = RideStatuss.All;
  searchText:string = '';
  rideTypeId:number = 0;
  tourId:string = '';
  carTypeId:number = 0;
  driverId:string = '';
  //fromIndex:number;
  //pageSize:number;
  sortMode = SortModes.CreateDate;
  sortDirection = SortDirections.Descending;
  selectedTab = 'new';
  prevTab = 'new';

  loadSearch(loadMore = false, event?){
    this.loading.next(true);
    if(this.offset == 0 && this.infinite){
      this.infinite.disabled = true;
    }

    this.rideStatus = RideStatuss.New;
    //
    switch(this.selectedTab){
      case 'new':
        break;
        case 'planned':
            this.rideStatus = RideStatuss.Planned;
        break;
        case 'finished':
            this.rideStatus = RideStatuss.Finished;
        break;
    }
    this.ridesService.getAll(this.dateFrom,this.dateTo,this.rideStatus,this.searchText,this.rideTypeId,this.tourId,this.carTypeId,this.driverId,this.offset,20,this.sortMode,this.sortDirection).subscribe(res=>{
      this.infinite.disabled = false;
      this.loading.next(false);
      console.log("hhh");
      if(this.offset > 0){
        this.searchList = [...this.searchList,...res];
      }
      else{
        this.searchList = res;
      }
      this.offset = this.searchList.length;
      if(event){
        event.target.complete();
      }
      if(this.offset > 100 || res.length == 0){
        this.infinite.disabled = true;
      }
    },(err)=>{
      if(err){
        console.log(err);
      }
      this.offset = 0;
      if(this.infinite){
        this.infinite.disabled = false;
      }
      this.loading.next(false);
        //this.errorMessage = "Some thing went wrong!";
    });
  }

  addAccount(){
    this.router.navigateByUrl(`/admin/edit-ride`);
  }
  tabChanged(ev:any){
    if(this.prevTab === this.selectedTab){
      return;
    }
    this.prevTab = this.selectedTab;
    
    if(this.infinite){
      this.infinite.complete();
    }
    
    this.searchList = [];
    this.offset = 0;
    this.loadSearch();
  }
  onSearchClick(searchItem){
    if(searchItem){
      if(this.view){
        if(this.modalController){
          this.modalController.dismiss(searchItem);
        }
      }
      else{
        this.navExtrasService.setExtras(searchItem);
        this.router.navigateByUrl(`/admin/ride/${searchItem.RideId}`);

        console.log(searchItem.FullName);
      }
    }
  }
  onSearchChange(){
   
    if(this.infinite){
      this.infinite.complete();
    }
    
    this.searchList = [];
    this.offset = 0;
    this.loadSearch();
  }
  isSearchBarOppend:boolean;
  toggleSearchBar(isSearchBarOppend:boolean){
    this.isSearchBarOppend = isSearchBarOppend;
    if(isSearchBarOppend){
      this.searchbar.setFocus();  
    }else{
      this.searchTxt = '';
      this.onSearchChange();
    }
  }
}

export class SearchModalPage extends SearchPage{
  
}