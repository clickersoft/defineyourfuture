import { Component, OnInit, Renderer2 } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavExtrasService } from 'src/app/services/nav-extras.service';
import { ToastController } from '@ionic/angular';
import { Observable, BehaviorSubject, ReplaySubject } from 'rxjs';
import { SharedApiService } from 'src/app/services/shared-api.service';
import { Lookup } from 'src/app/shared/models/lookup';
import * as moment from 'moment';
import { LookupTypes } from 'src/app/shared/enums/lookup-types.enum';
import { GsCountry } from 'src/app/shared/models/gs-country';
import { CarType } from 'src/app/shared/models/car-type';
import { Tour } from 'src/app/shared/models/tour';
import { ToursService } from 'src/app/services/tours.service';
import { TourCarPrice } from 'src/app/shared/models/tour-car-price';
import { Capacitor, Plugins, CameraResultType, FilesystemDirectory } from '@capacitor/core';
import { HttpEventType } from '@angular/common/http';
import { GsCity } from 'src/app/shared/models/gs-city';
import { AppSettings } from 'src/app/services/appsettings';
import { TourFeature } from 'src/app/shared/models/tour-feature';
import { Univ } from 'src/app/shared/models/univ';
const { Camera, Filesystem,CameraSource } = Plugins;

@Component({
  selector: 'app-edit-major',
  templateUrl: './edit-major.page.html',
  styleUrls: ['./edit-major.page.scss'],
})
export class EditTourPage implements OnInit {

  tour:Tour
  tourId: string;
  loadingOperation = new BehaviorSubject<boolean>(false);
  loadingOperation2 = new BehaviorSubject<boolean>(false);
  loadingOperation3 = new BehaviorSubject<boolean>(false);
  carTypes:CarType[] = [];
  tourPickups:GsCity[] = [];
  countries:GsCountry[] = [];
  universities:Univ[] = [];
  slideOptsProgressbar = {
    initialSlide: 0,
    spaceBetween:10,
    slidesPerView: 1.6,
    autoplay:true,
  };
  maxBirthDate = moment().format(AppSettings.DATE_MAX_DATE_FORMAT);
  DATE_FORMAT:any
  constructor(private activatedRoute: ActivatedRoute,private rd: Renderer2,
    private toursService: ToursService,
    private sharedApiService: SharedApiService,
    private navExtrasService: NavExtrasService,
    private toastController: ToastController) 
    { 
      this.DATE_FORMAT = AppSettings.DATE_FORMAT
      this.tourId = this.activatedRoute.snapshot.paramMap.get('id');

      this.tour = { PublishDate: moment().toISOString(), Active: true, PaymentMethod: "1", TourType: "1" , CountryCode: "", RequiredFields2: ["Name","ChildCount","Mobile","Email"],TourFeature:[{FeatureName: "Jerusalem"} as TourFeature,{FeatureName: "Tel Aviv"} as TourFeature] } as Tour;
      if(this.tourId){
        if(navExtrasService.getExtras()){
          this.loadingOperation.next(true);
          this.tour = navExtrasService.getExtras() as Tour;
          this.loadingOperation.next(false);
         }else{
          this.loadTour();
         }
      }else{
        //New Tour
        this.tour.TourFeature.forEach(element => {
          element.Active = true;
          element.Price = 0;
          element.OrderNumber = 1;
          element.ShortName = "";
        });
        
      }
       

       this.loadingOperation2.next(true);
       this.sharedApiService.GetCarTypes().subscribe(res=>{
        this.loadingOperation2.next(false);
        this.carTypes = res;
      },err=>{
        this.loadingOperation2.next(false);
      });
      this.sharedApiService.GetCities().subscribe(res=>{
        this.loadingOperation2.next(false);
        this.tourPickups = res;
      },err=>{
        this.loadingOperation2.next(false);
      });
       this.sharedApiService.GetUniversities().subscribe(res=>{
        this.loadingOperation2.next(false);
        this.universities = res;
      },err=>{
        this.loadingOperation2.next(false);
      });
    }

    loadTour(){
      this.loadingOperation.next(true);
      this.toursService.GetTour(this.tourId).subscribe(res=>{
        this.tour = res;
        console.log(this.tour);
        this.loadingOperation.next(false);
      },err=>{
        this.loadingOperation.next(false);
      }); 
    }
    
    submitTour(){
      this.loadingOperation2.next(true);
      this.loadingOperation3.next(true);
      this.tour.imageGroup = "temp";
      if(this.tourId){
        this.toursService.createOrUpdateTour(this.tour).subscribe(res=>{
         this.loadingOperation2.next(false);
         this.loadingOperation3.next(false);
         this.presentToast('Edited Successfully','success')
        },err=>{
          let errMsg: string;
          if(err){
            errMsg = err.error;
            console.log(err.error);
          }
          if(errMsg){
            errMsg = `Error, ${errMsg}`;
          }else{
            errMsg = 'Error, Try again later';
          }
        this.presentToast(errMsg,'danger')
         this.loadingOperation2.next(false);
         this.loadingOperation3.next(false);
        });
      }else{
        this.toursService.createOrUpdateTour(this.tour).subscribe(res=>{
          this.loadingOperation2.next(false);
          this.loadingOperation3.next(false);
          this.tourId = res;
          this.tour.TourId = res;
          this.presentToast('Created Successfully','success')
         },err=>{
           let errMsg: string;
           if(err){
             errMsg = err.error;
             console.log(err.error);
           }
           if(errMsg){
             errMsg = `Error, ${errMsg}`;
           }else{
             errMsg = 'Error, Try again later';
           }
         this.presentToast(errMsg,'danger')
          this.loadingOperation2.next(false);
          this.loadingOperation3.next(false);
         });
      }
    }

    async presentToast(msg:string, color: string) {
      const toast = await this.toastController.create({
        message: msg,
        duration: 2000,
        color: color
      });
      toast.present();
    }

    addCarPrice(){
      if(!this.tour.TourCarPrice){
        this.tour.TourCarPrice = [];
      }
      let newCarPrice = <TourCarPrice> {};

      this.tour.TourCarPrice = [...this.tour.TourCarPrice, newCarPrice];
    }
    addTourFeature(){
      if(!this.tour.TourFeature){
        this.tour.TourFeature = [];
      }
      let newCarPrice = <TourFeature> {};

      this.tour.TourFeature = [...this.tour.TourFeature, newCarPrice];
    }
    removeTourFeature(tourFeature:TourFeature){
      this.tour.TourFeature = this.tour.TourFeature.filter(t => t !== tourFeature);
    }

    
  ngOnInit() {

  }
  onUploadImage(){
    const options = {
      resultType: CameraResultType.Uri
    };

    Camera.getPhoto(options).then(

        (photo) => {
            console.log(photo);
        }, (err) => {
            console.log(err);

        }

    );
  }
  myFiles: string[] = [];  
  getFileDetails(e) {  
    //console.log (e.target.files);  
    for (var i = 0; i < e.target.files.length; i++) {  
      this.myFiles.push(e.target.files[i]);  
    }  
  } 
  uploadFiles() {  
    /*
    const frmData = new FormData();  
    for (var i = 0; i < this.myFiles.length; i++) {  
      frmData.append("images", this.myFiles[i],'temp');  
      if (i == 0) {  
       frmData.append("imageGroup", "temp");  
      }  
      break;
    }  
    this.toursService._uploadTourImages2( frmData).subscribe(  
      res => {  
        // SHOW A MESSAGE RECEIVED FROM THE WEB API.  
        console.log(res);  
      },err=>{        console.log(err);
      }
    );  
    */
  } 
  public progress: number;
  public message: string;
 
  public uploadFile = (files) => {
    if (files.length === 0) {
      return;
    }
 
    const formData = new FormData();
    for(let i = 0;i < files.length;i++){
      let fileToUpload = <File>files[i];
      formData.append('file', fileToUpload, fileToUpload.name);
    }
    formData.append('imageGroup','temp');
    this.toursService.uploadTourImages4(formData).subscribe(event => {
        if (event.type === HttpEventType.UploadProgress)
          this.progress = Math.round(100 * event.loaded / event.total);
        else if (event.type === HttpEventType.Response) {
          this.message = 'Upload success.';

          this.toursService.GetTourImages("temp").subscribe(imgs=>{
            this.tour.TourAttachment = imgs;
            console.log('finished');
          });
        }
      });
  }
  public progress2: number;
  public message2: string;
 
  public uploadTourPrimaryImage = (files) => {
    if (files.length === 0) {
      return;
    }
 
    const formData = new FormData();
    for(let i = 0;i < files.length;i++){
      let fileToUpload = <File>files[i];
      formData.append('file', fileToUpload, fileToUpload.name);
    }
    this.toursService.uploadTourPrimaryImage(formData).subscribe(event => {
        if (event.type === HttpEventType.UploadProgress)
          this.progress2 = Math.round(100 * event.loaded / event.total);
        else if (event.type === HttpEventType.Response) {
          this.message2 = 'Upload success.';
          console.log(event.body.toString());
          this.tour.TourImage = event.body.toString();
        }
      });
  } 
  
  public progress3: number;
  public message3: string;
 
  public uploadTourFeatureImage = (files) => {
    if (files.length === 0) {
      return;
    }
 
    const formData = new FormData();
    for(let i = 0;i < files.length;i++){
      let fileToUpload = <File>files[i];
      formData.append('file', fileToUpload, fileToUpload.name);
    }
    this.toursService.uploadTourPrimaryImage(formData).subscribe(event => {
        if (event.type === HttpEventType.UploadProgress)
          this.progress3 = Math.round(100 * event.loaded / event.total);
        else if (event.type === HttpEventType.Response) {
          this.message3 = 'Upload success.';
          console.log(event.body.toString());
          this.tour.TourFeatureImage = event.body.toString();
        }
      });
  }
  onSelectedFile(event) {
    
    /*
    if (event.target.files.length > 0) {
      const productImage = event.target.files[0];

      const formData = new FormData();
      formData.append('productImage', productImage);
      this.productService.uploadImage(formData).subscribe(
        res => {
          if (res.status === 200 && res.response.status === 'success') {
            this.uploadError = '';

            const li: HTMLLIElement = this.renderer.createElement('li');

            const img: HTMLImageElement = this.renderer.createElement('img');
            img.src = res.response.imagePath;
            this.renderer.addClass(img, 'product-image');

            const a: HTMLAnchorElement = this.renderer.createElement('a');
            a.innerText = 'Delete';
            this.renderer.addClass(a, 'delete-btn');
            a.addEventListener('click', this.deleteProductImage.bind(this, res.response.filename, a));

            this.renderer.appendChild(this.image.nativeElement, li);
            this.renderer.appendChild(li, img);
            this.renderer.appendChild(li, a);
          } else {
            this.uploadError = res.response.message;
          }
        },
        err => this.error = err
      );
    }
    */
  }

    /*
    const ab = await Camera.getPhoto(CameraSource.Photos).then(

      (photo) => {
          console.log(photo);
      }, (err) => {
          console.log(err);

      }

    );
    */
}
