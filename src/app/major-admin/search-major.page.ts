import { Component, OnInit, ViewChild, Optional } from '@angular/core';
import { IonInfiniteScroll, ModalController, NavParams } from '@ionic/angular';
import { BehaviorSubject, ReplaySubject } from 'rxjs';
import { Router } from '@angular/router';
import { NavExtrasService } from '../services/nav-extras.service';
import { ToursService } from '../services/tours.service';
import { SortModes } from '../shared/enums/sort-modes.enum';
import { SortDirections } from '../shared/enums/sort-directions.enum';
import { Tour } from '../shared/models/tour';


@Component({
  selector: 'app-major',
  templateUrl: 'search-major.page.html',
  styleUrls: ['search-major.page.scss']
})

export class SearchTourPage implements OnInit {
  offset = 0;
  searchList = [];
  searchTxt: string = '';
  fromDate: string = '';
  toDate: string = '';
  active = true;
  view: string;
  loading = new BehaviorSubject<boolean>(false);

  @ViewChild(IonInfiniteScroll,{static : false}) infinite : IonInfiniteScroll;

  constructor(private toursService : ToursService,private router: Router,
    private navExtrasService: NavExtrasService,
    @Optional() private modalController?: ModalController,@Optional() private params?: NavParams) 
    {
      if(params){
        this.view = params.get("view");
      }
    }
  
  ngOnInit(){
    this.loadSearch();
  }

  loadSearch(loadMore = false, event?){
    this.loading.next(true);
    if(this.offset == 0 && this.infinite){
      this.infinite.disabled = true;
    }

    this.toursService.GetTours(this.searchTxt,this.fromDate,this.toDate,this.active,this.offset,20,SortModes.CreateDate,SortDirections.Ascending).subscribe(res=>{
      this.infinite.disabled = false;
      this.loading.next(false);
      console.log("hhh");
      if(this.offset > 0){
        this.searchList = [...this.searchList,...res];
      }
      else{
        this.searchList = res;
      }
      this.offset = this.searchList.length;
      if(event){
        event.target.complete();
      }
      if(this.offset > 100 || res.length == 0){
        this.infinite.disabled = true;
      }
    },(err)=>{
      if(err){
        console.log(err);
      }
      this.offset = 0;
      if(this.infinite){
        this.infinite.disabled = false;
      }
      this.loading.next(false);
        //this.errorMessage = "Some thing went wrong!";
    });
  }

  addAccount(){
    this.router.navigateByUrl(`/admin/edit-tour`);
  }
  onSearchClick(searchItem){
    if(searchItem){
      if(this.view){
        if(this.modalController){
          this.modalController.dismiss(searchItem);
        }
      }
      else{
        this.navExtrasService.setExtras(searchItem);
        this.router.navigateByUrl(`/admin/edit-tour/${searchItem.TourId}`);
      }
    }
  }
  onSearchChange(e){
    if(this.infinite){
      this.infinite.complete();
    }
    
    this.searchList = [];
    this.searchTxt = e.detail.value; 
    this.offset = 0;
    this.loadSearch();
  }
}
export class SearchTourModalPage extends SearchTourPage{
  
}