import { Component} from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { PopoverController, AlertController, ModalController } from '@ionic/angular';
import { LanguagePopoverPage } from '../utils/language-popover/language-popover.page';
import { TranslateService } from '@ngx-translate/core';
import { ContentViewPage } from '../content-view/content-view.page';

@Component({
  selector: 'app-help',
  templateUrl: 'help.page.html',
  styleUrls: ['help.page.scss']
})
export class HelpPage {
  isLoggedIn:boolean
  constructor(private oauthService: OAuthService,private popoverCtrl: PopoverController,private alertController: AlertController,
    private translate: TranslateService,
    private modalController: ModalController) {}
  ionViewWillEnter(){
    if(this.oauthService.hasValidAccessToken()){
      this.isLoggedIn = true;
    }
  }
  async showAlert(){
    const alert = await this.alertController.create({
      header: this.translate.instant('HOME.title'),
      message: this.translate.instant('HOME.title'),
      buttons: ['OK']
    });
    alert.present();
  }
  async openLanguagePopover(ev){
    const popover = await this.popoverCtrl.create({
      component: LanguagePopoverPage,
      event: ev
    });
    await popover.present();
  }
  
  async phoneClick(ev,mobile){
   
  }
  async openContentView(ev,page){
    const modal = await this.modalController.create({
      component : ContentViewPage,
      componentProps : {
        event: event,
        page: page
      }
    });
    modal.present();
  }
  async emailClick(ev){
   
  }
}
