import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HelpPage } from './help.page';
import { TranslateModule } from '@ngx-translate/core';
import { LanguagePopoverPageModule } from '../utils/language-popover/language-popover.module';
import { ContentViewPageModule } from '../content-view/content-view.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([{ path: '', component: HelpPage }]),
    TranslateModule,
    ContentViewPageModule,
    LanguagePopoverPageModule
  ],
  declarations: [HelpPage]
})
export class HelpPageModule {}
