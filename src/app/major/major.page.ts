import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController, ModalController } from '@ionic/angular';
import { NavExtrasService } from '../services/nav-extras.service';
import { Tour } from '../shared/models/tour';
import { ReplaySubject, BehaviorSubject } from 'rxjs';
import { ToursService } from '../services/tours.service';
import { OAuthService } from 'angular-oauth2-oidc';
import { LocalStorageService } from '../auth/local-storage.service';
import { MapViewLocationPage } from '../map-view-location/map-view-location.page';
import { ImageSlideModalPage } from '../image-slide-modal/image-slide-modal.page';

@Component({
  selector: 'app-major',
  templateUrl: './major.page.html',
  styleUrls: ['./major.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TourPage implements OnInit {
  slideOptsProgressbar = {
    initialSlide: 0,
    slidesPerView: 1,
    autoplay:true
  };

  tourId:string
  tour: Tour;
  loadingOperation = new BehaviorSubject<boolean>(false);
  tourReplaySubject: ReplaySubject<Tour> = new ReplaySubject(1);
  constructor(private activatedRoute: ActivatedRoute,private router:Router,private navCtrl:NavController,private navExtrasService: NavExtrasService
    ,private toursService:ToursService,
    private oauthService: OAuthService,private localStorageService: LocalStorageService,
    private modalController: ModalController) {

      this.tourId = this.activatedRoute.snapshot.paramMap.get('id');
      if(!this.tourId){
        this.navCtrl.navigateBack('dashboard');
        return;
      }

    if(this.navExtrasService.getExtras()){
     // console.log('its working2');
      this.tour = this.navExtrasService.getExtras() as Tour;
      this.tourReplaySubject.next(this.tour);
    }else{
      this.tour = {} as Tour;

      if(this.tourId){
        this.loadingOperation.next(true);
        this.toursService.GetClientTour(this.tourId).subscribe(res=>{
          this.tour = res;
          this.loadingOperation.next(false);
          this.tourReplaySubject.next(this.tour);
        },err=>{
          this.loadingOperation.next(false);
          this.tourReplaySubject.next(null);
  
        }); 
      }
    }
   }

   openSlideImages(){
     if(this.tour.TourAttachment && this.tour.TourAttachment.length > 0){
      this.modalController.create({
        component:ImageSlideModalPage,
        componentProps: {
          images: this.tour.TourAttachment
        },
        cssClass: 'images-modal'
      }).then(modal => modal.present());
     }
   }
  backClick(){
    this.navCtrl.navigateBack('dashboard');
  }
  bookTour(){
    let navUrl = `/login/${this.tour.TourId}` 
    
      if(this.tour.TourFeature && this.tour.TourFeature.length){
        if(this.oauthService.hasValidAccessToken()){
          navUrl = `/tour-feature/${this.tour.TourId}`
        }
        else{
          navUrl = `/login/${this.tour.TourId}/2`
        }
      }else{
        if(this.oauthService.hasValidAccessToken()){
          navUrl = `/tour-pax/${this.tour.TourId}`
        }
      }
   
    this.navExtrasService.setExtras(this.tourReplaySubject);
    this.router.navigateByUrl(navUrl)
  }
  ngOnInit() {
  }

  async openMapPage(name,lat,lng){
    const modal = await this.modalController.create({
      component : MapViewLocationPage,
      componentProps : {
        title: name,
        lat: lat,
        lng: lng
      }
    });
   
    modal.present();
  
 
  // this.router.navigateByUrl(`/map-view-location/${name}/${lat}/${lng}`)
  }
}
