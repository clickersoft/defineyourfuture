import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TourPage } from './major.page';
import { MomentModule } from 'ngx-moment';
import { TranslateModule } from '@ngx-translate/core';
import { MapViewLocationPageModule } from '../map-view-location/map-view-location.module';
import { ImageSlideModalPageModule } from '../image-slide-modal/image-slide-modal.module';

const routes: Routes = [
  {
    path: '',
    component: TourPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MomentModule,
    TranslateModule,
    MapViewLocationPageModule,
    ImageSlideModalPageModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TourPage]
})
export class TourPageModule {}
