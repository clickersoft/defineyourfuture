import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './auth/auth-guard.service';
import { GlobService } from './auth/glob.service';
import { InitialPage } from './initial/initial.page';
import { AuthResolverService } from './auth/auth-resolver';

const routes: Routes = [
  { path: '', loadChildren: './home/home.module#HomePageModule' },
  //{ path: '', redirectTo: '/dashboard/home', pathMatch: 'full' },
  //{ path: '', redirectTo: '/login-admin', pathMatch: 'full' },
 // { path: 'admin', canActivate: [AuthGuardService, GlobService], children: 
//    [
 //     { path: 'home', loadChildren: './home/home.module#HomePageModule' },
 //     { path: '', redirectTo: 'home', pathMatch: 'full' }
     
     /* { path: 'dashboard', loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardPageModule) }*/
 //   ] 
 // },
  { path: 'initial', loadChildren: () => import('./initial/initial.module').then(m => m.InitialPageModule),canActivate: [AuthGuardService]},
  { path: 'login-admin', loadChildren: './login-admin/login-admin.module#LoginAdminPageModule' },
  { path: 'logout', loadChildren: './logout/logout.module#LogoutPageModule' },
  { path: 'get-started', loadChildren: './get-started/get-started.module#GetStartedPageModule' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
 
  { path: 'verification', loadChildren: './verification/verification.module#VerificationPageModule' },
 
  { path: 'major', loadChildren: './major/major.module#TourPageModule' },
  { path: 'major/:id', loadChildren: './major/major.module#TourPageModule' },
  /* Id in the folowing pages is for tourid so that after login or complete reg you route to the tour */
  { path: 'edit-profile/:id', loadChildren: './edit-profile/edit-profile.module#EditProfilePageModule' },
  { path: 'login/:id', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'login/:id/:pageRedirect', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'logout-admin', loadChildren: './logout-admin/logout-admin.module#LogoutAdminPageModule' },
  { path: 'login-mobile', loadChildren: './login-mobile/login-mobile.module#LoginMobilePageModule' },
  { path: 'login2', loadChildren: './login2/login2.module#Login2PageModule' },
  { path: 'questionnaire', loadChildren: './questionnaire/questionnaire.module#QuestionnairePageModule' },

  //{ path: 'map-view-location/:title/:lat/:lng', loadChildren: './map-view-location/map-view-location.module#MapViewLocationPageModule' }



];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
