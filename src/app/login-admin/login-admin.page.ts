import { Component, OnInit,ViewChild,ElementRef,AfterViewInit } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { IonInput } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-admin',
  templateUrl: './login-admin.page.html',
  styleUrls: ['./login-admin.page.scss'],
})
export class LoginAdminPage implements OnInit,AfterViewInit, AfterViewInit {

  ngAfterViewInit() {
    this.email.setFocus();
  }
  @ViewChild('email',{static : false}) email: IonInput;

  username: string;
  password: string;
  error: string;
  loading: boolean;

  constructor(private oauthService: OAuthService, private router: Router) {
    /*oauthService.redirectUri = window.location.origin;
    oauthService.clientId = '[client-id]';
    oauthService.scope = 'openid profile email';
    oauthService.oidc = true;
    oauthService.issuer = 'https://dev-[dev-id].oktapreview.com';
    */
  }

  ngOnInit() {
    //console.log("hhh2");
    setTimeout(() => {
   
    }, 500);
  }

  login(): void {
    this.loading = true;
    this.error = null;
    this.oauthService.fetchTokenUsingPasswordFlow(this.username, this.password).then((resp) => {
      //console.log("success");
          // Loading data about the user
          return this.oauthService.loadUserProfile();

    }).then(() => {
     // console.log("success");
          // Using the loaded user data
          let claims = this.oauthService.getIdentityClaims();
          
          //if (claims) console.debug('given_name', claims.given_name);
         // console.log(JSON.stringify(claims));
          this.router.navigate(['/admin']);
    }).catch((reason)=>{
      this.error = "Username or password is incorrect"
      console.log("Error");
      console.log(JSON.stringify(reason));
    }).finally(()=>{
      this.loading = false;
    });


    /*
      return authClient.signIn({
        username: ,
        password: 
      }).then((response) => {
        if (response.status === 'SUCCESS') {
          return authClient.token.getWithoutPrompt({
            nonce: nonce,
            responseType: ['id_token', 'token'],
            sessionToken: response.sessionToken,
            scopes: this.oauthService.scope.split(' ')
          })
            .then((tokens) => {
              // oauthService.processIdToken doesn't set an access token
              // set it manually so oauthService.authorizationHeader() works
              localStorage.setItem('access_token', tokens[1].accessToken);
              this.oauthService.processIdToken(tokens[0].idToken, tokens[1].accessToken);
              this.navCtrl.push(DashboardPage);
            });
        } else {
          throw new Error('We cannot handle the ' + response.status + ' status');
        }
      }).fail((error) => {
        console.error(error);
        this.error = error.message;
      });
    });
    */
  }
  
}
