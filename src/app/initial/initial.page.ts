import { Component, AfterViewInit, NgZone, ChangeDetectorRef, ViewRef, OnInit } from '@angular/core';
import { HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Subject, Observable, of } from 'rxjs';
import { catchError  } from 'rxjs/operators';
import { LocalStorageService, Clinic } from './../auth/local-storage.service';
import { OAuthService, JwksValidationHandler} from 'angular-oauth2-oidc';
import { UsersService } from '../services/users.service';
import { CompaniesService } from '../services/companies.service';
//import * as Prism from 'prismjs';



@Component({
  selector: 'app-initial',
  templateUrl: './initial.page.html',
  styleUrls: ['./initial.page.scss'],
})
export class InitialPage implements OnInit , AfterViewInit{
  public activeIndex: any = 0;

  constructor(private ngZone: NgZone,
    private changeDetectorRef: ChangeDetectorRef, private router: Router, private localStorageService: LocalStorageService, private oauthService: OAuthService,
    private usersService : UsersService,private companiesService : CompaniesService)
  {
   
  }

  public logout() {
    //this.localStorageService.storeClinic({} as Clinic);
    this.oauthService.logOut(true);
    this.router.navigate(['login-admin']);
  }

   public clinic: Company;
  /**
   * @method ngAfterViewInit
   */
  ngAfterViewInit() {

    this.ngZone.runOutsideAngular(() => {
      setTimeout(() => {
        this.activeIndex = [1, 2, 3];
        if (!(this.changeDetectorRef as ViewRef).destroyed) {
          this.changeDetectorRef.detectChanges();
        }
      }, 500);
    });

  }

  companies: Company[];
  users: User[];
  allusers: User[];
  counter: number = 0;
  user: User;
  password: string;
  errorMessage: string;
  successMessage: string;

  ngOnInit() {
    this.companies = [];
    this.users = [];
    this.allusers = [];
    let currentClinic = null//this.localStorageService.getClinic();
    const currentClinicId = currentClinic == null ? '' : currentClinic.Company_ID;
  
    this.companiesService.getAll().subscribe((data: Company[]) => {
        this.companies = data;
        if (this.companies &&  this.companies.length > 0) {
          let selectedClinic= this.companies.find(x=> x.Company_ID == currentClinicId);
          if(selectedClinic){
            this.companyClick(selectedClinic);
          }else if(this.companies.length){
            this.companyClick(this.companies[0]);
          }
        }
      },(err)=>{
        if(err){
          console.log(err);
        }
      });

   this.usersService.getAll().subscribe((data: User[]) => {
        this.allusers = data;
        this.companyClick(this.clinic);
      },(err)=>{
        if(err){
          console.log(err);
        }
      });
  }
 
  public login(): void {
    this.errorMessage = null;
    this.successMessage = null;
    if (this.user && this.password) {
     this.usersService.getAuthentication(this.clinic.Company_ID,this.user.User_Name,this.password).subscribe((data: User) => {
          if (data) {
            const clinic: Clinic = {
              Company_ID: this.clinic.Company_ID,
              User_Name: this.user.User_Name,
              Password: this.password,
              FullName: this.user.F_Name + ' ' + this.user.L_Name,
              Company_Name: this.clinic.Name,
              Company_Image: '',
              User_ID : this.user.User_ID,
              User_Image : this.user.Image,
              User_Type_ID: this.user.User_Type_ID
            };
            //this.localStorageService.storeClinic(clinic);
            this.successMessage = "Login Successfully";

            this.router.navigate(['']);
          }
        },(err)=>{
          if(err){
            console.log(err);
          }
            this.errorMessage = "Username or password is incorrect";
        });
    }
    else {
      this.errorMessage = "Plz enter username & password";
    }
  }

  public companyClick(company: Company) {
    this.clinic = company;
    this.errorMessage = null;
    if (company) {
      if (this.allusers) {
        this.users = this.allusers.filter(user => user.Company_ID === company.Company_ID);
      }
    }
    else {
      this.user = null;
      this.users = [];
    }
  }
}
