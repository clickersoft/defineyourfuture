import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import {NgbModule,NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { OAuthService,OAuthModule } from 'angular-oauth2-oidc';
import { FormsModule }   from '@angular/forms';
import { StorageServiceModule } from 'ngx-webstorage-service';
import { LocalStorageService } from './auth/local-storage.service';
import { AccountsService } from './services/accounts.service';
import { ExpandableHeaderComponent } from './utils/expandable-header/expandable-header.component';
import { TranslateModule,TranslateLoader } from '@ngx-translate/core'
import { TranslateHttpLoader } from '@ngx-translate/http-loader'
import { PayPal } from '@ionic-native/paypal/ngx';
import { ImageSlideModalPageModule } from './image-slide-modal/image-slide-modal.module';
export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http,'assets/i18n/','.json');
}
@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: 
  [
    BrowserModule, 
    FormsModule,
    IonicModule.forRoot(), 
    BrowserAnimationsModule,
    NgbModalModule,
    NgbModule,
    HttpClientModule,
    StorageServiceModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    OAuthModule.forRoot({
      resourceServer: {
        allowedUrls: ['http://dfapi.clickersoft.com/api'],
        sendAccessToken: true
      }
    }),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    AppRoutingModule],
    exports: [
      TranslateModule
    ],
  providers: [
    StatusBar,
    SplashScreen,
    PayPal,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    OAuthService,
    LocalStorageService,
    AccountsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
