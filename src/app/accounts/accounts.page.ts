import { Component, OnInit, ViewChild, Optional, ElementRef, Input, ContentChildren } from '@angular/core';
import { IonInfiniteScroll, ModalController, NavParams, IonSearchbar, IonContent, IonFab } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { NavExtrasService } from '../services/nav-extras.service';
import { AccountsService } from '../services/accounts.service';
import { AccountTypes } from '../shared/enums/account-types.enum';


@Component({
  selector: 'app-accounts',
  templateUrl: 'accounts.page.html',
  styleUrls: ['accounts.page.scss']
})

export class AccountsPage implements OnInit {
  offset = 0;
  searchList = [];
  searchTxt: string = '';
  view: string;
  loading = new BehaviorSubject<boolean>(false);

  @ViewChild(IonInfiniteScroll,{static : false}) infinite : IonInfiniteScroll; 
  @ViewChild(IonSearchbar,{static : false}) searchbar : IonSearchbar; 

  constructor(private accountsService : AccountsService,private router: Router,
    private navExtrasService: NavExtrasService,
    @Optional() private modalController?: ModalController,@Optional() private params?: NavParams) 
    {
      if(params){
        this.view = params.get("view");
      }
    }
  
  ngOnInit(){
    this.loadSearch();
  }

  loadSearch(loadMore = false, event?){
    this.loading.next(true);
    if(this.offset == 0 && this.infinite){
      this.infinite.disabled = true;
    }

    let accountTypeId = 0;
    if(this.view){
      switch(this.view){
        case 'Client':
            accountTypeId = AccountTypes.Client.valueOf();
          break;
          case 'Driver':
              accountTypeId = AccountTypes.Driver.valueOf();
          break;
      }
    }
    this.accountsService.GetAccounts(this.searchTxt,accountTypeId,0,null,this.offset).subscribe(res=>{
      this.infinite.disabled = false;
      this.loading.next(false);
      console.log("hhh");
      if(this.offset > 0){
        this.searchList = [...this.searchList,...res];
      }
      else{
        this.searchList = res;
      }
      this.offset = this.searchList.length;
      if(event){
        event.target.complete();
      }
      if(this.offset > 100 || res.length == 0){
        this.infinite.disabled = true;
      }
    },(err)=>{
      if(err){
        console.log(err);
      }
      this.offset = 0;
      if(this.infinite){
        this.infinite.disabled = false;
      }
      this.loading.next(false);
        //this.errorMessage = "Some thing went wrong!";
    });
  }

  addAccount(){
    this.router.navigateByUrl(`/admin/edit-account`);
  }
  onSearchClick(searchItem){
    if(searchItem){
      if(this.view){
        if(this.modalController){
          this.modalController.dismiss(searchItem);
        }
      }
      else{
        this.navExtrasService.setExtras(searchItem);
        this.router.navigateByUrl(`/admin/account/${searchItem.AccountId}`);

        console.log(searchItem.FullName);
      }
    }
  }
  onSearchChange(){
    if(this.infinite){
      this.infinite.complete();
    }
    
    this.searchList = [];
    this.offset = 0;
    this.loadSearch();
  }
  
  isSearchBarOppend:boolean;
  toggleSearchBar(isSearchBarOppend:boolean){
    this.isSearchBarOppend = isSearchBarOppend;
    if(isSearchBarOppend){
      this.searchbar.setFocus();  
    }else{
      this.searchTxt = '';
      this.onSearchChange();
    }
  }
}

export class AccountsModalPage extends AccountsPage{
  
}