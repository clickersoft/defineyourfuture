import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ModalController, NavParams, ToastController, LoadingController, PopoverController } from '@ionic/angular';
import { SearchModalPage } from '../search/search.page';
import { BehaviorSubject } from 'rxjs';
import * as moment from 'moment';
import { CalendarEvent } from 'calendar-utils';
import { Lookup } from '../shared/models/lookup';
import { SharedApiService } from '../services/shared-api.service';
import { LookupTypes } from '../shared/enums/lookup-types.enum';
import { Search } from '../shared/models/search';
import { AppointmentsService } from '../services/appointments.service';
import { IonColor, ColorService } from '../services/color-service';
import { ColorPopoverPage } from '../utils/color-popover/color-popover.page';
import { AppSettings } from '../services/appsettings';
import { Answer } from '../shared/models/answer';
import { AnswerMajor } from '../shared/models/answer-major';

@Component({
  selector: 'app-answer',
  templateUrl: './answer.page.html',
  styleUrls: ['./answer.page.scss'],
})
export class AnswerPage implements OnInit {
  selectedItem: Answer;
  constructor(private modalController: ModalController,private params: NavParams,private toastController: ToastController,
    private chRef: ChangeDetectorRef) 
  { 
    if(this.params.get("answer")){
      this.selectedItem = this.params.get("answer") as Answer
    }else{
      console.log('New Event with default values');
    }
  }

  ngOnInit(){

  }

  removeAnswer(answer:AnswerMajor){
    this.selectedItem.AnswerMajor = this.selectedItem.AnswerMajor.filter(acc => acc !== answer);
    //this.calcTotalAmount();
  }
  addAnswer(){
    if(!this.selectedItem.AnswerMajor){
      this.selectedItem.AnswerMajor = [];
    }
    this.selectedItem.AnswerMajor.push({} as AnswerMajor);
    
    //this.calcTotalAmount();
    this.chRef.detectChanges();
  }
  
  async presentToast(msg:string, color: string) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
      color: color
    });
    toast.present();
  }
  back(){
    this.modalController.dismiss();
  }
}
