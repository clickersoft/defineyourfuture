import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountPage } from './account.page';

const routes: Routes = [
  {
    path: '',
    component: AccountPage,
    /*
    children: [
      {
        path: 'details',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../account/account-details/account-details.module').then(m => m.AccountDetailsPageModule)
          }
        ]
      },
      {
        path: 'panorama',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../account/panorama/panorama.module').then(m => m.PanoramaPageModule)
          }
        ]
      },
      {
        path: 'balance',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../account/balance/balance.module').then(m => m.BalancePageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: 'details',
        pathMatch: 'full'
      }
    ]
    */
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountPageRoutingModule {}
