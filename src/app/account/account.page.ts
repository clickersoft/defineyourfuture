import { Component, OnInit, ViewChild, ElementRef, Renderer2, AfterViewInit, Inject, Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Search } from '../shared/models/search';
import { IonContent, ToastController } from '@ionic/angular';
import { DOCUMENT } from '@angular/common';
import { BehaviorSubject, Observable, ReplaySubject } from 'rxjs';
import { AccountsService } from '../services/accounts.service';
import { Account } from '../shared/models/account';
import { NavExtrasService } from '../services/nav-extras.service';
import * as moment from 'moment';
import { AppSettings } from '../services/appsettings';

@Injectable({
  providedIn: 'root'
})
@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit,AfterViewInit {
  @ViewChild(IonContent,{static: false}) content:IonContent;
  //@ViewChild('myNav',{static: false}) myNav:ElementRef;
 // @ViewChild('myUl',{static: false}) myUl:ElementRef;
 // @ViewChild(SuperTabs,{static:false}) superTabs: SuperTabs;

  selectedTab = 'details';
  //private selectedTab = new BehaviorSubject<string>('details');
  account:Account
  accountId: string;
 // complaintList: AccountComplaint[] = [];
  loadingOperation = new BehaviorSubject<boolean>(false);
  accountReplaySubject: ReplaySubject<Account> = new ReplaySubject(1);

  constructor(@Inject(DOCUMENT) private document: Document,
  private activatedRoute: ActivatedRoute,private rd: Renderer2,
  private accountsService: AccountsService,
  private navExtrasService: NavExtrasService,
  private toastController: ToastController,
  private router: Router) 
  {
    //if(this.navExtrasService.getExtras()){
      //console.log('its working');
     // console.log(this.navExtrasService.getExtras());
    //}
    this.accountId = this.activatedRoute.snapshot.paramMap.get('id');
    
    if(this.navExtrasService.getExtras()){
     // console.log('its working2');
      this.account = this.navExtrasService.getExtras() as Account;

      this.accountReplaySubject.next(this.account);
      
    }else{
      this.account = {
        FullName: 'No Name',
        Mobile: ''
      } as Account

      if(this.accountId){
        this.loadingOperation.next(true);
        this.accountsService.GetAccount(this.accountId).subscribe(res=>{
          this.account = res;
          console.log(this.account);
          this.loadingOperation.next(false);
          this.accountReplaySubject.next(this.account);
        },err=>{
          this.loadingOperation.next(false);
          this.accountReplaySubject.next(null);
  
        }); 
        
      }
    }
    
   
    
  }
  editAccount(){
    this.navExtrasService.setExtras(this.accountReplaySubject);
    this.router.navigateByUrl(`/admin/edit-account/${this.accountId}`);
  }

  tabChanged(ev:any){
    console.log(this.selectedTab);
  }
  /*
  scrolling(event) {
    var windowTop = event.detail.scrollTop;
    console.log(windowTop);
    windowTop > 100 ? this.rd.addClass(this.myNav.nativeElement, 'navShadow') : this.rd.removeClass(this.myNav.nativeElement, 'navShadow');
    //windowTop > 100 ? this.rd.setStyle(this.myUl.nativeElement, 'top','100px') : this.rd.setStyle(this.myUl.nativeElement, 'top','180px');
  }

  
*/
  //selectedComplaint: AccountComplaint;
  createComplaint(){
    /*
    this.selectedComplaint = {
      Complaint: '',
      Complaint_Date: moment().format(AppSettings.DATE_FORMAT)
    } as AccountComplaint
    */
  }
  submitComplaint(){
    /*
    const submitedComplaint = this.selectedComplaint;
    let msg : string;
    if(submitedComplaint.Account_Complaint_ID){
      msg = 'Edited Successfully'
    }else{
      msg = 'Created Successfully'
      this.account.Account_Complaint = [...this.account.Account_Complaint,submitedComplaint];
    }
    

    this.loadingOperation.next(true);
    submitedComplaint.Account_ID = this.account.AccountId;
    this.accountsService.createOrUpdateComplaint(submitedComplaint)
    .subscribe(res=>{
      this.loadingOperation.next(false);
      this.presentToast(msg,'success')
      this.selectedComplaint = null;
      submitedComplaint.Account_Complaint_ID = res;
    },err=>{
      let errMsg: string;
      if(err){
        errMsg = err.error;
        console.log(err.error);
      }
      if(errMsg){
        errMsg = `Error, ${errMsg}`;
      }else{
        errMsg = 'Error, Try again later';
      }
      this.presentToast(errMsg,'danger')
      this.loadingOperation.next(false);
      if(!submitedComplaint.Account_Complaint_ID){
        this.account.Account_Complaint = this.account.Account_Complaint.filter(event => event !== submitedComplaint);
      }
    });    
    */
  }
  discardComplaint(){
    //this.selectedComplaint = null;
  }
  selectComplaint(selected){
    //this.selectedComplaint = selected;
  }
  deleteComplaint(){
    /*
    const deletedComplaint = this.selectedComplaint;
    this.loadingOperation.next(true);

    this.account.Account_Complaint = this.account.Account_Complaint.filter(event => event !== deletedComplaint);

    this.accountsService.deleteComplaint(deletedComplaint.Account_Complaint_ID)
    .subscribe(res=>{
      this.presentToast('Deleted Successfully','success')
      this.loadingOperation.next(false);
      this.selectedComplaint = null;
    },err=>{
      let errMsg: string;
      if(err){
        errMsg = err.error;
        console.log(err.error);
      }
      if(errMsg){
        errMsg = `Error, ${errMsg}`;
      }else{
        errMsg = 'Error, Try again later';
      }
      this.presentToast(errMsg,'danger')
      this.loadingOperation.next(false);
      this.account.Account_Complaint = [...this.account.Account_Complaint,deletedComplaint];
    });    
    */
  }
  ngAfterViewInit() {
  }

  onTabChange(ev: any) {
    console.log('Segment changed', ev);
    if(ev.index === 2){
      
    }
  }
  onTabClick(ev: any){

  }
  
  ngOnInit() {
  //  this.accountId = this.activatedRoute.snapshot.paramMap.get('id');
  //  console.log(this.accountId);
  }
  async presentToast(msg:string, color: string) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
      color: color
    });
    toast.present();
  }
}
