import { Component, OnInit, Renderer2 } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AccountsService } from 'src/app/services/accounts.service';
import { NavExtrasService } from 'src/app/services/nav-extras.service';
import { ToastController } from '@ionic/angular';
import { Observable, BehaviorSubject, ReplaySubject } from 'rxjs';
import { SharedApiService } from 'src/app/services/shared-api.service';
import { Lookup } from 'src/app/shared/models/lookup';
import * as moment from 'moment';
import { LookupTypes } from 'src/app/shared/enums/lookup-types.enum';
import { Account } from 'src/app/shared/models/account';
import { GsCountry } from 'src/app/shared/models/gs-country';
import { CarType } from 'src/app/shared/models/car-type';
import { AppSettings } from 'src/app/services/appsettings';

@Component({
  selector: 'app-edit-account',
  templateUrl: './edit-account.page.html',
  styleUrls: ['./edit-account.page.scss'],
})
export class EditAccountPage implements OnInit {

  account:Account
  accountId: string;
  loadingOperation = new BehaviorSubject<boolean>(false);
  loadingOperation2 = new BehaviorSubject<boolean>(false);
  loadingOperation3 = new BehaviorSubject<boolean>(false);
  countries:GsCountry[] = [];
  carTypes:CarType[] = [];

  maxBirthDate = moment().format(AppSettings.DATE_MAX_DATE_FORMAT);
  constructor(private activatedRoute: ActivatedRoute,private rd: Renderer2,
    private accountsService: AccountsService,
    private sharedApiService: SharedApiService,
    private navExtrasService: NavExtrasService,
    private toastController: ToastController) 
    { 
      this.accountId = this.activatedRoute.snapshot.paramMap.get('id');

      this.account = { AccountTypeId: '3', Active: true} as Account;
      if(this.accountId){
        if(navExtrasService.getExtras()){
          this.loadingOperation.next(true);
          let accountReplaySubject = navExtrasService.getExtras() as ReplaySubject<Account>;
          accountReplaySubject.subscribe(res=>{
            if(res){
              this.loadingOperation.next(false);
              this.account = res;
            }else{
              this.loadAccount();
            }
          })
         }else{
          this.loadAccount();
         }
      }else{
        //New Account
      }
       

       this.loadingOperation2.next(true);
       this.sharedApiService.GetCountries().subscribe(res=>{
         this.countries = res;
        this.loadingOperation2.next(false);
       },err=>{
        this.loadingOperation2.next(false);
       });

       this.sharedApiService.GetCarTypes().subscribe(res=>{
        this.carTypes = res;
      },err=>{
      });
    }

    loadAccount(){
      this.loadingOperation.next(true);
      this.accountsService.GetAccount(this.accountId).subscribe(res=>{
        this.account = res;
        console.log(this.account);
        this.loadingOperation.next(false);
      },err=>{
        this.loadingOperation.next(false);
      }); 
    }
    submitAccount(){
      this.loadingOperation2.next(true);
      this.loadingOperation3.next(true);
      if(this.accountId){
        this.accountsService.createOrUpdateAccount(this.account).subscribe(res=>{
         this.loadingOperation2.next(false);
         this.loadingOperation3.next(false);
         this.presentToast('Edited Successfully','success')
        },err=>{
          let errMsg: string;
          if(err){
            errMsg = err.error;
            console.log(err.error);
          }
          if(errMsg){
            errMsg = `Error, ${errMsg}`;
          }else{
            errMsg = 'Error, Try again later';
          }
        this.presentToast(errMsg,'danger')
         this.loadingOperation2.next(false);
         this.loadingOperation3.next(false);
        });
      }else{
        this.accountsService.createOrUpdateAccount(this.account).subscribe(res=>{
          this.loadingOperation2.next(false);
          this.loadingOperation3.next(false);
          this.accountId = res;
          this.account.AccountId = res;
          this.presentToast('Created Successfully','success')
         },err=>{
           let errMsg: string;
           if(err){
             errMsg = err.error;
             console.log(err.error);
           }
           if(errMsg){
             errMsg = `Error, ${errMsg}`;
           }else{
             errMsg = 'Error, Try again later';
           }
         this.presentToast(errMsg,'danger')
          this.loadingOperation2.next(false);
          this.loadingOperation3.next(false);
         });
      }
    }

    async presentToast(msg:string, color: string) {
      const toast = await this.toastController.create({
        message: msg,
        duration: 2000,
        color: color
      });
      toast.present();
    }

  ngOnInit() {

  }

}
