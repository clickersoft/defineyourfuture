import { Component, OnInit, Input, ElementRef, Renderer, Renderer2 } from '@angular/core';
import { DomController } from '@ionic/angular';

@Component({
  selector: 'expandable-header',
  templateUrl: './expandable-header.component.html',
  styleUrls: ['./expandable-header.component.scss'],
})

export class ExpandableHeaderComponent implements OnInit {

  @Input('scrollArea') scrollArea: any;
  @Input('headerHeight') headerHeight: number;

  newHeaderHeight: any;

  constructor(public element: ElementRef, public renderer: Renderer,
    private domCtrl: DomController,
    private rd: Renderer2) {

  }

  ngOnInit(){
    this.rd.setStyle(this.element.nativeElement, 'height', this.headerHeight + 'px')
    //this.renderer.setElementStyle(this.element.nativeElement, 'height', this.headerHeight + 'px');

    this.scrollArea.ionScroll.subscribe((ev) => {
      this.resizeHeader(ev);
    });

  }

  resizeHeader(ev){
    if (ev) {
      this.domCtrl.write(() => {
      this.newHeaderHeight = this.headerHeight - ev.detail.scrollTop;

      if(this.newHeaderHeight < 0){
        this.newHeaderHeight = 0;
      }   
console.log(this.newHeaderHeight)
      this.rd.setStyle(this.element.nativeElement, 'height', this.newHeaderHeight + 'px');

      for(let headerElement of this.element.nativeElement.children){
        let totalHeight = headerElement.offsetTop + headerElement.clientHeight;

        if(totalHeight > this.newHeaderHeight && !headerElement.isHidden){
          headerElement.isHidden = true;
          this.rd.setStyle(headerElement, 'opacity', '0');
        } else if (totalHeight <= this.newHeaderHeight && headerElement.isHidden) {
          headerElement.isHidden = false;
          this.rd.setStyle(headerElement, 'opacity', '0.7');
        }

      }
      });
  }
   

  }


}
