import { Component, ViewChild } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { LocalStorageService } from '../auth/local-storage.service';
import { Account } from '../shared/models/account';
import { IonTabs } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard-client',
  templateUrl: 'dashboard-client.page.html',
  styleUrls: ['dashboard-client.page.scss']
})
export class DashboardClientPage {
  loginStatus:string;
  @ViewChild(IonTabs,{static : false}) ionTabs: IonTabs;
  constructor(private oauthService: OAuthService,private localStorageService:LocalStorageService,
    private router: Router) {
    //profileTab
    //admin/home
  }
  ionViewWillEnter(){
    let account = this.localStorageService.getAccount();
    console.log(account);
    this.selectTab(account);
    let firstTime = true;
    this.localStorageService.getAccountBehaviorSubject().subscribe(res=>{
      if(firstTime){
        firstTime = false;
        return
      }
     this.selectTab(res);
    });
  }
  selectTab(account:Account){
    this.loginStatus = "anonymous";
    if(this.oauthService.hasValidAccessToken()){
      let claims = this.oauthService.getIdentityClaims()
      this.loginStatus = 'client';
      if(claims){
        switch(claims['role'] || '--'){
          case 'admin':
            this.loginStatus = 'admin';
            return
            case 'driver':
              this.loginStatus = 'driver';
            return
        }
      }
    }
    console.log(this.loginStatus);
  }
}
