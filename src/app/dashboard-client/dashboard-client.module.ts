import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { DashboardClientPageRoutingModule } from './dashboard-client.router.module';

import { DashboardClientPage } from './dashboard-client.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    DashboardClientPageRoutingModule,
    TranslateModule
  ],
  declarations: [DashboardClientPage]
})
export class DashboardClientPageModule {}
