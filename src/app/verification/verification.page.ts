import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { IonInput, NavController, ToastController } from '@ionic/angular';
import { timeout } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { UsersService } from '../services/users.service';
import { OAuthService } from 'angular-oauth2-oidc';
import { LocalStorageService } from '../auth/local-storage.service';

@Component({
  selector: 'app-verification',
  templateUrl: './verification.page.html',
  styleUrls: ['./verification.page.scss'],
})
export class VerificationPage implements OnInit, AfterViewInit {
  constructor(private navCtrl: NavController,private router: Router
    ,private route: ActivatedRoute,private toastController:ToastController,private usersService: UsersService,
    private oauthService: OAuthService,
    private localStorageService: LocalStorageService) 
  {
    this.route.queryParams.subscribe(params => {
      if (params && params.mobile) {
        this.mobileNumber = params.mobile;
        this.tourId = params.tourId;
        //+970 Palestine
        //+972 
       this.sendCode();
      }else{
        this.changeMobile();
      }
    });
  }
  tourId: string
  sendCode(){
    this.loadingOperation.next(true);
    this.usersService.getMobileCode("+970",this.mobileNumber).subscribe(res=>{
      this.presentToast("سيصلك رمز التفعيل خلال لحظات","success");
      this.loadingOperation.next(false);
      this.startTime();
    },err=>{
      this.counter = 0;
      this.loadingOperation.next(false);
      console.log(err);
      this.error=JSON.stringify(err);
      this.presentToast("لم يتم ارسال رمز التفعيل , حاول مرة اخرى","danger");
      //this.presentToast(JSON.stringify(err),"danger");
    });

  }
  ngAfterViewInit(): void {
    setTimeout(() => {
      this.code1.setFocus();
    }, 400);
  }
  mobileNumber: string;
  counter: number;
  code1Number:number;
  code2Number:number;
  code3Number:number;
  code4Number:number;
  loadingOperation = new BehaviorSubject<boolean>(false);

  ngOnInit() {
 
  }
  startTime(){
    this.counter = 11;
    let intervalId = setInterval(() => {
      this.counter = this.counter - 1;
      if(this.counter <= 0) {
        this.counter = 0;
        clearInterval(intervalId)
      }
    }, 1000)
  }
  reSendCode(){
    this.discardCode();
    this.sendCode();
  }
  changeMobile(){
    this.navCtrl.navigateBack('login');
  }
  error:string;
  submitCode(c1,c2,c3,c4){
    this.presentToast('Verfining...','primary')

    this.loadingOperation.next(true);
    //this.error = null;
    let mobileCode = `${c1}${c2}${c3}${c4}`
    this.oauthService.fetchTokenUsingPasswordFlow(this.mobileNumber, mobileCode).then((resp) => {
      //console.log("success");
          // Loading data about the user
          return this.oauthService.loadUserProfile();

    }).then(() => {
     // console.log("success");
          // Using the loaded user data
          let claims = this.oauthService.getIdentityClaims();
         console.log(JSON.stringify(claims));
         this.presentToast('Verified Successfully','success')
        // this.localStorageService.getAccountBehaviorSubject().next(this.localStorageService.getAccount());
         if(claims['name']){
           if(this.tourId){
            this.navCtrl.navigateRoot(`/tour-reserve/${this.tourId}`)
           }else{
            this.navCtrl.navigateRoot('/dashboard')
           }
         
         }
         else{
          if(this.tourId){
            this.navCtrl.navigateRoot(`/edit-profile/${this.tourId}`)
          }
          else{
            this.navCtrl.navigateRoot('/edit-profile')
          }
         }
          
          

          
          //if (claims) console.debug('given_name', claims.given_name);
         // console.log(JSON.stringify(claims));
          //this.router.navigate(['']);
    }).catch((reason)=>{
     this.presentToast("رمز التفعيل خاطئ","danger");
      console.log("Error");
      console.log(JSON.stringify(reason));
      this.discardCode();
    }).finally(()=>{
      this.loadingOperation.next(false);
    });
  }
  async presentToast(msg:string, color: string) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
      color: color
    });
    toast.present();
  }

  @ViewChild('code1',{static : false}) code1: IonInput;
  @ViewChild('code2',{static : false}) code2: IonInput;
  @ViewChild('code3',{static : false}) code3: IonInput;
  @ViewChild('code4',{static : false}) code4: IonInput;
  
  
  async codeChanged(inputNumber:number,ele:any,event){
    console.log('hhh');
    if(await this.checkToVerfiy(ele)){
      return;
    }
    if(ele.value){
      switch(inputNumber){
        case 1:
          if(this.code2.value){
            await this.codeChanged(2,this.code2,null);
          }else{
            this.code2.setFocus();
          }
          break;
          case 2:
              if(this.code3.value){
                await this.codeChanged(3,this.code3,null);
              }else{
                this.code3.setFocus();
              }
          break;
          case 3:
              if(this.code4.value){
                await this.codeChanged(4,this.code4,null);
              }else{
                this.code4.setFocus();
              }
          break;
          case 4:
              if(this.code1.value){
                await this.codeChanged(1,this.code1,null);
              }else{
                this.code1.setFocus();
              }
          break;
      }
    }
  }
  async checkToVerfiy(ele:any):Promise<boolean>
  {
    if(this.code1.value
      && this.code2.value
      && this.code3.value
      && this.code4.value){
        //Submit
        (await ele.getInputElement()).blur();
        this.submitCode(this.code1.value,this.code2.value,this.code3.value,this.code4.value);
        return true;
    }
    return false;
  }
  codeFocus(event:any){
    event.target.value = "";
  }
  discardCode(){
    this.code1.value = "";
    this.code2.value = "";
    this.code3.value = "";
    this.code4.value = "";

    this.code1.setFocus();
    //this.code1Number= null;
    //this.code2Number= null;
    //this.code3Number= null;
    //this.code4Number= null;
  }
  keydownCode(inputNumber:number,event:any){
    let map = {
      enter: {
        key: "Backspace",
        keyCode: "8"
      },
    };
    
    if(event.key === map.enter.key || event.keyCode === map.enter.keyCode) {
      switch(inputNumber){
        case 1:
           
          break;
          case 2:
            this.code1.setFocus();
          break;
          case 3:
            this.code2.setFocus();
          break;
          case 4:
            this.code3.setFocus();
          break;
      }
    }
  }
}
