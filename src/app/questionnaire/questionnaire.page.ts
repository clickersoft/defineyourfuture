import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Tour } from '../shared/models/tour';
import { BehaviorSubject, ReplaySubject } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController, ToastController } from '@ionic/angular';
import { NavExtrasService } from '../services/nav-extras.service';
import { ToursService } from '../services/tours.service';
import { Account } from '../shared/models/account';
import { LocalStorageService } from '../auth/local-storage.service';
import { AccountsService } from '../services/accounts.service';
import { RidesService } from '../services/rides.service';
import { ClientRide } from '../shared/models/client-ride';
import { SharedApiService } from '../services/shared-api.service';
import { TranslateService } from '@ngx-translate/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AppSettings } from '../services/appsettings';
import { AuthNetCapPlugin } from 'auth-net-cap'
import { Plugins } from '@capacitor/core';
import { CreditCardValidator } from 'angular-cc-library';
import { Ride } from '../shared/models/ride';
import { Question } from '../shared/models/question';

declare var paypal: any;
var submitRide: ClientRide;
var tour: Tour;
var _ridesService: RidesService;
var loadingOperation : BehaviorSubject<boolean>;
var checkoutNow: (orderId:string,payerID:string,nonce:string)=> void;
var presentToast:(msg:string, color: string)=> void;
var _translate: TranslateService;
var _navCtrl:NavController;
var _toastController:ToastController;
var totalAmount = 0;
@Component({
  selector: 'app-questionnaire',
  templateUrl: './questionnaire.page.html',
  styleUrls: ['./questionnaire.page.scss'],
})
export class QuestionnairePage implements OnInit {

  tourId:string
  tour: Tour;
  ride: ClientRide;
  account:Account
  error:string;
  authNetErrors:string;
  loadingOperation = new BehaviorSubject<boolean>(false);
  loadingOperation2 = new BehaviorSubject<boolean>(false);
  tourPickups = [];
  tourReplaySubject: ReplaySubject<Tour> = new ReplaySubject(1);
  formSubmit = new BehaviorSubject<boolean>(false);
  confirmed = new BehaviorSubject<boolean>(false);
  addScript: boolean = false;
  paypalLoad: boolean = true;
  cardData:CardData;
  questions:Question[];
  @ViewChild('form',{static : false}) form: FormControl;
  @ViewChild('paypal',{static:true}) paypalElemnt : ElementRef
  constructor(private activatedRoute: ActivatedRoute,private router:Router,private navCtrl:NavController,private navExtrasService: NavExtrasService
    ,private toursService:ToursService,private localStorageService: LocalStorageService,
    private accountsService: AccountsService,private ridesService: RidesService,
    private toastController:ToastController,
    private sharedApiService: SharedApiService,
    private translate: TranslateService) {
      this.cardData = { cardNumber : "", expiry:"", cardCode:"" ,fullName:""}
      checkoutNow = this.checkoutNow;
      _ridesService = this.ridesService;
      _translate = this.translate;
      _navCtrl = this.navCtrl;
      loadingOperation = this.loadingOperation;
      presentToast = this.presentToast;
      _toastController = this.toastController;
      submitRide = this.ride = {} as ClientRide;

      //this.tourId = this.activatedRoute.snapshot.paramMap.get('id');

      this.account = { AccountTypeId: '3', Active: true} as Account;
      if(localStorageService.getAccount()){
        this.account = localStorageService.getAccount();
      }
      else{
        this.loadAccount();
      }

      this.toursService.GetQuestions().subscribe(res=>{
        this.questions = res;
        this.loadingOperation.next(false);
        this.tourReplaySubject.next(this.tour);
        this.calcTotalAmount();
      },err=>{
        this.loadingOperation.next(false);
        this.tourReplaySubject.next(null);
  
      }); 
      /*
     if(!this.tourId && this.navExtrasService.getExtras2()){
       //By Transfare
       this.tour = { RequiredFields : "Mobile,Email", PaymentMethod: "3" , CurrencyCode: "USD"} as Tour
       this.ride = this.navExtrasService.getExtras2() as ClientRide
       submitRide = this.ride
       console.log(JSON.stringify(this.ride))
       totalAmount  = this.ride.TotalPrice
       this.totalAmount = this.ride.TotalPrice
      this.configurePaypal();

     }else{
       //By Tour
      if(this.navExtrasService.getExtras()){
       
        this.loadingOperation.next(true);
        this.tourReplaySubject = navExtrasService.getExtras() as ReplaySubject<Tour>;
        
        this.tourReplaySubject.subscribe(res=>{
          if(res){
            this.loadingOperation.next(false);
            tour = this.tour = res;
            if(this.tour._ride){
              submitRide = this.ride = this.tour._ride;
            }
          }else{
            this.loadTour();
          }
          this.calcTotalAmount();
          this.configurePaypal();
        })
      }else{
        tour = this.tour = {} as Tour;
  
        if(this.tourId){
        
          this.loadingOperation.next(true);
          this.loadTour();
        }
      }
      this.loadPickupCities();
      this.calcTotalAmount();
     }
*/
      

  
   }
   loadTour(){
    this.toursService.GetClientTour(this.tourId).subscribe(res=>{
      tour = this.tour = res;
      this.loadingOperation.next(false);
      this.tourReplaySubject.next(this.tour);
      this.calcTotalAmount();
    },err=>{
      this.loadingOperation.next(false);
      this.tourReplaySubject.next(null);

    }); 
   }
  
   loadAccount(){
    this.loadingOperation.next(true);
    this.accountsService.getMyInfo().subscribe(res=>{
      this.account = res;
      this.localStorageService.storeAccount(this.account);
      console.log(this.account);
      this.loadingOperation.next(false);
    },err=>{
      this.loadingOperation.next(false);
    }); 
  }
  backClick(){
    this.navCtrl.navigateBack('dashboard');
  }
  checkoutNow(orderId:string,payerID:string,nonce:string){
    loadingOperation.next(true);
    submitRide.OrderId = orderId;
    submitRide.PayerID = payerID;
    submitRide.Nonce = nonce;
    loadingOperation.next(true);
    _ridesService.confirmReserve(submitRide).subscribe(res=>{
      loadingOperation.next(false);
      let rideId = res;
      submitRide.RideId = res;
      
      presentToast(_translate.instant('your_booking_confirmed'),'success')
      _navCtrl.navigateRoot(`/my-order/${submitRide.RideId}`);
    },err=>{
      let errMsg: string;
      if(err){
        errMsg = err.error;
        console.log(err.error);
      }
      if(errMsg){
        errMsg = `Error, ${errMsg}`;
      }else{
        errMsg = 'Error, Try again later';
      }
      presentToast(errMsg,'danger')
      loadingOperation.next(false);
    });
  }
  confirmReserve(){
    this.formSubmit.next(true)
 
    if(this.form.invalid){
      this.presentToast(this.translate.instant('please_enter_required_fileds'),'danger')
      return;
    }
   

    this.ride.TourId = this.tourId;
    if(+this.tour.PaymentMethod == 2){
      this.cashClick();
    }else{
      this.confirmed.next(true);
    }
  }
  cashClick(){
    this.ride.PaymentMethod = 2;//Cash
    checkoutNow(null,null,null);
  }
  @ViewChild('ccNumber',{static: false}) ccNumber:ElementRef;
  @ViewChild('ccExp',{static: false}) ccExp:ElementRef;
  @ViewChild('ccCVC',{static: false}) ccCVC:ElementRef;
  sendPaymentDataToAnet(){
    let validate_ccNumber = CreditCardValidator.validateCCNumber(this.ccNumber.nativeElement);
    let validate_ccExp = CreditCardValidator.validateExpDate(this.ccExp.nativeElement);
    this.authNetErrors = "";
    
    if(validate_ccNumber){
      this.authNetErrors = this.translate.instant('incorrect_card_number')
      return
    }
    if(validate_ccExp){
      this.authNetErrors = this.translate.instant('incorrect_expiry_date')
      return
    }
    if(!this.ccCVC.nativeElement.value || this.ccCVC.nativeElement.value.length < 3){
      this.authNetErrors = this.translate.instant('incorrect_security_code')
      return
    }
    
    this.loadingOperation.next(true);
    
    //var clientKey = "292kNVFM9de9d245LqZ6Dt73yHtRvhTnW97ZEf6NEtY5D9KUajsM8Bs3Th4Ykj7j";
    //var apiLoginID =  "6S8nG2bwDt";
    //var env = "SANDBOX";
    var clientKey = "7382T5wV5TkWF4te5gjTmSF6PEUXqreWht6Ve8veU3X263gn4kpSyXFqG83Tg4zH";
    var apiLoginID =  "43FrnP4DkS";
    var env = "PRODUCTION";
    var expiryArr = this.ccExp.nativeElement.value.split(' / ');
    var month = expiryArr[0];
    var year = expiryArr[1];
    if(year.length == 4){
      year = year.substring(2,4)
    }
    
    Plugins.AuthNetCap.dispatchData(
      { 
        env:env, 
        clientName:apiLoginID,
        clientKey:clientKey,
        cardNumber: this.ccNumber.nativeElement.value.replace(/\s/g, ""),
        expirationMonth: month,
        expirationYear:year, 
        cardCode: this.ccCVC.nativeElement.value,
        cardHolderName: this.cardData.fullName,
        zipCode: "00970"
      }).then(data=>{
        //this.presentToast("Success","success");
        //this.presentToast(data.descriptor,"success");
        //this.presentToast(data.value,"success");
        this.presentToast("Processing...","success");
        console.log('AuthNet Success:' + JSON.stringify(data));
        this.loadingOperation.next(false);

        this.ride.PaymentMethod = 3;//Paypal or credit card
        this.ride.CardHolderName = this.cardData.fullName;
        checkoutNow(null,null,data.value);
      }).catch(err=>{
        if(err && err.error){
          this.presentToast(err.error,"danger");
        }else{
          this.presentToast("Error, Not valid","danger");
        }
        console.error('AuthNet Error:' + JSON.stringify(err));
        this.loadingOperation.next(false);
      });
      console.log('end AuthNetCap')
  }

  totalAmount = 0;
  calcTotalAmount(){
    return;
    if(this.ride.RideTypeId > 1){
      return;
    }
    this.totalAmount = 0;
    if(this.ride.RideAccounts && this.tour){
      console.log(this.ride.RideAccounts);
      if(this.tour.PricePerPerson){
        this.ride.RideAccounts.forEach(element => {
          if(element.TourFeatureId){
            this.totalAmount += (element.Price || 0)
          }else{
            this.totalAmount += (this.tour.Price || 0)
          }
        });
        this.totalAmount +=  ((this.ride.ChildCount || 0) * (this.tour.ChildPrice || 0))
      }else{
        if(this.ride.RideAccounts[0].TourFeatureId){
          this.totalAmount = (this.ride.RideAccounts[0].Price || 0)
        }else{
          this.totalAmount = (this.tour.Price || 0)
        }
      }
    }
    totalAmount = this.totalAmount;
  }
  ngOnInit() {
  }

  
  async presentToast(msg:string, color: string) {
    const toast = await _toastController.create({
      message: msg,
      duration: 2000,
      color: color
    });
    toast.present();
  }
}
export interface CardData{
  cardNumber:string;
  expiry:string;
  cardCode:string;
  fullName:string;
}