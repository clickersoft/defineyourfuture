import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { CreditCardDirectivesModule } from 'angular-cc-library';
import { QuestionnairePage } from './questionnaire.page';
import { TranslateModule } from '@ngx-translate/core';

const routes: Routes = [
  {
    path: '',
    component: QuestionnairePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    CreditCardDirectivesModule,
    RouterModule.forChild(routes)
  ],
  declarations: [QuestionnairePage]
})
export class QuestionnairePageModule {}
