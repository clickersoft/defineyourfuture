import { Component, OnInit } from '@angular/core';
import { MouseEvent } from '@agm/core';
import { NavParams, ModalController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { marker } from '../shared/models/marker';
import { AgmDirection } from 'agm-direction/src/modules/agm-direction.module';

declare var google;
@Component({
  selector: 'app-google-maps',
  templateUrl: './google-maps.component.html',
  styleUrls: ['./google-maps.component.scss'],
})
export class GoogleMapsComponent implements OnInit {

 // google maps zoom level
 zoom: number = 8;
  
 // initial center position for the map
 title: string = "Map";
 lat: number = 31.8858323;
 lng: number = 34.7315881;n
 myMap: AgmDirection
 public renderRoute:any
 public origin: any;
public destination: any;
 clickedMarker(label: string, index: number) {
   console.log(`clicked the marker: ${label || index}`)
 }
 
 mapClicked($event: MouseEvent) {
   /*
   this.markers.push({
     lat: $event.coords.lat,
     lng: $event.coords.lng,
     draggable: true
   });
   */
 }
 
 markerDragEnd(m: marker, $event: MouseEvent) {
  // console.log('dragEnd', m, $event);
 }
 
 markers: marker[] = [
   /*
   {
     lat: 51.673858,
     lng: 7.815982,
     label: 'A',
     draggable: true
   },
   {
     lat: 51.373858,
     lng: 7.215982,
     label: 'B',
     draggable: false
   },
   {
     lat: 51.723858,
     lng: 7.895982,
     label: 'C',
     draggable: true
   }
   */
 ]
 //private modalController: ModalController,private params: NavParams,
 constructor() {
     //private activatedRoute: ActivatedRoute
   //this.title = this.activatedRoute.snapshot.paramMap.get("title");
   //this.lat = +this.activatedRoute.snapshot.paramMap.get("lat");
   //this.lng = +this.activatedRoute.snapshot.paramMap.get("lng");
   
   //this.title = this.params.get("title");
   //this.lat = +this.params.get("lat");
   //this.lng = +this.params.get("lng");
  /* 
   this.markers.push(
     {
       lat: this.lat,
       lng: this.lng,
       label: this.title,
       draggable: false

   } as marker)
   */
  }

 ngOnInit() {
 }

 back(){
  // this.modalController.dismiss();
 }

  public addMarker(lat: number, lng: number,label:string): marker {

    let marker =  {
      lat: lat,
      lng: lng,
      label: label,
      draggable: false

      } as marker;

    this.markers.push(marker)
    return marker

/*
      let latLng = new google.maps.LatLng(lat, lng);

      let marker = new google.maps.Marker({
          map: this.map,
          animation: google.maps.Animation.DROP,
          position: latLng
      });

      this.markers.push(marker);
*/
  }
  public removeMarker(marker:any): void {
    this.markers = this.markers.filter(m => m !== marker);
  }
  public drawDirection(origin:marker,destination:marker){
    this.origin = origin;
    this.destination = destination;
    if(this.renderRoute){
      console.log(JSON.stringify(this.renderRoute));
    }else{
      console.log('hhhhhhhhh')
    }

    this.getDistancia(origin,destination)
  }
  public getDistancia(origen, destino) {
    return new google.maps.DistanceMatrixService().getDistanceMatrix({'origins': [origen], 'destinations': [destino], travelMode: 'DRIVING'}, (results: any) => {
      //console.log(JSON.stringify(results));  
      console.log('resultados distancia (mts) -- ', results.rows[0].elements[0].distance.value)
    });
}

  calculateDistance() {
    if(this.origin && this.destination){
      const fromM = new google.maps.LatLng(this.origin.lat,this.origin.lng);
      const toM = new google.maps.LatLng(this.destination.lat,this.destination.lng);
      return google.maps.geometry.spherical.computeDistanceBetween(fromM, toM) / 1000;
    }
    return null;
  }

  public removeDirection(){
    this.origin = null;
    this.destination = null;
  }
}

