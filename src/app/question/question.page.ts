import { Component, OnInit, ViewChild, ElementRef, Renderer2, AfterViewInit, Inject, Injectable, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Search } from '../shared/models/search';
import { IonContent, ToastController, ModalController } from '@ionic/angular';
import { DOCUMENT } from '@angular/common';
import { BehaviorSubject, Observable, ReplaySubject } from 'rxjs';
import { AccountsService } from '../services/accounts.service';
import { Account } from '../shared/models/account';
import { NavExtrasService } from '../services/nav-extras.service';
import * as moment from 'moment';
import { SharedApiService } from '../services/shared-api.service';
import { ToursService } from '../services/tours.service';
import { HttpEventType } from '@angular/common/http';
import { Question } from '../shared/models/question';
import { Answer } from '../shared/models/answer';
import { AnswerPage } from '../answer/answer.page';

@Injectable({
  providedIn: 'root'
})
@Component({
  selector: 'app-question',
  templateUrl: './question.page.html',
  styleUrls: ['./question.page.scss'],
})
export class QuestionPage implements OnInit,AfterViewInit {
  selectedTab = 'details';
  itemsList: Question[] = [];
  loadingOperation = new BehaviorSubject<boolean>(false);

  constructor(@Inject(DOCUMENT) private document: Document,
  private toastController: ToastController,
  private sharedApiService: SharedApiService,
  private router: Router,
  private toursService: ToursService,
  private chRef: ChangeDetectorRef,
  private modalController : ModalController) 
  {
    this.loadingOperation.next(true);
    this.sharedApiService.GetQuestionnaire().subscribe(res=>{
      this.itemsList = res;
      this.loadingOperation.next(false);
    },err=>{
      this.loadingOperation.next(false);
    }); 
  }

  selectedItem: Question;
  createItem(){
    this.selectedItem = {
      
    } as Question
  }
  submitItem(){
    
    const submitedItem = this.selectedItem;
    let msg : string;
    if(submitedItem.QuestionId){
      msg = 'Edited Successfully'
    }else{
      msg = 'Created Successfully'
      this.itemsList = [...this.itemsList,submitedItem];
    }
    

    this.loadingOperation.next(true);
    this.sharedApiService.createOrUpdateQuestion(submitedItem)
    .subscribe(res=>{
      this.loadingOperation.next(false);
      this.presentToast(msg,'success')
      this.selectedItem = null;
      if(res){
        submitedItem.QuestionId = res;
      }
      
    },err=>{
      let errMsg: string;
      if(err){
        errMsg = err.error;
        console.log(err.error);
      }
      if(errMsg){
        errMsg = `Error, ${errMsg}`;
      }else{
        errMsg = 'Error, Try again later';
      }
      this.presentToast(errMsg,'danger')
      this.loadingOperation.next(false);
      if(!submitedItem.QuestionId){
        this.itemsList = this.itemsList.filter(event => event !== submitedItem);
      }
    });    
    
  }
  discardItem(){
    this.selectedItem = null;
  }
  selectItem(selected){
    this.selectedItem = selected;
  }
  deleteItem(){
    const deletedItem = this.selectedItem;
    this.loadingOperation.next(true);

    this.itemsList = this.itemsList.filter(event => event !== deletedItem);

    this.sharedApiService.deleteQuestion(deletedItem)
    .subscribe(res=>{
      this.presentToast('Deleted Successfully','success')
      this.loadingOperation.next(false);
      this.selectedItem = null;
    },err=>{
      let errMsg: string;
      if(err){
        errMsg = err.error;
        console.log(err.error);
      }
      if(errMsg){
        errMsg = `Error, ${errMsg}`;
      }else{
        errMsg = 'Error, Try again later';
      }
      this.presentToast(errMsg,'danger')
      this.loadingOperation.next(false);
      this.itemsList = [...this.itemsList,deletedItem];
    });    
  }
  ngAfterViewInit() {
  }

 
  
  ngOnInit() {
  }

  removeAnswer(answer:Answer){
    this.selectedItem.Answer = this.selectedItem.Answer.filter(acc => acc !== answer);
    //this.calcTotalAmount();
  }
  addAnswer(){
    if(!this.selectedItem.Answer){
      this.selectedItem.Answer = [];
    }
    this.selectedItem.Answer.push({} as Answer);
    
    //this.calcTotalAmount();
    this.chRef.detectChanges();
  }
  async presentToast(msg:string, color: string) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
      color: color
    });
    toast.present();
  }

  async openAnswerPage(answer){
    const modal = await this.modalController.create({
      component : AnswerPage,
      componentProps : {
        answer: answer
      }
    });
    modal.onDidDismiss().then((obj)=>{
      if(obj.data){
        //this.fetchEvents();
      }
      console.log("dismisted");
    });
    modal.present();
  }
}
