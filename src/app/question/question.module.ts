import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { QuestionPage } from './question.page';
import { QuestionPageRoutingModule } from './question.router.module';
import { TranslateModule } from '@ngx-translate/core';
import { AnswerPageModule } from '../answer/answer.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    QuestionPageRoutingModule,
    AnswerPageModule
  ],
  declarations: [QuestionPage]
})
export class QuestionPageModule {}
