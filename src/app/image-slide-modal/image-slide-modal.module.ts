import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ImageSlideModalPage } from './image-slide-modal.page';

const routes: Routes = [
  {
    path: 'image-slide-modal',
    component: ImageSlideModalPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ImageSlideModalPage]
})
export class ImageSlideModalPageModule {}
