import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-image-slide-modal',
  templateUrl: './image-slide-modal.page.html',
  styleUrls: ['./image-slide-modal.page.scss'],
})
export class ImageSlideModalPage implements OnInit {

  slideOptsProgressbar = {
    initialSlide: 0,
    slidesPerView: 1,
    autoplay:false
  };
  
  images:[];
  constructor(private modalController: ModalController,private params: NavParams) { }

  ngOnInit() {
    this.images = this.params.get('images');
  }

  close(){
    this.modalController.dismiss();
  }

}
