import { Component, OnInit, Renderer2 } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountsService } from 'src/app/services/accounts.service';
import { NavExtrasService } from 'src/app/services/nav-extras.service';
import { ToastController } from '@ionic/angular';
import { Observable, BehaviorSubject, ReplaySubject } from 'rxjs';
import { SharedApiService } from 'src/app/services/shared-api.service';
import { Lookup } from 'src/app/shared/models/lookup';
import * as moment from 'moment';
import { LookupTypes } from 'src/app/shared/enums/lookup-types.enum';
import { Account } from 'src/app/shared/models/account';
import { GsCountry } from 'src/app/shared/models/gs-country';
import { CarType } from 'src/app/shared/models/car-type';
import { OAuthService } from 'angular-oauth2-oidc';
import { LocalStorageService } from '../auth/local-storage.service';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from '../services/appsettings';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {

  account:Account
  loadingOperation = new BehaviorSubject<boolean>(false);
  loadingOperation2 = new BehaviorSubject<boolean>(false);
  loadingOperation3 = new BehaviorSubject<boolean>(false);
  countries:GsCountry[] = [];
  carTypes:CarType[] = [];
  error: string;
  claims:object;
  maxBirthDate = moment().format(AppSettings.DATE_MAX_DATE_FORMAT);
  tourId: string
  constructor(private activatedRoute: ActivatedRoute,private rd: Renderer2,
    private accountsService: AccountsService,
    private sharedApiService: SharedApiService,
    private navExtrasService: NavExtrasService,
    private toastController: ToastController,
    private oauthService: OAuthService,
    private localStorageService: LocalStorageService,
    private router:Router,
    private translate:TranslateService) 
    { 
      this.claims = oauthService.getIdentityClaims()
      this.tourId = this.activatedRoute.snapshot.paramMap.get('id');
      if(this.claims){
        this.presentToast(this.claims['name'] || 'No Name',"primary");
      }
   
      this.account = { AccountTypeId: '3', Active: true} as Account;
      if(localStorageService.getAccount()){
        this.account = localStorageService.getAccount();
      }
      else{
        this.loadAccount();
      }
    

       this.loadingOperation2.next(true);
       this.sharedApiService.GetCountries().subscribe(res=>{
         this.countries = res;
        this.loadingOperation2.next(false);
       },err=>{
        this.loadingOperation2.next(false);
       });

       this.sharedApiService.GetCarTypes().subscribe(res=>{
        this.carTypes = res;
      },err=>{
      });
    }

    loadAccount(){
      this.loadingOperation.next(true);
      this.accountsService.getMyInfo().subscribe(res=>{
        this.account = res;
        this.localStorageService.storeAccount(this.account);
        console.log(this.account);
        this.loadingOperation.next(false);
      },err=>{
        this.loadingOperation.next(false);
      }); 
    }
    submitAccount(){
      this.accountsService.updateClientInfo(this.account).subscribe(res=>{
        this.loadingOperation2.next(false);
        this.loadingOperation3.next(false);
        this.localStorageService.storeAccount(this.account);
        this.presentToast(this.translate.instant('saved_successfully'),'success')
        if(this.tourId){
          this.router.navigateByUrl( `/tour-reserve/${this.tourId}`)
        }
       },err=>{
         let errMsg: string;
         if(err){
           errMsg = err.error;
           console.log(err.error);
         }
         if(errMsg){
           errMsg = `Error, ${errMsg}`;
         }else{
           errMsg = 'Error, Try again later';
         }
       this.presentToast(errMsg,'danger')
        this.loadingOperation2.next(false);
        this.loadingOperation3.next(false);
       });
    }

    async presentToast(msg:string, color: string) {
      const toast = await this.toastController.create({
        message: msg,
        duration: 2000,
        color: color
      });
      toast.present();
    }

  ngOnInit() {

  }

}
