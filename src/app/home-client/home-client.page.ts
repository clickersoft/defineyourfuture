import { Component, OnInit, Renderer2, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { IonInfiniteScroll, ToastController } from '@ionic/angular';
import { ToursService } from '../services/tours.service';
import { Router } from '@angular/router';
import { SortModes } from '../shared/enums/sort-modes.enum';
import { SortDirections } from '../shared/enums/sort-directions.enum';
import { Tour } from '../shared/models/tour';
import { NavExtrasService } from '../services/nav-extras.service';
import { TranslateService } from '@ngx-translate/core';
import { Plugins } from '@capacitor/core';

@Component({
  selector: 'app-home-client',
  templateUrl: './home-client.page.html',
  styleUrls: ['./home-client.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomeClientPage implements OnInit {
  @ViewChild('navbar',{static: false}) navbar:ElementRef;
  @ViewChild('logo',{static: false}) logo:ElementRef;
  loading = new BehaviorSubject<boolean>(false);
  @ViewChild(IonInfiniteScroll,{static : false}) infinite : IonInfiniteScroll;

  slideOptsProgressbar = {
    initialSlide: 0,
    slidesPerView: 1,
    autoplay:false
  };
  selectedTab = "daily_tour";
  selectedPlace = "0";
  constructor(private rd: Renderer2,private toursService : ToursService,private router: Router,
    private navExtrasService: NavExtrasService,private toastController: ToastController,private translate:TranslateService) { }

  private customProgressBar(current: number, total: number): string {
    const ratio: number = current / total;

    const progressBarStyle: string = 'style=\'transform: translate3d(0px, 0px, 0px) scaleX(' + ratio + ') scaleY(1); transition-duration: 300ms;\'';
    const progressBar: string = '<span class=\'swiper-pagination-progressbar-fill\' ' + progressBarStyle + '></span>';

    let progressBarContainer: string = '<div class=\'swiper-pagination-progressbar\' style=\'height: 4px; top: 6px; width: 100%;\'>';
    progressBarContainer += progressBar;
    progressBarContainer += '</span></div>';

    return progressBarContainer;
}

  testData: any[] = [0,1,2,3,4,5,6];

  slides = [];
  unives = [];
  ngOnInit(){
    this.loadSearch(false,event);
  }
  mainBtnClick(cat:any){
    if(cat == 'events' || cat == 'packages' || cat == 'tours'){
      if(cat == 'tours'){
        cat = 'all';
      }
      this.router.navigateByUrl(`/packages/${cat}`)
    }else{
      if(cat == 'car_rentals'){
        this.router.navigateByUrl(`/test`)
      }else{
        this.router.navigateByUrl(`/chick-chack-web`)
      }
    }
  
  }
  toggleSection(index){
    this.unives[index].open = !this.unives[index].open;
    if(this.unives[index].open){
      this.unives.filter((item,itemIndex) => itemIndex != index)
      .map(item=>item.open = false);
    }
  }
  
  tabChanged(ev: any){
    this.selectedTab = ev.detail.value;
    this.loadSearch();
  }
  placeChanged(ev: any){
    this.selectedTab = 'all';
    this.selectedPlace = ev.detail.value;
    this.loadSearch();
  }
  doRefresh(event){
    this.loadSearch(false,event);
  }
 
  tourClick(tour:Tour){
    this.navExtrasService.setExtras(tour);
    this.router.navigateByUrl(`/tour/${tour.TourId}`)
  }
  loadSearch(loadMore = false, event?){
    var tourCategory = 0;
    if(this.selectedTab == 'daily_tour'){
      tourCategory = 1
    }else if(this.selectedTab == 'business'){
      tourCategory = 2
    }else if(this.selectedTab == 'vip'){
      tourCategory = 3
    }else if(this.selectedTab == 'multi_days'){
      tourCategory = 4
    }
    this.loading.next(true);
    this.toursService.GetUniv(this.selectedPlace).subscribe(res=>{
      this.loading.next(false);
      //this.slides = res.filter(slide => slide.Slider).sort((a, b) => (a.TourDescription != '#outside#') ? 1 : -1);
      this.unives = res;
      if(event){
        try{
          event.target.complete();
        }
        catch(err){
          
        }
      }
    },(err)=>{
      if(err){
        console.log(err);
      }
      this.presentToast(this.translate.instant('Internet_connection_lost'),'danger')
      //
      //this.
      if(event){
        try{
          event.target.complete();
        }
        catch(err){
          
        }
      }
      this.loading.next(false);
    });
  }
  async presentToast(msg:string, color: string) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
      color: color
    });
    toast.present();
  }
  scrolling(event) {
    var windowTop = event.detail.scrollTop;
    console.log(windowTop);

   // var flex = Number.parseFloat(document.getElementById("box-1").style.flex);
    var newFlex = windowTop / 100.0;
    newFlex = newFlex + 3.0;
    document.getElementById("box-1").style.flex = newFlex + '';

    if (windowTop > 150 || windowTop > 150) {
      //document.getElementById("navbar").style.padding = "30px 10px";
     
      document.getElementById("navbar").style.height = "80px";
      document.getElementById("logo").style.fontSize = "25px";
    } else {
      //document.getElementById("navbar").style.padding = "80px 10px";
      document.getElementById("navbar").style.height = "400px";
      document.getElementById("logo").style.fontSize = "35px";
    }

    //windowTop > 100 ? this.rd.addClass(this.myNav.nativeElement, 'navShadow') : this.rd.removeClass(this.myNav.nativeElement, 'navShadow');
    //windowTop > 100 ? this.rd.setStyle(this.myUl.nativeElement, 'top','100px') : this.rd.setStyle(this.myUl.nativeElement, 'top','180px');
  }

  onSlideClick(tour:Tour){
    if(tour.TourDescription && tour.TourDescription.startsWith('#outside#')){
      this.router.navigateByUrl(`/transfer`)
    }else{
      this.navExtrasService.setExtras(tour);
      this.router.navigateByUrl(`/tour/${tour.TourId}`)
    }
  }

  test(){
    var clientKey = "292kNVFM9de9d245LqZ6Dt73yHtRvhTnW97ZEf6NEtY5D9KUajsM8Bs3Th4Ykj7j";
    var apiLoginID =  "6S8nG2bwDt";
    var env = "SANDBOX";
    //var clientKey = "7382T5wV5TkWF4te5gjTmSF6PEUXqreWht6Ve8veU3X263gn4kpSyXFqG83Tg4zH";
    //var apiLoginID =  "43FrnP4DkS";
    //var env = "PRODUCTION";
    var month = "06";
    var year = "22";
    this.presentToast("start AuthNetCap...","success");
    console.log('start AuthNetCap')

var cardData= "370000000000002"
  var cardCode= "1234"
    Plugins.AuthNetCap.dispatchData(
      { 
        env:env, 
        clientName:apiLoginID,
        clientKey:clientKey,
        cardNumber: cardData,
        expirationMonth: month,
        expirationYear:year, 
        cardCode: cardCode,
        cardHolderName: "Alaa Najjar",
        zipCode: "00970"
      }).then(data=>{
        //this.presentToast("Success","success");
        //this.presentToast(data.descriptor,"success");
        //this.presentToast(data.value,"success");
        this.presentToast("Processing...","success");
        console.log('AuthNet Success:' + JSON.stringify(data));

      }).catch(err=>{
        this.presentToast("Error","danger");
        console.log('AuthNet Error:' + JSON.stringify(err));
      });
      console.log('end AuthNetCap')
  }
}
