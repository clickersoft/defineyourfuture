import { Inject, Injectable } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { Account } from '../shared/models/account';
import { BehaviorSubject, ReplaySubject } from 'rxjs';
import { ClientRide } from '../shared/models/client-ride';
import { Plugins } from '@capacitor/core';
import { OAuthStorage } from 'angular-oauth2-oidc';
const { Storage } = Plugins;

export interface Clinic {
  Company_ID: string;
  User_Name: string;
  Password: string;
  FullName: string;
  Company_Name: string;
  User_ID: string;
  User_Image: string;
  Company_Image: string;
  User_Type_ID: string;
}



@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {
  public static ACCOUNT_STORAGE_KEY: string = 'ACCOUNT';
  public static CLIENT_RIDE_STORAGE_KEY: string = 'CLIENT_RIDE';
  public static accountBehaviorSubject = new BehaviorSubject<Account>(null);
  public static accountReplaySubject: ReplaySubject<Account> = new ReplaySubject(1);
  constructor(@Inject(LOCAL_STORAGE) private storage: StorageService) { }

  public storeAccount(account: Account): void {
    LocalStorageService.accountBehaviorSubject.next(account);
    this.storage.set(LocalStorageService.ACCOUNT_STORAGE_KEY, account);
  }
  public getAccountBehaviorSubject(){
    return LocalStorageService.accountBehaviorSubject;
  }
  public getAccount(): Account {
    return this.storage.get(LocalStorageService.ACCOUNT_STORAGE_KEY) || null;
  }
  public clearAccount(){
    this.storage.remove(LocalStorageService.ACCOUNT_STORAGE_KEY);
  }

  public storeClientRide(clientRide: ClientRide): void {
    this.storage.set(LocalStorageService.CLIENT_RIDE_STORAGE_KEY, clientRide);
  }
  public getClientRide(): ClientRide {
    return this.storage.get(LocalStorageService.CLIENT_RIDE_STORAGE_KEY) || null;
  }

  async setOauthData(obj: any) {
    await Storage.set({
      key: 'oauth',
      value: JSON.stringify(obj)
    });
  }
  
  // JSON "get" example
  async getOauthData() {
    const ret = await Storage.get({ key: 'oauth' });
    const oauth = JSON.parse(ret.value);
    return oauth;
  }

  setItem(key:string,value:string) {
    this.storage.set(key, value);
  }
  
  getItem(key:string) {
    return this.storage.get(key) || null;
  }
  
  removeItem(key:string) {
    this.storage.remove(key);
  }
  
  
  async setItem2(key:string,value:string) {
    await Storage.set({
      key: key,
      value: value
    });
  }
  
  async getItem2(key:string) {
    const value = await Storage.get({ key: key });
    console.log('Got item ' + key + ' : ', value);
    return value;
  }
  
  async removeItem2(key:string) {
    await Storage.remove({ key: key });
  }
  
}
