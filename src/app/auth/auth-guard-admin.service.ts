import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { OAuthService } from 'angular-oauth2-oidc';
import { AppComponent } from '../app.component';
import { LocalStorageService } from './local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardAdminService implements CanActivate {

  constructor(private oauthService: OAuthService, private router: Router,private localStorageService:LocalStorageService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean>  {
      return new Promise<boolean>(resolve => {
        let account = this.localStorageService.getAccount();
        console.log(account);
        this.localStorageService.getAccountBehaviorSubject().subscribe(res=>{
          if(this.oauthService.hasValidAccessToken()){
            let claims = this.oauthService.getIdentityClaims()
            if(claims){
              switch(claims['role'] || '--'){
                case 'admin':
                  resolve(true);
                  return
                  case 'driver':
                  resolve(true);
                  return
              }
            }
          }
          
          this.router.navigate(['/login']);
          resolve(false);
        });
    });
  }
}
