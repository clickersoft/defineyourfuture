import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, Router, RouterStateSnapshot, Resolve } from '@angular/router';
import { OAuthService } from 'angular-oauth2-oidc';
import { AppComponent } from '../app.component';
import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;

@Injectable({
  providedIn: 'root'
})
export class AuthResolverService implements Resolve<any> {

  constructor() { }

  resolve(route:ActivatedRouteSnapshot){
    console.log('22222222222222');
    return Storage.get({key:"oAuth"})
  }
}
