import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { OAuthService } from 'angular-oauth2-oidc';
import { AppComponent } from './../app.component';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private oauthService: OAuthService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    var hasValidAccessToken = this.oauthService.hasValidAccessToken();
    //console.log('hasValidAccessToken:' + hasValidAccessToken);
    if (hasValidAccessToken) {
      return true;
    }
    //this.oauthService.initImplicitFlow();
    this.router.navigate(['login']);
    return false;
  }
}
