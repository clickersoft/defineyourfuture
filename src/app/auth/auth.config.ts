import { AuthConfig } from 'angular-oauth2-oidc';

export const authConfig: AuthConfig = {
  issuer: 'http://dfaccounts.clickersoft.com',
  
  oidc: false,
  // URL of the SPA to redirect the user to after login
  //redirectUri: window.location.origin + '/signin-oidc',

  // The SPA's id. The SPA is registered with this id at the auth-server
  //clientId: 'DefineYourFuture_web_local',
  clientId: 'defineyourfuture',
  
  // set the scope for the permissions the client should request
  // The first three are defined by OIDC. The 4th is a usecase-specific one
  scope: 'openid profile defineyourfutureApi offline_access',


  requireHttps : false,
  // Url of the Identity Provider
  //issuer: 'https://demo.identityserver.com/identity',

  // Login Url of the Identity Provider
  //loginUrl: 'https://demo.identityserver.com/identity/connect/authorize',

  // Login Url of the Identity Provider
  //logoutUrl: window.location.origin + '/signout-oidc',
  //postLogoutRedirectUri: window.location.origin + '/signout-oidc',

  // URL of the SPA to redirect the user to after login
  //redirectUri: window.location.origin + '/dashboard.html',

  // The SPA's id. The SPA is registerd with this id at the auth-server
  //clientId: 'billing_demo',

  // set the scope for the permissions the client should request
  // The first three are defined by OIDC. Also provide user sepecific
  //scope: 'openid profile email billing_demo_api',
}
