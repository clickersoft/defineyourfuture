import { Injectable} from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { AppComponent } from './../app.component';
import { LocalStorageService } from './../auth/local-storage.service';


@Injectable({
  providedIn: 'root'
})
export class GlobService implements CanActivate {

  constructor(private router: Router, private localStorageService: LocalStorageService) { }
  
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return true;
    /*
    if (this.localStorageService.getClinic() && this.localStorageService.getClinic().User_Name
    && this.localStorageService.getClinic().Password) {
      return true;
    }
    this.router.navigate(['initial']);
    return false;
    */
  }
}
