import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HomePage } from './home.page';
import { TranslateModule } from '@ngx-translate/core';
import { AuthResolverService } from '../auth/auth-resolver';
import { AuthSwitchService } from '../auth/auth-switch.service';

const routes: Routes = [
  {
    path: '',
    component: HomePage,
    children: [
      { path: 'dashboard', loadChildren: () => import('./../dashboard-client/dashboard-client.module').then(m => m.DashboardClientPageModule) },
      //{ path: 'dashboard', loadChildren: () => import('./../dashboard/dashboard.module').then(m => m.DashboardPageModule),canActivate: [AuthSwitchService] },
     
      {
        path: 'admin',
        children: [
          { path: 'accounts', loadChildren: () => import('./../accounts/accounts.module').then(m => m.AccountsPageModule) },
          { path: 'account', loadChildren: () => import('./../account/account.module').then(m => m.AccountPageModule) },
          { path: 'account/:id', loadChildren: () => import('./../account/account.module').then(m => m.AccountPageModule) },
          { path: 'edit-account',loadChildren: () => import('./../account/edit-account/edit-account.module').then(m => m.EditAccountPageModule) },
          { path: 'edit-account/:id', loadChildren: () => import('./../account/edit-account/edit-account.module').then(m => m.EditAccountPageModule) },
          { path: 'tours', loadChildren: () => import('../major-admin/search-major.module').then(m => m.SearchTourPageModule) },
          { path: 'edit-tour',loadChildren: () => import('../major-admin/edit-major/edit-major.module').then(m => m.EditTourPageModule) },
          { path: 'edit-tour/:id', loadChildren: () => import('../major-admin/edit-major/edit-major.module').then(m => m.EditTourPageModule) },
          { path: 'question', loadChildren: () => import('./../question/question.module').then(m => m.QuestionPageModule) },
          { path: 'countries', loadChildren: () => import('./../country/country.module').then(m => m.CountryPageModule) },
        ]
      },
      { path: 'edit-profile', loadChildren: './../edit-profile/edit-profile.module#EditProfilePageModule' },
      { path: 'my-order/:id', loadChildren: './../my-order/my-order.module#MyOrderPageModule' },

      { path: '', redirectTo: 'dashboard', pathMatch: 'full' }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HomePage]
})
export class HomePageModule {}
