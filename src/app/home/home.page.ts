import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';
import { OAuthService } from 'angular-oauth2-oidc';
import { LocalStorageService } from '../auth/local-storage.service';
import { Account } from '../shared/models/account';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  pages = [
    {
      title: 'accounts',
      url: '/admin/accounts',
      icon:'people'
    },
    {
      title: 'majors',
      url: '/admin/tours',
      icon:'map'
    },
    {
      title: 'questionnaire',
      url: '/admin/question',
      icon:'list'
    },
    {
      title: 'universities',
      url: '/admin/countries',
      icon:'business'
    }
  ];
  selectedPath = '';
  loginStatus = "";
  constructor(private oauthService: OAuthService,private localStorageService:LocalStorageService,
    private router: Router) {
    this.router.events.subscribe((event: RouterEvent) =>{
      if(event && event.url)
      {
        //console.log(event.url);

        this.selectedPath = event.url;
      }
    });
   }

   ionViewWillEnter(){
    let account = this.localStorageService.getAccount();
    console.log(account);
    this.selectTab(account);
    let firstTime = true;
    this.localStorageService.getAccountBehaviorSubject().subscribe(res=>{
      if(firstTime){
        firstTime = false;
        return
      }
     this.selectTab(res);
    });
  }
  selectTab(account:Account){
    this.loginStatus = "anonymous";
    if(this.oauthService.hasValidAccessToken()){
      let claims = this.oauthService.getIdentityClaims()
      this.loginStatus = 'client';
      if(claims){
        switch(claims['role'] || '--'){
          case 'admin':
            this.loginStatus = 'admin';
            return
            case 'driver':
              this.loginStatus = 'driver';
            return
        }
      }
    }
    console.log(this.loginStatus);
  }

  ngOnInit() {
  }

}
