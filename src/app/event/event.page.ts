import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams, ToastController, LoadingController, PopoverController } from '@ionic/angular';
import { SearchModalPage } from '../search/search.page';
import { BehaviorSubject } from 'rxjs';
import * as moment from 'moment';
import { CalendarEvent } from 'calendar-utils';
import { Lookup } from '../shared/models/lookup';
import { SharedApiService } from '../services/shared-api.service';
import { LookupTypes } from '../shared/enums/lookup-types.enum';
import { Search } from '../shared/models/search';
import { AppointmentsService } from '../services/appointments.service';
import { IonColor, ColorService } from '../services/color-service';
import { ColorPopoverPage } from '../utils/color-popover/color-popover.page';
import { AppSettings } from '../services/appsettings';

@Component({
  selector: 'app-event',
  templateUrl: './event.page.html',
  styleUrls: ['./event.page.scss'],
})
export class EventPage implements OnInit {
  appointment: Appointment = {} as Appointment;
  endTimeValues: number[];
  appointmentTypes:Lookup[] = [];
  public color : IonColor;
  DATE_FORMAT: any;
  constructor(private modalController: ModalController,private params: NavParams,private sharedApiService: SharedApiService,
    private appointmentsService: AppointmentsService,private toastController: ToastController,
    private loadingController: LoadingController,private colorService: ColorService,private popoverController: PopoverController) 
  { 
    this.DATE_FORMAT = AppSettings.DATE_FORMAT;
    //this.sharedApiService.GetLookups(LookupTypes.AppointmentType).subscribe(res=>{
    //  this.appointmentTypes = res;
    //});
    this.defaultValues();
    if(this.params.get("event")){
      let event = this.params.get("event") as CalendarEvent<{ appointment: Appointment }>
      if(event.meta.appointment.Appointment_ID){
        this.appointment = event.meta.appointment;
        if(event.color){
          this.color = {
            key: 'key_' + event.color.primary.replace('#',''),
            value: event.color.primary,
            friendlyName: event.color.primary
          }
          this.colorService.addIonColor(this.color.key,this.color.value);
        }
        //this.appointment.Start_Time =moment(this.appointment.Start_Time,'hh:mm:ss').format('hh:mm');
        //this.appointment.End_Time =moment(this.appointment.End_Time,'hh:mm:ss').format('hh:mm');

        console.log('editEvent');
        return;
      }
      else{
        console.log('New Event with Some values');
      

        if(event.start && event.end){
          console.log('New Event Start & End');
          this.appointment.Appointment_Date = event.start.toISOString();
          this.appointment.Start_Time =moment(event.start).format('hh:mm');
          this.appointment.End_Time =moment(event.end).format('hh:mm');
        }else if(event.start){
          this.appointment.Appointment_Date = event.start.toISOString();
        }
      }
    }else{
      console.log('New Event with default values');
    }
  }
  async pickColor(ev:Event){
    let mypopover = await this.popoverController.create(
      {
      component: ColorPopoverPage,
      event: ev,
      componentProps: {
        color: this.color
      }
    });
  
    mypopover.onDidDismiss()
        .then((data) => {
          const x = data['data']; // Here's returned value from popover
          this.color = x || this.color;
      });
    return await mypopover.present();
  }
  
  defaultValues() {
    let sMinutes = 0;
    if(moment().minutes() > 30){
      sMinutes = 30;
    }
    let sTime = moment(moment().format('hh'),'hh');
    sTime.add(sMinutes,'minutes');
    this.appointment.Start_Time =sTime.format('hh:mm');
    this.appointment.End_Time = sTime.add(30,'minutes').format('hh:mm');

    this.appointment.Appointment_Date = moment().toISOString();
    this.color = this.colorService.colorList[3];
  }
  
  appTypeChange(){
    let appType = this.appointmentTypes.find(ele=>{
      return this.appointment.Appointment_Type_ID == ele.ID;
    });

    if(appType && appType.ExtraData){
      this.color = {
        key: 'key_' + (appType.ExtraData.length > 7 ? appType.ExtraData.slice(3) : appType.ExtraData.slice(1)),
        value: ('#' + (appType.ExtraData.length > 7 ? appType.ExtraData.slice(3) : appType.ExtraData.slice(1))),
        friendlyName: ('#' + (appType.ExtraData.length > 7 ? appType.ExtraData.slice(3) : appType.ExtraData.slice(1)))
      }
      this.colorService.addIonColor(this.color.key,this.color.value);
    }
  }

  ngOnInit() {
    this.startTimeChange(null);
  }
  startTimeChange(e){
    let startHour = 0;
    this.endTimeValues = [];

    if(this.appointment.Start_Time){
      startHour = moment(this.appointment.Start_Time,'hh:mm').hours();
    }

    for(var h:number = startHour; h<24; h++){
      this.endTimeValues = [...this.endTimeValues,h];
    }
    console.log();
  }
  clearAccountSearch(){
    this.appointment.PF_Name = '';
    this.appointment.PS_Name = '';
    this.appointment.PT_Name = '';
    this.appointment.PL_Name = '';
    this.appointment.Mobile = '';
   // this.appointment.AccountFullName = '';
   // this.appointment.AccountHalfName = '';
  //  this.appointment.Account_ID = ''; 
  }
  async searchAccount(){
    const modal = await this.modalController.create({
      component : SearchModalPage,
      componentProps : {
        view: 'Account'
      }
    });
    modal.onDidDismiss().then((obj)=>{
      console.log("dismisted");
      if(obj.data){
        let searchObj = obj.data as Search;

        this.appointment.PF_Name = searchObj.F_Name;
        this.appointment.PS_Name = searchObj.S_Name;
        this.appointment.PT_Name = searchObj.T_Name;
        this.appointment.PL_Name = searchObj.L_Name;
        this.appointment.Mobile = searchObj.Mobile;
       // this.appointment.AccountFullName = searchObj.FullName;
       // this.appointment.AccountHalfName = searchObj.halfName;
      //  this.appointment.Account_ID = searchObj.Account_ID; 
        console.log();
      }
    });
    modal.present();
  }
  async submitForm(){

    if(this.color){
      this.appointment.Color = this.color.value;
    }
    

    let loadMsg = 'Creating...'
    if(this.appointment.Appointment_ID){
      loadMsg = 'Editing...';
    }
  
    const loading = await this.loadingController.create({
      message: loadMsg,
      duration: 0,
      backdropDismiss:true
    });
    loading.present();


    this.appointmentsService.createOrUpdate(this.appointment).subscribe(x=>{
      let msg : string;
      if(this.appointment.Appointment_ID){
        msg = 'Edited Successfully'
      }else{
        msg = 'Created Successfully'
      }
      this.presentToast(msg,'success')
      this.modalController.dismiss('success');
      loading.dismiss();
    },(err=>{
      let errMsg: string;
      if(err){
        errMsg = err.error;
        console.log(err.error);
      }
      if(errMsg){
        errMsg = `Error, ${errMsg}`;
      }else{
        errMsg = 'Error, Try again later';
      }
      this.presentToast(errMsg,'danger')
      loading.dismiss();
    }));
  }
  loadingCtrlObj: any;
  

  async presentToast(msg:string, color: string) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
      color: color
    });
    toast.present();
  }
  back(){
    this.modalController.dismiss();
  }
}
