import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { EventPage } from './event.page';
import { SearchPageModule } from '../search/search.module';
import { SearchModalPage } from '../search/search.page';
import { ColorPopoverPageModule } from '../utils/color-popover/color-popover.module';
import { TranslateModule } from '@ngx-translate/core';

const routes: Routes = [
  {
    path: 'eventPage',
    component: EventPage
  },
  {
    path:'searchModalPage',
    component:SearchModalPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ColorPopoverPageModule,
    TranslateModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EventPage,SearchModalPage]
})
export class EventPageModule {}
