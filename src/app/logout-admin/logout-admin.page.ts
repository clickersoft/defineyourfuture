import { Component, OnInit } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { Router } from '@angular/router';
import { LocalStorageService } from '../auth/local-storage.service';

@Component({
  selector: 'app-logout-admin',
  templateUrl: './logout-admin.page.html',
  styleUrls: ['./logout-admin.page.scss'],
})
export class LogoutAdminPage implements OnInit {

  constructor(private router: Router, private localStorageService: LocalStorageService,
    private oauthService: OAuthService) {
    this.oauthService.logOut(true);
    this.logout();
   }

   logout(){
    this.router.navigateByUrl('/login-admin');
   }
  ngOnInit() {
    
  }
}
