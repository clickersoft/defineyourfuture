export enum LookupTypes {
    Gender = 1,
    MaritalStatus,
    Nationality,
    Country,
    City,
    Insurance,
    OfficalDocument,
    MedicalHistory,
    Drug,
    JobType,
    PatientImageState,
    AppointmentType,
    Referral
}
