export enum RideStatuss {
    All = 0,
    New = -1,
    Planned = -2,
    Finished = -3,
    
    Created = 1,
    Paid = 2,
    Assigned = 3,
    Pickup = 4,
    Dropoff = 5,
    CanceledByClient = 6,
    CanceledByManager = 7,
}
