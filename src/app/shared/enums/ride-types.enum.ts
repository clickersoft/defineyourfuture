export enum RideTypes {
    OneDayTour = 1,
    Transfer = 2,
    ByHour = 3,
}
