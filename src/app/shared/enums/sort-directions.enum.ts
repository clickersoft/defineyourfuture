export enum SortDirections {
    Descending = 0,
    Ascending = 1
}
