export enum AccountTypes {
    Manager = 2,

    Driver = 3,

    Client = 4,
}
