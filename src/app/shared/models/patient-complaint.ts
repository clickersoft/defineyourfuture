export class PatientComplaint {
    Patient_Complaint_ID: string;
    Patient_Treatment_Plan_ID?: number;
    Doctor_ID: string;
    Create_User_ID: string;
    Complaint: string;

    Enter_Date: string;
    Complaint_Date: string;
    Modify_Date: string;

    Patient_ID: string;
}
