export class PatientImages {
    Patient_Images_ID: string;

	The_Date: string;
	Patient_Image_State_ID: number;
	Patient_Image_State: string;
	Comments: string;
	GS_Image_ID: string;
	Image_Name: string;
}
