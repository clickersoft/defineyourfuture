import { RideAccount } from './ride-account';
import { Tour } from './tour';

export class ClientRide {
    RideId: string
    Notes :string
    TourId  :string
    CouponCode : string
    Mobile : string
    PickupCityId : string
    ChildCount: number
    TotalPrice: number
    RideStatusId: string
    RideStatusName: string
    TourName: string
    RideDate: string
    dayName: string
    TourImage:string
    Email:string
    TourType:number
    TourCategory:number
    OrderId:string
    PayerID:string
    Nonce:string
    Concierge:string
    PaymentMethod:number
    CardHolderName:string
    Tour:Tour
    RideAccounts: RideAccount[]

    PickupLocation: string;
    DropoffLocation: string;
    PickupLat: string;
    PickupLng: string;
    DropoffLat: string;
    DropoffLng: string;
    PickupSign: string;
    RideTypeId: number;
    RideTime: string;
    ClientReferenceNumber: string;
    RideHours: number;
    CarTypeId: string;
    PickupTime: string;
    Pax: number;
    Cargo: number;
    FlightNo: string;
    OrderNumber: string;
    TourFeatureId: string;
    FeatureName: string;
    ClientName: string;
    Language: string;
    
    distance: number;
}
