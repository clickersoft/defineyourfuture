import { TourCarPrice } from './tour-car-price';
import { TourLocation } from './tour-location';
import { TourAttachment } from './tour-attachment';
import { TourPickup } from './tour-pickup';
import { ClientRide } from './client-ride';
import { TourFeature } from './tour-feature';

export interface Tour {
    TourId: string;
    TourTitle: string;
    TourSubtitle: string;
    TourDescription: string;
    TourImage: string;
    TourVideo: string;
    Active: boolean;
    PublishDate: string;
    TourType:string;
    TourPlace:string;
    TourCategory:string;
    PaymentMethod:string;
    CurrencyCode:string;
    CurrencySymbol:string;
    CountryCode:string;
    EndDate: string;
    MaxPax: number;
    Deleted: boolean;
    Price: number;
    OldPrice?:number;
    ChildPrice:number;
    Slider: boolean;
    TourDate: string;
    imageGroup:string;
    RequiredFields:string;
    TourFeatureImage:string;
    PricePerPerson: boolean;
    TourNumber: string ;
    Universities: string ;
    
    TourAttachment: TourAttachment[];
    TourCarPrice: TourCarPrice[];
    TourLocation: TourLocation[];
    TourPickup: TourPickup[];
    TourFeature: TourFeature[];
    TourPickup2: string[];
    RequiredFields2: string[];
    Universities2: string[];

    dayName: string;
    
    _ride:ClientRide

    _CurrentTourFeature: TourFeature;

}
