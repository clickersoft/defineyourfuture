export interface GsCountry {
    GsCountryId: string;
    CountryName: string;
    CountryCode: string;
    PhoneCode: string;
    Deleted: boolean;
}
