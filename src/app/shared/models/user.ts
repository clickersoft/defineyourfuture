interface User {
    User_ID: string;
    F_Name: string;
    S_Name: string;
    T_Name: string;
    L_Name: string;
  
    User_Name: string;
    Image: string;
    Mobile: string;
    User_Type_ID: string;
    GS_Religion_ID: string;
    GS_Marital_Status_ID: string;
    Gender: string;
    Birth_Date: string;
    Hire_date: string;
    Birth_Country: string;
    GS_Image_ID: string;
    Job_Type_ID: string;
    Email: string;
    Admin: string;
    Active: string;
    Salary: string;
    Parent_Organization_ID: string;
    Note: string;
    Company_ID: string;
  }