export class PatientDrugHistory {
    DD_Drug_ID: string;
	Patient_ID: string;
	Details: string;
	DrugName: string;

	Date: string;
}
