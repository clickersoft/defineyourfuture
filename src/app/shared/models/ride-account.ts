export class RideAccount {
    RideAccountId:string;
    AccountId: string;
    FullName:string;
    RideId: string;
    IdNumber: string;
    IsChild: boolean;
    RideAccountTypeId: string;
    BirthDate:string;
    TourFeatureId:number;
    Price:number;
}
