export interface CarType {
    CarTypeId: string;
    Name: string;
    ModelName: string;
    ImageName: string;
    Description: string;
    MaxPax: number;
    MaxCargo: number;
    InitialPrice: number;
    DistancePrice: number;
    HourPrice: number;
    Deleted: boolean;
    CarTypeCatId: string;
    CarTypeCatName: string;
    TotalPrice: number;
    CalculatedDistance: number;
    //this.selectedCarType
}
