export interface Account {
    AccountId: string;
    Title: string;
    IdNumber: string;
    Email: string;
    Mobile: string;
    GsCountryId: string;
    GsCityId: string;
    Password: string;
    FacebookSocialId: string;
    AccountTypeId: string;
    Nickname: string;
    ImageName: string;
    Active: boolean;
    Deleted: boolean;
    CreateDate: string;
    LastLoginDate: string;
    CountryCode: string;


    CarTypeId: string;
    DriverLicense: string;
    AttachmentName: string;
    Driver_Active: boolean;
    CityName: string;

    FullName:string;

    bookForSomeoneElse
}
