import { TourCarPrice } from './tour-car-price';
import { TourLocation } from './tour-location';
import { TourAttachment } from './tour-attachment';
import { TourPickup } from './tour-pickup';
import { ClientRide } from './client-ride';
import { TourFeature } from './tour-feature';
import { Tour } from './tour';
import { Answer } from './answer';

export interface Question {


    QuestionId: string;
    QuestionText: string;
    QuestionType: string;
    QuestionOrder: number;
    
    Answer: Answer[];
}
