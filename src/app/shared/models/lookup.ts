import { LookupTypes } from '../enums/lookup-types.enum';

export class Lookup {
    ID: string;
    Name: string;
    GroupID: string;
    GroupName: string;
    LookupID: LookupTypes;
    ExtraData: string;
}
