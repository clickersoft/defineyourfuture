import { RideStatuss } from '../enums/ride-statuss.enum';

export interface Ride {
    RideId: string;
    RideDate: string;
    RideTime: string;
    RideTypeId: number;
    PickupSign: string;
    RideCode: string;
    ClientReferenceNumber: string;
    PickupLocation: string;
    DropoffLocation: string;
    PickupLat: string;
    PickupLng: string;
    DropoffLat: string;
    DropoffLng: string;
    RideHours: number;
    TourId: string;
    AccountClientId: string;
    RideClientId: string;
    CarTypeId: string;
    Notes: string;
    Price: number;
    DriverId: string;
    PickupTime: string;
    DropoffTime: string;
    CancelReason: string;
    RideStatusId: string;
    Pax: number;
    Cargo: number;
    Concierge: string;

    DriverName: string;
    CarTypeName: string;
    RideClientName: string;
    ClientName: string;
    RideTypeName: string;
    TourTitle: string;
    RideStatusName: string;

    ClientMobile: string;
    ClientEmail: string;

    RideClientMobile: string;
    RideClientEmail: string;

    DriverMobile: string;
    CurrencySymbol: string;
    CurrencyCode: string;
    FlightNo: string;
    
    distance: number;
}
