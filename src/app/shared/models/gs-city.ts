export interface GsCity {
    GsCityId: string;
    CityName: string;
    GsCountryId: string;
    Lat: string;
    Lng: string;
    Deleted: boolean;
}
