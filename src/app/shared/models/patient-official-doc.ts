export class PatientOfficialDoc {
    Patient_Paper_ID: string;

	GS_Attachment_ID: string;

	GS_Official_Doc_ID: string;

	Issue_Place: string;

	Paper_Num: string;

	OffDoc: string;

	Issue_Date: string;

	Issue_End_Date: string;
}
