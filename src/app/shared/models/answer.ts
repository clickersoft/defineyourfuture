import { TourCarPrice } from './tour-car-price';
import { TourLocation } from './tour-location';
import { TourAttachment } from './tour-attachment';
import { TourPickup } from './tour-pickup';
import { ClientRide } from './client-ride';
import { TourFeature } from './tour-feature';
import { Tour } from './tour';
import { AnswerMajor } from './answer-major';

export interface Answer {

    AnswerId: number;
    QuestionId: number;
    AnswerText: string;
    AnswerOrder: number;
    
    AnswerMajor: AnswerMajor[];
}
