import { TourCarPrice } from './tour-car-price';
import { TourLocation } from './tour-location';
import { TourAttachment } from './tour-attachment';
import { TourPickup } from './tour-pickup';
import { ClientRide } from './client-ride';
import { TourFeature } from './tour-feature';
import { Tour } from './tour';

export interface AnswerMajor {

    AnswerId: number;
    MajorId: number;
    MatchPoint: number;
}
