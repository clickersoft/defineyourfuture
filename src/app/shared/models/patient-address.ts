export class PatientAddress {
    
    Patient_Address_ID: string;

    Address: string;

    Emergency_Phone: string;

    GS_City_ID: string;

    GS_Town_ID: number;

    Land_Phone: string;

    P_Box: string;

    Second_Mobile: string;

    Second_Social_Mail: string;

    Social_Mail: string;

    Street: string;

    City: string;

    Town: string;

    Country: string;

    GS_Country_ID: string;
}
