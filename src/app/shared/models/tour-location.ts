export interface TourLocation {
    TourLocationId: string;
    TourId: string;
    OrderNumber: number;
    LocationName: string;
    Lat: string;
    Lng: string;
    LocationImage: string;
    LocationVideo: string;
    Description: string;
}
