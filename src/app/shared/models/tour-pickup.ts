export interface TourPickup {
    GsCityId: string;
    GsCountryId: string;
    CityName: string;
    CountryName: string;
}
