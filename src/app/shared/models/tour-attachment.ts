export interface TourAttachment {
    TourAttachmentId: number;
    Description: string;
    AttachmentUrl: string;
    AttachmentType: string;
}
