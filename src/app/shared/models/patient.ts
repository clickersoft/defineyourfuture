import { PatientMedicalHistory } from './patient-medical-history';
import { PatientAddress } from './patient-address';
import { PatientOfficialDoc } from './patient-official-doc';
import { PatientDrugHistory } from './patient-drug-history';
import { PatientImages } from './patient-images';
import { PatientBalance } from './patient-balance';
import { PatientName } from './patient-name';
import { PatientComplaint } from './patient-complaint';
import { Search } from './search';

export class Patient {
Patient_ID: string;

F_Name: string;

S_Name: string;

T_Name: string;

L_Name: string;

Enter_Date: string;

Image: string;

Mobile: string;

Mobile2 : string;
Gender: string;//number

Amount: number;

Currency_Rate: number;

Birth_Date : string;

Age: number;
Job : string;
GS_Nationality_ID: string;

Email: string;

GS_Religion_ID: number;

GS_Marital_Status_ID: string;//number

Note: string;
Patient_Number : string;
GS_Referral_ID : string;

Co_Insurance : number;
Patient_Code : string;
Confirmed : boolean;

Modify_Date : string;

TotalRejectedPayments: number;


TotalPayments: number;


TotalDiscounts: number;


TotalFee: number;


TotalBalance: number;


MainCurrencySymbol: string;


//#region Insurance


File_No: string;

DD_Insurance_ID: number;

Insurance_No: string;

HealthCardNo: string;

Insurance_Register_Date: string;

Insurance_End_Date: string;

Insurance_RegExpires: string;

Insurance_Enter_Date: string;


FullName: string;
halfName: string;

//region Other


Patient_Medical_History: PatientMedicalHistory[];


Patient_Address: PatientAddress[];


Patient_OfficialDoc: PatientOfficialDoc[];


Patient_DrugHistory: PatientDrugHistory[];


Patient_Images: PatientImages[];


Patient_Balance: PatientBalance[];
Patient_Name : PatientName[];
Patient_Complaint : PatientComplaint[];

public static CloneSearch(search: Search){
    if(!search){
        return null;
    }
    let patient: Patient;
    patient = {
        Patient_ID: search.Patient_ID,
        F_Name : search.F_Name,
        L_Name : search.L_Name,
        Image : search.Image,
        Mobile : search.Mobile,
        S_Name : search.S_Name,
        T_Name : search.T_Name,

        FullName: search.FullName,
        halfName: search.halfName,
    } as Patient;

    return patient;
}
}
