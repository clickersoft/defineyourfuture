export interface TourCarPrice {
    CarTypeId: number;
    TourId: string;
    Price: number;
}
