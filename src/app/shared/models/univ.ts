import { TourCarPrice } from './tour-car-price';
import { TourLocation } from './tour-location';
import { TourAttachment } from './tour-attachment';
import { TourPickup } from './tour-pickup';
import { ClientRide } from './client-ride';
import { TourFeature } from './tour-feature';
import { Tour } from './tour';

export interface Univ {
    UniversityId: string;
    Name: string;
    Description: string;
    Place: string;
    
    Majors: Tour[];

    open:boolean;
}
