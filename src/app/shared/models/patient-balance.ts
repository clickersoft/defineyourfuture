export class PatientBalance {
    Patient_Balance_ID: number;

	The_Date: string;
	Title: string;
	Amount: number;
	Vat: number;
	Details: string;
	Type: string;//Treatment , Ortho , Payment
	Doctor_Name: string;
	Doctor_ID: string;
	Clinic_ID: string;
	User_ID: string;

	Currency_Code: string;
	Currency_Rate: number;
	Cheque_Number: string;
	Balance_Method_ID: number;
	Collected: number;

	DD_Area_Selection_Type_ID?: number;
	DD_Treatment_Phase_ID: number;
	Treatment_State_ID?: number;
	Treatment_Type_ID?: number;


	Plan_Date: string;
	Invoice_Number: string;
	Invoice_Number_Prefix: string;
}
