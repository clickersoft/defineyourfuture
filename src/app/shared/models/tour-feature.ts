export interface TourFeature {
    TourFeatureId: number;
    TourId: string;
    FeatureName: string;
    ShortName: string;
    Description: string;
    Price: number;
    Active: boolean;
    FeatureImage: string;
    OrderNumber: number;

    rideAccountCount: number;
}
