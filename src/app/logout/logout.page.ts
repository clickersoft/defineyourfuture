import { Component, OnInit } from '@angular/core';
import { LocalStorageService, Clinic } from '../auth/local-storage.service';
import { Router } from '@angular/router';
import { OAuthService } from 'angular-oauth2-oidc';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.page.html',
  styleUrls: ['./logout.page.scss'],
})
export class LogoutPage implements OnInit {

  constructor(private router: Router, private localStorageService: LocalStorageService,
    private oauthService: OAuthService) {
    this.oauthService.logOut(true);
    this.logout();
   }

   logout(){
    this.router.navigateByUrl('/dashboard/login');
   }
  ngOnInit() {
    
  }

}
