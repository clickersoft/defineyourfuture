import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides, NavController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-get-started',
  templateUrl: './get-started.page.html',
  styleUrls: ['./get-started.page.scss'],
})
export class GetStartedPage implements OnInit {
  slidesOpts = {
    initialSlide: 1,
    speed: 400
  }
  @ViewChild(IonSlides,{static : false}) ionSlides : IonSlides;
  slides = [
    {
      img: 'https://ionicframework.com/docs/demos/api/slides/slide-1.png',
      titulo: 'Visible Changes<br>in 3 weeks'
    },
    {
      img: 'https://ionicframework.com/docs/demos/api/slides/slide-2.png',
      titulo: 'Forget about<br>strict diet'
    }
  ];


  constructor(private navCtrl: NavController) { }

  ngOnInit() {
  }
  slideNext(){
    this.ionSlides.slideNext();
  }
  finish(){
    this.navCtrl.navigateRoot(['dashboard']);
  }
}
