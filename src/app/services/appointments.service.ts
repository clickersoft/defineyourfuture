import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map, catchError  } from 'rxjs/operators';
import { AppSettings } from './appsettings';
import { LocalStorageService } from '../auth/local-storage.service';
import * as moment from 'moment';
import { ParametersEncoder } from '../shared/utils/parameters-encoder';

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
};


@Injectable({
  providedIn: 'root'
})
export class AppointmentsService {
  constructor(private httpClient: HttpClient,private localStorageService:LocalStorageService) { }

  public createOrUpdate(appointment: Appointment)
  {
    if(appointment.Start_Time && moment(appointment.Start_Time,'hh:mm:ss',true).isValid()){
      appointment.Start_Time = moment(appointment.Start_Time,'hh:mm:ss').format('hh:mm');
    }
    if(appointment.End_Time && moment(appointment.End_Time,'hh:mm:ss',true).isValid()){
      appointment.End_Time = moment(appointment.End_Time,'hh:mm:ss').format('hh:mm');
    }

    const params = new HttpParams({encoder: new ParametersEncoder()})
    .set('LangID','en-GB')
    .set('AppointmentID',appointment.Appointment_ID || '')
    .set('Unique_ID',appointment.Unique_ID || '')
    .set('DoctorID',appointment.Doctor_ID || '')
    .set('PatientID',appointment.Patient_ID || '')
    .set('Color',appointment.Color || '')
    .set('Appointment_Type_ID',appointment.Appointment_ID || '')
    .set('Appointment_Date',appointment.Appointment_Date ? moment(appointment.Appointment_Date).utc().format(AppSettings.DATE_ISO_FORMAT) : '')
    .set('TimeFrom',appointment.Start_Time || '')
    .set('TimeTo',appointment.End_Time || '')
    .set('Title',appointment.Title || '')
    .set('IsSmsApp','false')
    .set('SmsTimeMode','1')
    .set('SmsTimeValue','1')

    /*
    .set(
      'primary_release_date.gte',
      format(getStart(this.viewDate), AppSettings.DATE_FORMAT)
    )
    .set(
      'primary_release_date.lte',
      format(getEnd(this.viewDate), AppSettings.DATE_FORMAT)
    )
    .set('api_key', '0ec33936a68018857d727958dca1424f');
*/
    return this.httpClient
    .get(AppSettings.API_ENDPOINT + `SubmitAppointment`, { params ,responseType:'text'})
    .pipe(
      map((results:string) => {
        return results;
      }));
  }

  public delete(id: string)
  {
    const params = new HttpParams({encoder: new ParametersEncoder()})
    .set('LangID','en-GB')
    .set('AppointmentID',id)

    /*
    .set(
      'primary_release_date.gte',
      format(getStart(this.viewDate), AppSettings.DATE_FORMAT)
    )
    .set(
      'primary_release_date.lte',
      format(getEnd(this.viewDate), AppSettings.DATE_FORMAT)
    )
    .set('api_key', '0ec33936a68018857d727958dca1424f');
*/
    return this.httpClient
    .get(AppSettings.API_ENDPOINT + `DeleteAppointment`, { params ,responseType:'text'})
    .pipe(
      map((results:string) => {
        return results;
      }));
  }
  public sendAppointmentSms(appointmentID: string)
  {
    const params = new HttpParams({encoder: new ParametersEncoder()})
    .set('LangID','en-GB')
    .set('AppointmentID',appointmentID)

    /*
    .set(
      'primary_release_date.gte',
      format(getStart(this.viewDate), AppSettings.DATE_FORMAT)
    )
    .set(
      'primary_release_date.lte',
      format(getEnd(this.viewDate), AppSettings.DATE_FORMAT)
    )
    .set('api_key', '0ec33936a68018857d727958dca1424f');
*/
    return this.httpClient
    .get(AppSettings.API_ENDPOINT + `SendAppointmentSms`, { params ,responseType:'text'})
    .pipe(
      map((results:string) => {
        return results;
      }));
  }
  

  public getById(id: number){}

  public getAll(actions,dateFrom:moment.Moment,dateTo:moment.Moment,year = 0,month = 0)
  {
    const params = new HttpParams({encoder: new ParametersEncoder()})
    .set('LangID','en-GB')
    .set('Year', year.toString())
    .set('Month', month.toString())
    .set('DateFrom',dateFrom ? moment(dateFrom).utc().format(AppSettings.DATE_ISO_FORMAT) : '')
    .set('DateTo', dateTo ? moment(dateTo).utc().format(AppSettings.DATE_ISO_FORMAT) : '')
    
    return this.httpClient
    .get(AppSettings.API_ENDPOINT + `GetAppointments`, { params })
    .pipe(
      map((results: Appointment[]) => {
        return results.map((appointment: Appointment) => {
          return {
            title: appointment.Title && appointment.PatientHalfName ? appointment.Title + ' - ' + appointment.PatientHalfName :
              (appointment.Title ? appointment.Title : (appointment.PatientHalfName ? appointment.PatientHalfName : (appointment.Appointment_Type_Name ? appointment.Appointment_Type_Name : 'Empty'))),
            start: this.setAppointmentTime(new Date(
              appointment.Appointment_Date
            ), appointment.Start_Time),
            end: this.setAppointmentTime(new Date(appointment.Appointment_Date), appointment.End_Time),
            color: appointment.Color ? { primary: ('#' + (appointment.Color.length > 7 ? appointment.Color.slice(3) : appointment.Color.slice(1))), secondary: ('#' + (appointment.Color.length > 7 ? appointment.Color.slice(3) : appointment.Color.slice(1))) }  : colors.yellow,
            allDay: false,
            draggable: false,
            resizable: {
              beforeStart: true,
              afterEnd: true
            },
            cssClass: this.contrastClass(appointment.Color ? ('#' + (appointment.Color.length > 7 ? appointment.Color.slice(3) : appointment.Color.slice(1))) : ''),
            meta: {
              appointment
            },
          };
        });
      })
    )
  }

  public setAppointmentTime(date: Date, timeStr: string): Date {
    if (timeStr) {
      var time = moment(timeStr, 'HH:mm:ss');
      //console.log(time.hours() + ":" + time.minutes());
      date.setHours(time.hours(), time.minutes(), 0, 0);
    }
    return date;
  }

  contrastColor(hex: string) : string
  {
    if(!hex){
      return '#454545';
    }
    hex = hex.replace('#','');
    let red = parseInt(hex.substring(0,2), 16);
    let green = parseInt(hex.substring(2,4), 16);
    let blue = parseInt(hex.substring(4,6), 16);

      if ((red*0.299) + (green*0.587) + (blue*0.114) > 186)
      {
          //dark color
          console.log('black');
          //Return white
         // return 'black-color';
          return '#454545';
          //return (SolidColorBrush)new BrushConverter().ConvertFromString("#");
      }
      else
      {
          //light color
          console.log('light');
          //Return black
         // return 'white-color';
         return '#f1f1f1';
         // return (SolidColorBrush)new BrushConverter().ConvertFromString("#f1f1f1");
         
      }
  }
  contrastClass(hex: string) : string
  {
    if(!hex){
      return '';
    }
    hex = hex.replace('#','');
    let red = parseInt(hex.substring(0,2), 16);
    let green = parseInt(hex.substring(2,4), 16);
    let blue = parseInt(hex.substring(4,6), 16);

      if ((red*0.299) + (green*0.587) + (blue*0.114) > 186)
      {
          //dark color
          console.log('black');
          //Return white
          return 'black-app';
          //return (SolidColorBrush)new BrushConverter().ConvertFromString("#");
      }
      else
      {
          //light color
          console.log('light');
          //Return black
          return 'white-app';
         // return (SolidColorBrush)new BrushConverter().ConvertFromString("#f1f1f1");
         
      }
  }
}
