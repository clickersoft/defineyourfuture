import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LocalStorageService } from '../auth/local-storage.service';
import { Platform } from '@ionic/angular';

const LNG_KEY = 'SELECTED_LANGUAGE';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {
  selected = '';
  constructor(private translate: TranslateService, private localStorageService: LocalStorageService, private plt: Platform) { }

  setInitialAppLanguage(){
    let language = this.translate.getBrowserLang();
    this.translate.setDefaultLang(language);

    this.localStorageService.getItem2(LNG_KEY).then(val =>{
      if(val && val.value){
        this.setLanguage(val.value);
        this.selected = val.value;
      }
    });
  }

  getLanguages(){
    return [
      { text: 'English', value: 'en', img: 'assets/images/en.png'},
      { text: 'العربية', value: 'ar', img: 'assets/images/ar.png'},
    ]
  }

  setLanguage(lng){
    this.translate.use(lng);
    this.selected = lng;
    this.localStorageService.setItem2(LNG_KEY,lng);
  }
}
