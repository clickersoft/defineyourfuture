import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError  } from 'rxjs/operators';
import { AppSettings } from './appsettings';

@Injectable({
  providedIn: 'root'
})
export class CompaniesService {

  constructor(private httpClient: HttpClient) { }
  public getById(id: number){}

  public getAll()
  {
    return this.httpClient.get(AppSettings.API_ENDPOINT + 'GetCompanies?LangID=en-GB&Company_ID=')
    .pipe(
      map((results: Company[]) => {
        return results;
      })
    )
  }
}
