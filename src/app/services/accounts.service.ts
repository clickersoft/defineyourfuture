import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map, catchError  } from 'rxjs/operators';
import { AppSettings } from './appsettings';
import { LocalStorageService } from '../auth/local-storage.service';
import * as moment from 'moment';
import { ParametersEncoder } from '../shared/utils/parameters-encoder';
import { Account } from '../shared/models/account';
import { SortModes } from '../shared/enums/sort-modes.enum';
import { SortDirections } from '../shared/enums/sort-directions.enum';

@Injectable({
  providedIn: 'root'
})
export class AccountsService {

  constructor(private httpClient: HttpClient,private localStorageService:LocalStorageService) { }

  public GetAccounts(searchTxt: string, accountTypeId: number, gsCountryId: number,active?: boolean,fromIndex =0, pageSize = 9,sortMode = SortModes.ByDefault, sortDirection = SortDirections.Descending)
  {
    const params = new HttpParams({encoder: new ParametersEncoder()})
    .set('SearchText', searchTxt)
    .set('AccountTypeId', accountTypeId.toString())
    .set('GsCountryId', gsCountryId.toString())
    .set('Active', active ? active.toString() : '')
    .set('FromIndex', fromIndex.toString())
    .set('PageSize', pageSize.toString())
    .set('SortMode', sortMode.valueOf().toString())
    .set('SortDirection', sortDirection.valueOf().toString())

    return this.httpClient.get(AppSettings.API_ENDPOINT + `Manager/GetAccounts`, { params })
    .pipe(
      map((results: Account[]) => {
        return results;
      }),
      map(results=>{
        return results.map((item,index)=>{
          item.AccountTypeId = item.AccountTypeId + '';
          item.GsCountryId = item.GsCountryId + '';
          item.CarTypeId = item.CarTypeId + '';
          return item;
        });
      })
    )
  }
  
  public createOrUpdateAccount(account: Account)
  {
    return this.httpClient
    .post(AppSettings.API_ENDPOINT + `Manager/SubmitAccount`,account, { responseType:'text'})
    .pipe(
      map((results:string) => {
        return results;
      }));
  }
  public updateClientInfo(account: Account)
  {
    return this.httpClient
    .post(AppSettings.API_ENDPOINT + `Client/SubmitAccount`,account, { responseType:'text'})
    .pipe(
      map((results:string) => {
        return results;
      }));
  }
  /*
  public createOrUpdateComplaint(accountComplaint: AccountComplaint)
  {
    const params = new HttpParams({encoder: new ParametersEncoder()})
    .set('LangID','en-GB')
    .set('Username',this.localStorageService.getClinic().User_Name)
    .set('Password',this.localStorageService.getClinic().Password)
    .set('Company_ID',this.localStorageService.getClinic().Company_ID)

    .set('AccountComplaintID',accountComplaint.Account_Complaint_ID || '')
    .set('AccountID',accountComplaint.Account_ID || '')
    .set('Account_Treatment_Plan_ID',accountComplaint.Account_Treatment_Plan_ID != null ? accountComplaint.Account_Treatment_Plan_ID.toString() : '')
    .set('Doctor_ID',accountComplaint.Doctor_ID || this.localStorageService.getClinic().User_ID)
    .set('Complaint',accountComplaint.Complaint || '')
    .set('Complaint_Date',accountComplaint.Complaint_Date ? moment(accountComplaint.Complaint_Date).utc().format(AppSettings.DATE_ISO_FORMAT) : '')

    return this.httpClient
    .get(AppSettings.API_ENDPOINT + `SubmitAccountComplaint`, { params ,responseType:'text'})
    .pipe(
      map((results:string) => {
        return results;
      }));
  }

  public deleteComplaint(id: string)
  {
    const params = new HttpParams({encoder: new ParametersEncoder()})
    .set('LangID','en-GB')
    .set('Username',this.localStorageService.getClinic().User_Name)
    .set('Password',this.localStorageService.getClinic().Password)
    .set('Company_ID',this.localStorageService.getClinic().Company_ID)

    .set('AccountComplaintID',id)

    return this.httpClient
    .get(AppSettings.API_ENDPOINT + `DeleteAccountComplaint`, { params ,responseType:'text'})
    .pipe(
      map((results:string) => {
        return results;
      }));
  }
*/
  public GetAccount(id: string)
  {
    const params = new HttpParams({encoder: new ParametersEncoder()})
    .set('AccountID',id)
    
    return this.httpClient.get(AppSettings.API_ENDPOINT + `Manager/GetAccount`, { params })
    .pipe(
      map((item: Account) => {
        item.AccountTypeId = item.AccountTypeId + '';
        item.GsCountryId = item.GsCountryId + '';
        item.CarTypeId = item.CarTypeId + '';
        //if(item.CreateDate){
        //  item.CreateDate = moment(item.CreateDate).format(AppSettings.DATE_FORMAT);
        //}
        return item;
      })      
    )
  }
  public getMyInfo()
  {
    return this.httpClient.get(AppSettings.API_ENDPOINT + `Client/MyInfo`)
    .pipe(
      map((item: Account) => {
        item.AccountTypeId = item.AccountTypeId + '';
        item.GsCountryId = item.GsCountryId + '';
        item.CarTypeId = item.CarTypeId + '';
        item.GsCityId = item.GsCityId + '';
        //if(item.CreateDate){
        //  item.CreateDate = moment(item.CreateDate).format(AppSettings.DATE_FORMAT);
        //}
        return item;
      })      
    )
  }
}
