import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map, catchError  } from 'rxjs/operators';
import { AppSettings } from './appsettings';
import { LocalStorageService } from '../auth/local-storage.service';
import * as moment from 'moment';
import { of } from 'rxjs';
import { GsCountry } from '../shared/models/gs-country';
import { CarType } from '../shared/models/car-type';
import { Tour } from '../shared/models/tour';
import { GsCity } from '../shared/models/gs-city';
import { AppContent } from '../shared/models/app-content';
import { ParametersEncoder } from '../shared/utils/parameters-encoder';
import { Univ } from '../shared/models/univ';
import { Question } from '../shared/models/question';

@Injectable({
  providedIn: 'root'
})
export class SharedApiService {

  constructor(private httpClient: HttpClient,private localStorageService:LocalStorageService) { }
  public responseCache= new Map();
  public GetCountries()
  {
    const URL = AppSettings.API_ENDPOINT + `Shared/GetCountries`;

    const responseFromCache = this.responseCache.get(URL);
    if (responseFromCache) {
      return of(responseFromCache);
    }

    const response = this.httpClient.get(URL)
    .pipe(
      map((results: GsCountry[]) => {
        return results;
      }));
      response.subscribe(res=> this.responseCache.set(URL,res));
    return response;
  }
  public GetUniversities()
  {
    const URL = AppSettings.API_ENDPOINT + `Shared/GetUniversities`;

    const responseFromCache = this.responseCache.get(URL);
    if (responseFromCache) {
      return of(responseFromCache);
    }

    const response = this.httpClient.get(URL)
    .pipe(
      map((results: Univ[]) => {
        return results;
      }));
      response.subscribe(res=> this.responseCache.set(URL,res));
    return response;
  }
  public GetCities()
  {
    const URL = AppSettings.API_ENDPOINT + `Shared/GetCities`;

    const responseFromCache = this.responseCache.get(URL);
    if (responseFromCache) {
      return of(responseFromCache);
    }

    const response = this.httpClient.get(URL)
    .pipe(
      map((results: GsCity[]) => {
        return results;
      }));
      response.subscribe(res=> this.responseCache.set(URL,res));
    return response;
  }
  public GetAppContent(tag: string)
  {
    const URL = AppSettings.API_ENDPOINT + `Client/GetAppContent?tag=` + tag;

    const responseFromCache = this.responseCache.get(URL);
    if (responseFromCache) {
      return of(responseFromCache);
    }

    const response = this.httpClient.get(URL)
    .pipe(
      map((results: AppContent) => {
        return results;
      }));
      response.subscribe(res=> this.responseCache.set(URL,res));
    return response;
  }
  public GetCarTypes()
  {
    const URL = AppSettings.API_ENDPOINT + `Shared/GetCarTypes`;

    const responseFromCache = this.responseCache.get(URL);
    if (responseFromCache) {
      return of(responseFromCache);
    }

    const response = this.httpClient.get(URL)
    .pipe(
      map((results: CarType[]) => {
        return results;
      }),
      map(results=>{
        return results.map((item,index)=>{
          item.CarTypeCatId = item.CarTypeCatId + '';
          switch(item.CarTypeCatId){
            case '1':
                item.CarTypeCatName = 'Business Class';
              break;
              case '2':
                  item.CarTypeCatName = 'Business Van/SUV';
                break;
              case '3':
                  item.CarTypeCatName = 'First Class';
                break;
          }
          return item;
        });
      }));
     
      response.subscribe(res=> this.responseCache.set(URL,res));
    return response;
  }
  public GetQuestionnaire()
  {
    const URL = AppSettings.API_ENDPOINT + `Shared/GetQuestionnaire`;

    const responseFromCache = this.responseCache.get(URL);
    if (responseFromCache) {
      return of(responseFromCache);
    }

    const response = this.httpClient.get(URL)
    .pipe(
      map((results: Question[]) => {
        return results;
      }),
      map(results=>{
        return results.map((item,index)=>{
          item.QuestionType = item.QuestionType + '';
          return item;
        });
      }));
     
      response.subscribe(res=> this.responseCache.set(URL,res));
    return response;
  }
  
  public GetCarTypesWithPrices(RideTypeId: number,RideHours: number,PickupLat: string,PickupLng: string,DropoffLat: string,DropoffLng: string)
  {
    const params = new HttpParams({encoder: new ParametersEncoder()})
    .set('RideTypeId', RideTypeId + "")
    .set('RideHours',RideHours + "")
    .set('PickupLat', PickupLat? PickupLat : '')
    .set('PickupLng', PickupLng? PickupLng : '')
    .set('DropoffLat', DropoffLat? DropoffLat : '')
    .set('DropoffLng', DropoffLng? DropoffLng : '')

    return this.httpClient.get(AppSettings.API_ENDPOINT + `Shared/GetCarTypesWithPrices`, { params })
    .pipe(
      map((results: CarType[]) => {
        return results;
      }),
      map(results=>{
        return results.map((item,index)=>{
          item.CarTypeCatId = item.CarTypeCatId + '';
          switch(item.CarTypeCatId){
            case '1':
                item.CarTypeCatName = 'Business Class';
              break;
              case '2':
                  item.CarTypeCatName = 'Business Van/SUV';
                break;
              case '3':
                  item.CarTypeCatName = 'First Class';
                break;
          }
          return item;
        });
      })
    )
  }
  
  public createOrUpdateCarType(data: CarType)
  {
    return this.httpClient
    .post(AppSettings.API_ENDPOINT + `Manager/SubmitCarType`,data, { responseType:'text'})
    .pipe(
      map((results:string) => {
        return results;
      }));
  }

  public createOrUpdateCountry(data: GsCountry)
  {
    return this.httpClient
    .post(AppSettings.API_ENDPOINT + `Manager/SubmitCountry`,data, { responseType:'text'})
    .pipe(
      map((results:string) => {
        return results;
      }));
  }
  public createOrUpdateUniv(data: Univ)
  {
    return this.httpClient
    .post(AppSettings.API_ENDPOINT + `Manager/SubmitUniv`,data, { responseType:'text'})
    .pipe(
      map((results:string) => {
        return results;
      }));
  }
  public deleteUniv(data: Univ)
  {
    return this.httpClient
    .post(AppSettings.API_ENDPOINT + `Manager/DeleteUniv`,data, { responseType:'text'})
    .pipe(
      map((results:string) => {
        return results;
      }));
  }
  public createOrUpdateQuestion(data: Question)
  {
    return this.httpClient
    .post(AppSettings.API_ENDPOINT + `Manager/SubmitQuestion`,data, { responseType:'text'})
    .pipe(
      map((results:string) => {
        return results;
      }));
  }
  public deleteQuestion(data: Question)
  {
    return this.httpClient
    .post(AppSettings.API_ENDPOINT + `Manager/DeleteQuestion`,data, { responseType:'text'})
    .pipe(
      map((results:string) => {
        return results;
      }));
  }
  public createOrUpdateTour(data: Tour)
  {
    return this.httpClient
    .post(AppSettings.API_ENDPOINT + `Manager/SubmitTour`,data, { responseType:'text'})
    .pipe(
      map((results:string) => {
        return results;
      }));
  }
}
