import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map, catchError  } from 'rxjs/operators';
import { AppSettings } from './appsettings';
import { ParametersEncoder } from '../shared/utils/parameters-encoder';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private httpClient: HttpClient) { }
  public getById(id: number){}
  
  public facebookLogin(token:string){
    return this.httpClient.get(AppSettings.ACCOUNTS_ENDPOINT + `FacebookLogin?token=${token}`)
    .pipe(
      map((result: string) => {
        return result;
      }));
  }
  public getAuthentication(companyId:string,username:string,password:string){
    return this.httpClient.get(AppSettings.API_ENDPOINT + `GetAuthentication?LangID=en-GB&Company_ID=${companyId}&Username=${username}&Password=${password}`)
    .pipe(
      map((result: User) => {
        return result;
      }));
  }
  public getAll()
  {
    return this.httpClient.get(AppSettings.API_ENDPOINT + 'GetUsers?LangID=en-GB&Company_ID=')
    .pipe(
      map((results: User[]) => {
        return results;
      })
    )
  }
  public getMobileCode(phoneCode:string,mobile:string)
  {
    const params = new HttpParams({encoder: new ParametersEncoder()})
    .set('PhoneCode', phoneCode)
    .set('Mobile', mobile)

    return this.httpClient.get(AppSettings.API_ENDPOINT + `Client/GetMobileCode`, {params, responseType:'text'})
    .pipe(
      map((results:string) => {
        return results;
      }));
  }
}
