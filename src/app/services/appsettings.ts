
export class AppSettings {
    public static API_ENDPOINT='http://dfapi.clickersoft.com/api/';
    public static BASE_ENDPOINT='http://dfapi.clickersoft.com/';
    public static BASE_IMAGES = 'http://dfapi.clickersoft.com/images/';
    public static ACCOUNTS_ENDPOINT = 'http://dfaccounts.clickersoft.com/Account/';
    public static DATE_FORMAT = "DD/MM/YYYY"
    public static DATETIME_FORMAT = "DD/MM/YYYY HH:mm"
    public static DATETIME_FORMAT2 = "DD/MM/YYYY HH"
    public static DATE_MAX_DATE_FORMAT = "YYYY-DD-MM"
    public static DATE_ISO_FORMAT = "YYYY-MM-DDTHH:mm:ss[Z]"
    public static DATE_ISO_FORMAT2 = "YYYY-MM-DDTHH:mm:ss"
 }