import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map, catchError  } from 'rxjs/operators';
import { AppSettings } from './appsettings';
import { LocalStorageService } from '../auth/local-storage.service';
import * as moment from 'moment';
import { ParametersEncoder } from '../shared/utils/parameters-encoder';
import { RideStatuss } from '../shared/enums/ride-statuss.enum';
import { SortModes } from '../shared/enums/sort-modes.enum';
import { SortDirections } from '../shared/enums/sort-directions.enum';
import { Ride } from '../shared/models/ride';
import { ClientRide } from '../shared/models/client-ride';

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  },
  green: {
    primary: '#10dc60',
    secondary: '#0ec254'
  },
  gray: {
    primary: '#86888f',
    secondary: '#a2a4ab'
  },
  purple: {
    primary: '#7044ff',
    secondary: '#633ce0'
  },
  blueSky: {
    primary: '#0cd1e8',
    secondary: '#0bb8cc'
  },
};


@Injectable({
  providedIn: 'root'
})
export class RidesService {
  constructor(private httpClient: HttpClient,private localStorageService:LocalStorageService) { }

  public GetRide(id: string)
  {
    const params = new HttpParams({encoder: new ParametersEncoder()})
    .set('RideID',id)
    
    return this.httpClient.get(AppSettings.API_ENDPOINT + `Manager/GetRide`, { params })
    .pipe(
      map((item: Ride) => {
        item.CarTypeId = item.CarTypeId + '';
        item.RideStatusId = item.RideStatusId + '';
        //item.CarTypeId = item.CarTypeId + '';
        //if(item.CreateDate){
        //  item.CreateDate = moment(item.CreateDate).format(AppSettings.DATE_FORMAT);
        //}
        return item;
      })      
    )
  }
  
  public MyRides()
  {
   
    return this.httpClient
    .get(AppSettings.API_ENDPOINT + `Client/MyRides`)
    .pipe(
      map((results: ClientRide[]) => {
        return results.map((item,index)=>{
          item.PickupCityId = item.PickupCityId + '';
          item.RideStatusId = item.RideStatusId + '';
          if(item.Tour){
            item.Tour.PaymentMethod = item.Tour.PaymentMethod + '';
            item.Tour.TourType = item.Tour.TourType + '';
            item.Tour.TourCategory = item.Tour.TourCategory + '';
          }
          if(item.RideDate){
            item.RideDate = moment(item.RideDate).format(AppSettings.DATE_FORMAT);
            //item.dayName = moment(item.RideDate).locale('ar').format('dddd');
            }
          return item;
        });
      })
    )
  }
  public CaptureOrder(id:string)
  {
    const params = new HttpParams({encoder: new ParametersEncoder()})
    .set('OrderId',id)

    return this.httpClient
    .get(AppSettings.API_ENDPOINT + `Client/CaptureOrderAsync`, { params })
    .pipe(
      map((item: string) => {
        return item;
      })      
    )
  }
  public MyRide(id:string)
  {
    const params = new HttpParams({encoder: new ParametersEncoder()})
    .set('RideID',id)

    return this.httpClient
    .get(AppSettings.API_ENDPOINT + `Client/MyRide`, { params })
    .pipe(
      map((item: ClientRide) => {
        item.PickupCityId = item.PickupCityId + '';
        item.RideStatusId = item.RideStatusId + '';
        if(item.Tour){
          item.Tour.PaymentMethod = item.Tour.PaymentMethod + '';
          item.Tour.TourType = item.Tour.TourType + '';
          item.Tour.TourCategory = item.Tour.TourCategory + '';
        }
        if(item.RideDate){
          item.RideDate = moment(item.RideDate).format(AppSettings.DATE_FORMAT);
          //item.dayName = moment(item.RideDate).locale('ar').format('dddd');
          }

        //if(item.CreateDate){
        //  item.CreateDate = moment(item.CreateDate).format(AppSettings.DATE_FORMAT);
        //}
        return item;
      })      
    )
  }
  
  public createOrUpdateRide(ride: Ride)
  {
    ride.RideDate = moment(ride.RideDate).utc().format(AppSettings.DATE_ISO_FORMAT);
    if(ride.PickupTime){
      ride.PickupTime = moment(ride.PickupTime).utc().format(AppSettings.DATE_ISO_FORMAT);
    }
    if(ride.DropoffTime){
      ride.DropoffTime = moment(ride.DropoffTime).utc().format(AppSettings.DATE_ISO_FORMAT);
    }
    return this.httpClient
    .post(AppSettings.API_ENDPOINT + `Manager/SubmitRide`,ride, { responseType:'text'})
    .pipe(
      map((results:string) => {
        return results;
      }));
  }
  public confirmReserve(clientRide: ClientRide)
  {
    //ride.RideDate = moment(ride.RideDate).utc().format(AppSettings.DATE_ISO_FORMAT);
    //if(ride.PickupTime){
    //  ride.PickupTime = moment(ride.PickupTime).utc().format(AppSettings.DATE_ISO_FORMAT);
   // }
   // if(ride.DropoffTime){
   //   ride.DropoffTime = moment(ride.DropoffTime).utc().format(AppSettings.DATE_ISO_FORMAT);
   // }
   if(clientRide.RideDate){
    clientRide.RideDate = moment(clientRide.RideDate).utc().format(AppSettings.DATE_ISO_FORMAT)
   }
    
    return this.httpClient
    .post(AppSettings.API_ENDPOINT + `Client/SubmitRide`,clientRide, { responseType:'text'})
    .pipe(
      map((results:string) => {
        return results;
      }));
  }
 
  public delete(id: string)
  {
    const params = new HttpParams({encoder: new ParametersEncoder()})
    .set('LangID','en-GB')
    .set('AppointmentID',id)

    /*
    .set(
      'primary_release_date.gte',
      format(getStart(this.viewDate), AppSettings.DATE_FORMAT)
    )
    .set(
      'primary_release_date.lte',
      format(getEnd(this.viewDate), AppSettings.DATE_FORMAT)
    )
    .set('api_key', '0ec33936a68018857d727958dca1424f');
*/
    return this.httpClient
    .get(AppSettings.API_ENDPOINT + `DeleteAppointment`, { params ,responseType:'text'})
    .pipe(
      map((results:string) => {
        return results;
      }));
  }
  public sendAppointmentSms(appointmentID: string)
  {
    const params = new HttpParams({encoder: new ParametersEncoder()})
    .set('LangID','en-GB')
    .set('AppointmentID',appointmentID)

    /*
    .set(
      'primary_release_date.gte',
      format(getStart(this.viewDate), AppSettings.DATE_FORMAT)
    )
    .set(
      'primary_release_date.lte',
      format(getEnd(this.viewDate), AppSettings.DATE_FORMAT)
    )
    .set('api_key', '0ec33936a68018857d727958dca1424f');
*/
    return this.httpClient
    .get(AppSettings.API_ENDPOINT + `SendAppointmentSms`, { params ,responseType:'text'})
    .pipe(
      map((results:string) => {
        return results;
      }));
  }
  

  public getById(id: number){}

  public getAll(dateFrom:moment.Moment,dateTo:moment.Moment,rideStatus:RideStatuss,searchText:string,rideTypeId:number,
    tourId:string,carTypeId:number,driverId:string,fromIndex:number,pageSize:number,sortMode:SortModes,sortDirection:SortDirections)
  {
    const params = new HttpParams({encoder: new ParametersEncoder()})
    .set('RideStatus', rideStatus.valueOf().toString())
    .set('SearchText', searchText)
    .set('RideTypeId', rideTypeId.toString())
    .set('TourId', tourId)
    .set('CarTypeId', carTypeId.toString())
    .set('DriverId', driverId)
    .set('FromIndex', fromIndex.toString())
    .set('PageSize', pageSize.toString())
    .set('SortMode', sortMode.valueOf().toString())
    .set('SortDirection', sortDirection.valueOf().toString())
    .set('FromDate',dateFrom ? moment(dateFrom).utc().format(AppSettings.DATE_ISO_FORMAT) : '')
    .set('ToDate', dateTo ? moment(dateTo).utc().format(AppSettings.DATE_ISO_FORMAT) : '')
    
    return this.httpClient
    .get(AppSettings.API_ENDPOINT + `Manager/GetRides`, { params })
    .pipe(
      map((results: Ride[]) => {
        return results.map((item,index)=>{
          item.CarTypeId = item.CarTypeId + '';
          item.RideStatusId = item.RideStatusId + '';
          return item;
        });
      })
    )
  }
/*
  public getAllForCalendar(actions,dateFrom:moment.Moment,dateTo:moment.Moment,rideStatus:RideStatuss,searchText:string,rideTypeId:number,
    tourId:string,carTypeId:number,driverId:string,fromIndex:number,pageSize:number,sortMode:SortModes,sortDirection:SortDirections)
  {
    const params = new HttpParams({encoder: new ParametersEncoder()})
    .set('RideStatus', rideStatus.valueOf().toString())
    .set('SearchText', searchText)
    .set('RideTypeId', rideTypeId.toString())
    .set('TourId', tourId)
    .set('CarTypeId', carTypeId.toString())
    .set('DriverId', driverId)
    .set('FromIndex', fromIndex.toString())
    .set('PageSize', pageSize.toString())
    .set('SortMode', sortMode.valueOf().toString())
    .set('SortDirection', sortDirection.valueOf().toString())
    .set('FromDate',dateFrom ? moment(dateFrom).utc().format(AppSettings.DATE_ISO_FORMAT) : '')
    .set('ToDate', dateTo ? moment(dateTo).utc().format(AppSettings.DATE_ISO_FORMAT) : '')
    
    return this.httpClient
    .get(AppSettings.API_ENDPOINT + `Manager/GetRides`, { params })
    .pipe(
      map((results: Ride[]) => {
        return results.map((ride: Ride) => {
          return {
            title: ride.ClientName + " - " +  ride.ClientMobile,
            start: new Date(ride.RideDate),
            color: this.getColorByStatus(ride.RideStatusId),
            allDay: true,
            draggable: false,
            resizable: {
              beforeStart: false,
              afterEnd: false
            },
            cssClass: this.contrastClass(this.getColorByStatus(ride.RideStatusId).primary),
            meta: {
              ride
            },
          };
        });
      })
    )
  }
*/
  public getColorByStatus(status:string){
    let statusId = Number.parseInt(status)
    switch(statusId){
      case RideStatuss.Assigned.valueOf():
        return colors.blueSky;
      case RideStatuss.CanceledByClient.valueOf():
          return colors.red;
      case RideStatuss.CanceledByManager.valueOf():
          return colors.red;
      case RideStatuss.Created.valueOf():
          return colors.gray;
      case RideStatuss.Paid.valueOf():
          return colors.blue;
      case RideStatuss.Pickup.valueOf():
          return colors.purple;
      case RideStatuss.Dropoff.valueOf():
          return colors.green;
    }
    return colors.gray;
  }

  public setAppointmentTime(date: Date, timeStr: string): Date {
    if (timeStr) {
      var time = moment(timeStr, 'HH:mm:ss');
      //console.log(time.hours() + ":" + time.minutes());
      date.setHours(time.hours(), time.minutes(), 0, 0);
    }
    return date;
  }

  public contrastColor(hex: string) : string
  {
    if(!hex){
      return '#454545';
    }
    hex = hex.replace('#','');
    let red = parseInt(hex.substring(0,2), 16);
    let green = parseInt(hex.substring(2,4), 16);
    let blue = parseInt(hex.substring(4,6), 16);

      if ((red*0.299) + (green*0.587) + (blue*0.114) > 186)
      {
          //dark color
          console.log('black');
          //Return white
         // return 'black-color';
          return '#454545';
          //return (SolidColorBrush)new BrushConverter().ConvertFromString("#");
      }
      else
      {
          //light color
          console.log('light');
          //Return black
         // return 'white-color';
         return '#f1f1f1';
         // return (SolidColorBrush)new BrushConverter().ConvertFromString("#f1f1f1");
         
      }
  }
  public contrastClass(hex: string) : string
  {
    if(!hex){
      return '';
    }
    hex = hex.replace('#','');
    let red = parseInt(hex.substring(0,2), 16);
    let green = parseInt(hex.substring(2,4), 16);
    let blue = parseInt(hex.substring(4,6), 16);

      if ((red*0.299) + (green*0.587) + (blue*0.114) > 186)
      {
          //dark color
          console.log('black');
          //Return white
          return 'black-app';
          //return (SolidColorBrush)new BrushConverter().ConvertFromString("#");
      }
      else
      {
          //light color
          console.log('light');
          //Return black
          return 'white-app';
         // return (SolidColorBrush)new BrushConverter().ConvertFromString("#f1f1f1");
         
      }
  }
}
