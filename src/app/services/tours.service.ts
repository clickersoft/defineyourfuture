import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map, catchError  } from 'rxjs/operators';
import { AppSettings } from './appsettings';
import { LocalStorageService } from '../auth/local-storage.service';
import * as moment from 'moment';
import { ParametersEncoder } from '../shared/utils/parameters-encoder';
import { Account } from '../shared/models/account';
import { SortModes } from '../shared/enums/sort-modes.enum';
import { SortDirections } from '../shared/enums/sort-directions.enum';
import { Tour } from '../shared/models/tour';
import { TourAttachment } from '../shared/models/tour-attachment';
import { TourPickup } from '../shared/models/tour-pickup';
import { Univ } from '../shared/models/univ';
import { Question } from '../shared/models/question';

@Injectable({
  providedIn: 'root'
})
export class ToursService {

  constructor(private httpClient: HttpClient,private localStorageService:LocalStorageService) { }

  public GetTours(searchTxt: string,fromDate: string,toDate: string,active: boolean,fromIndex =0, pageSize = 9,sortMode = SortModes.ByDefault, sortDirection = SortDirections.Descending)
  {
    const params = new HttpParams({encoder: new ParametersEncoder()})
    .set('SearchText', searchTxt)
    .set('FromDate',fromDate ? moment(fromDate).utc().format(AppSettings.DATE_ISO_FORMAT) : '')
    .set('ToDate', toDate ? moment(toDate).utc().format(AppSettings.DATE_ISO_FORMAT) : '')
    .set('Active', active.toString())
    .set('FromIndex', fromIndex.toString())
    .set('PageSize', pageSize.toString())
    .set('SortMode', sortMode.valueOf().toString())
    .set('SortDirection', sortDirection.valueOf().toString())

    return this.httpClient.get(AppSettings.API_ENDPOINT + `Manager/GetTours`, { params })
    .pipe(
      map((results: Tour[]) => {
        return results;
      }),
      map(results=>{
        return results.map((item,index)=>{
          //item.CarTypeId = item.CarTypeId + '';
          item.TourType = item.TourType + '';
          item.TourPlace = item.TourPlace + '';
          item.TourCategory = item.TourCategory + '';
          item.PaymentMethod = item.PaymentMethod + '';
          item.TourPickup2 = [];
          item.RequiredFields2 = [];
          item.Universities2 = [];
        if(item.TourPickup){
          item.TourPickup.forEach(element => {
            item.TourPickup2.push(element.GsCityId+ '')
          });
        }
        if(item.RequiredFields){
          item.RequiredFields.split(',').forEach(element => {
            item.RequiredFields2.push(element)
          });
        }
        if(item.Universities){
          item.Universities.split(',').forEach(element => {
            item.Universities2.push(element)
          });
        }
          return item;
        });
      })
    )
  }
  public GetUniv(tourPlace: string)
  {
    const params = new HttpParams({encoder: new ParametersEncoder()})
    .set('tourPlace', tourPlace + '')

    
    return this.httpClient.get(AppSettings.API_ENDPOINT + `Client/GetUniv`, { params })
    .pipe(
      map((results: Univ[]) => {
        return results;
      }),
      map(results=>{
        return results.map((item,index)=>{
          item.Place = item.Place + '';
       
          return item;
        });
      })
    )
  }
  public GetQuestions()
  {
    const params = new HttpParams({encoder: new ParametersEncoder()})

    
    return this.httpClient.get(AppSettings.API_ENDPOINT + `Client/GetQuestions`, { params })
    .pipe(
      map((results: Question[]) => {
        return results;
      }),
      map(results=>{
        return results.map((item,index)=>{
       
          return item;
        });
      })
    )
  }
  public GetClientTours(tourType: number,tourPlace: string)
  {
    const params = new HttpParams({encoder: new ParametersEncoder()})
    .set('tourType', tourType + '')
    .set('tourPlace', tourPlace + '')

    
    return this.httpClient.get(AppSettings.API_ENDPOINT + `Client/GetTours`, { params })
    .pipe(
      map((results: Tour[]) => {
        return results;
      }),
      map(results=>{
        return results.map((item,index)=>{
          item.TourType = item.TourType + '';
          item.TourCategory = item.TourCategory + '';
          item.PaymentMethod = item.PaymentMethod + '';
          if(item.TourDate){
          item.TourDate = moment(item.TourDate).format(AppSettings.DATE_FORMAT);
          item.dayName = moment(item.TourDate).locale('en').format('dddd');
          }
          if(item.TourFeature){
            item.TourFeature.forEach(element => {
              element.rideAccountCount = 0;
            });
          }
          return item;
        });
      })
    )
  }
  public GetTour(id: string)
  {
    const params = new HttpParams({encoder: new ParametersEncoder()})
    .set('TourID',id)
    
    return this.httpClient.get(AppSettings.API_ENDPOINT + `Manager/GetTour`, { params })
    .pipe(
      map((item: Tour) => {
        item.TourType = item.TourType + '';
        item.TourPlace = item.TourPlace + '';
        item.TourCategory = item.TourCategory + '';
        item.PaymentMethod = item.PaymentMethod + '';
        item.TourPickup2 = [];
        item.RequiredFields2 = [];
        item.Universities2 = [];
        if(item.TourPickup){
          item.TourPickup.forEach(element => {
            item.TourPickup2.push(element.GsCityId + '')
          });
        }
        if(item.RequiredFields){
          item.RequiredFields.split(',').forEach(element => {
            item.RequiredFields2.push(element)
          });
        }
        if(item.Universities){
          item.Universities.split(',').forEach(element => {
            item.Universities2.push(element)
          });
        }
        //item.AccountTypeId = item.AccountTypeId + '';
        //if(item.CreateDate){
        //  item.CreateDate = moment(item.CreateDate).format(AppSettings.DATE_FORMAT);
        //}
        return item;
      })      
    )
  }
  public GetClientTour(id: string)
  {
    const params = new HttpParams({encoder: new ParametersEncoder()})
    .set('TourID',id)
    
    return this.httpClient.get(AppSettings.API_ENDPOINT + `Client/GetTour`, { params })
    .pipe(
      map((item: Tour) => {
        if(item.TourDate){
          item.TourDate = moment(item.TourDate).format(AppSettings.DATE_FORMAT);
          item.dayName = moment(item.TourDate).locale('en').format('dddd');
          }
          if(item.EndDate){
            item.EndDate = moment(item.EndDate).format(AppSettings.DATE_FORMAT);
            item.EndDate = moment(item.EndDate).hours(12).format(AppSettings.DATE_ISO_FORMAT2)
            }
            if(item.TourFeature){
              item.TourFeature.forEach(element => {
                element.rideAccountCount = 0;
              });
            }
            
        //item.AccountTypeId = item.AccountTypeId + '';
        //if(item.CreateDate){
        //  item.CreateDate = moment(item.CreateDate).format(AppSettings.DATE_FORMAT);
        //}
        return item;
      })      
    )
  }
  public GetTourImages(imageGroup:string)
  {
    return this.httpClient.get(AppSettings.API_ENDPOINT + `Manager/GetTourImages?imageGroup=${imageGroup}`,)
    .pipe(
      map((results: TourAttachment[]) => {
        return results;
      }),
      map(results=>{
        return results.map((item,index)=>{
          console.log(item);
          return item;
        });
      })
    )
  }
  public createOrUpdateTour(tour: Tour)
  {
    tour.PublishDate = moment(tour.PublishDate).utc().format(AppSettings.DATE_ISO_FORMAT);
    if(tour.EndDate){
      tour.EndDate = moment(tour.EndDate).utc().format(AppSettings.DATE_ISO_FORMAT);
    }
    if(tour.TourDate){
      tour.TourDate = moment(tour.TourDate).utc().format(AppSettings.DATE_ISO_FORMAT);
    }
    if(tour.RequiredFields2){
      tour.RequiredFields = tour.RequiredFields2.join();
    }  
    if(tour.Universities2){
      tour.Universities = tour.Universities2.join();
    }
    return this.httpClient
    .post(AppSettings.API_ENDPOINT + `Manager/SubmitTour`,tour, { responseType:'text'})
    .pipe(
      map((results:string) => {
        return results;
      }));
  }
  public uploadTourImages(files: FormData)
  {
    return this.httpClient
    .post(AppSettings.API_ENDPOINT + `Manager/UploadTourImages`,FormData, { responseType:'text',headers: {
      "Content-Type": "multipart/form-data"
  }})
    .pipe(
      map((results:string) => {
        return results;
      }));
  }
  public uploadTourImages2(files: FormData)
  {
    return this.httpClient
    .post(AppSettings.API_ENDPOINT + `Manager/UploadTourImages2`,FormData, { responseType:'text',headers: {
      "Content-Type": "application/x-www-form-urlencoded"
  }})
    .pipe(
      map((results:string) => {
        return results;
      }));
  }
  public uploadTourImages3(files: FormData)
  {
    return this.httpClient
    .post(AppSettings.API_ENDPOINT + `Manager/UploadTourImages3`,FormData, { responseType:'text',headers: {
      "Content-Type": "multipart/form-data"
  }})
    .pipe(
      map((results:string) => {
        return results;
      }));
  }

  public _uploadTourImages(files: FormData)
  {
    return this.httpClient
    .post(AppSettings.BASE_ENDPOINT + `Home/UploadTourImages`,FormData, { responseType:'text',headers: {
      "Content-Type": "multipart/form-data"
  }})
    .pipe(
      map((results:string) => {
        return results;
      }));
  }
  public _uploadTourImages2(files: FormData)
  {
    return this.httpClient
    .post(AppSettings.BASE_ENDPOINT + `Home/UploadTourImages2`,FormData, { responseType:'text',headers: {
      "Content-Type": "application/x-www-form-urlencoded"
  }})
    .pipe(
      map((results:string) => {
        return results;
      }));
  }
  public _uploadTourImages3(files: FormData)
  {
    return this.httpClient
    .post(AppSettings.BASE_ENDPOINT + `Home/UploadTourImages3`,FormData, { responseType:'text',headers: {
      "Content-Type": "multipart/form-data"
  }})
    .pipe(
      map((results:string) => {
        return results;
      }));
  }
  public uploadTourImages4(formData : FormData){
    return this.httpClient.post(AppSettings.API_ENDPOINT + 'Manager/UploadTourImages2', formData, {reportProgress: true, observe: 'events'})
  }
  public uploadTourPrimaryImage(formData : FormData){
    return this.httpClient.post(AppSettings.API_ENDPOINT + 'Manager/UploadTourPrimaryImage', formData, {responseType:'text',reportProgress: true, observe: 'events'})
  }
  //application/x-www-form-urlencoded
  /*
  public createOrUpdateComplaint(accountComplaint: AccountComplaint)
  {
    const params = new HttpParams({encoder: new ParametersEncoder()})
    .set('LangID','en-GB')
    .set('Username',this.localStorageService.getClinic().User_Name)
    .set('Password',this.localStorageService.getClinic().Password)
    .set('Company_ID',this.localStorageService.getClinic().Company_ID)

    .set('AccountComplaintID',accountComplaint.Account_Complaint_ID || '')
    .set('AccountID',accountComplaint.Account_ID || '')
    .set('Account_Treatment_Plan_ID',accountComplaint.Account_Treatment_Plan_ID != null ? accountComplaint.Account_Treatment_Plan_ID.toString() : '')
    .set('Doctor_ID',accountComplaint.Doctor_ID || this.localStorageService.getClinic().User_ID)
    .set('Complaint',accountComplaint.Complaint || '')
    .set('Complaint_Date',accountComplaint.Complaint_Date ? moment(accountComplaint.Complaint_Date).utc().format(AppSettings.DATE_ISO_FORMAT) : '')

    return this.httpClient
    .get(AppSettings.API_ENDPOINT + `SubmitAccountComplaint`, { params ,responseType:'text'})
    .pipe(
      map((results:string) => {
        return results;
      }));
  }

  public deleteComplaint(id: string)
  {
    const params = new HttpParams({encoder: new ParametersEncoder()})
    .set('LangID','en-GB')
    .set('Username',this.localStorageService.getClinic().User_Name)
    .set('Password',this.localStorageService.getClinic().Password)
    .set('Company_ID',this.localStorageService.getClinic().Company_ID)

    .set('AccountComplaintID',id)

    return this.httpClient
    .get(AppSettings.API_ENDPOINT + `DeleteAccountComplaint`, { params ,responseType:'text'})
    .pipe(
      map((results:string) => {
        return results;
      }));
  }
*/
  public GetAccount(id: string)
  {
    const params = new HttpParams({encoder: new ParametersEncoder()})
    .set('AccountID',id)
    
    return this.httpClient.get(AppSettings.API_ENDPOINT + `Manager/GetAccount`, { params })
    .pipe(
      map((item: Account) => {
        item.AccountTypeId = item.AccountTypeId + '';
        item.GsCountryId = item.GsCountryId + '';
        item.CarTypeId = item.CarTypeId + '';
        //if(item.CreateDate){
        //  item.CreateDate = moment(item.CreateDate).format(AppSettings.DATE_FORMAT);
        //}
        return item;
      })      
    )
  }
}
