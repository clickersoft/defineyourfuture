import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NavExtrasService {

  static extras: any;
  static extras2: any;

  constructor() { }

  public setExtras(data){
    NavExtrasService.extras = data;
  }
  public setExtras2(data){
    NavExtrasService.extras2 = data;
  }
  public getExtras(){
    return NavExtrasService.extras;
  }
  public getExtras2(){
    return NavExtrasService.extras2;
  }
}
