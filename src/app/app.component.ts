import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { OAuthService,JwksValidationHandler, OAuthStorage } from 'angular-oauth2-oidc';
import { DashboardPage } from './dashboard/dashboard.page';
import { authConfig } from './auth/auth.config';
import { map, filter, scan } from 'rxjs/operators';
import { LoginAdminPage } from './login-admin/login-admin.page';
import { registerWebPlugin } from '@capacitor/core';
import { FacebookLogin } from '@rdlabo/capacitor-facebook-login';
import "@codetrix-studio/capacitor-google-auth";
import { LocalStorageService } from './auth/local-storage.service';

import { Plugins } from '@capacitor/core';
import { LanguageService } from './services/language.service';
const { Storage } = Plugins;

export class MyOAuthStorage extends OAuthStorage{

  constructor(private localStorageService: LocalStorageService){
    super();
  }
  getItem(key: string): string {
    console.log('getItem:' +key);
    return this.localStorageService.getItem(key);
  }  
  removeItem(key: string): void {
    console.log('removeItem:' +key);
    Storage.remove({key : 'access_token'});
    this.localStorageService.removeItem(key)
  }
  setItem(key: string, data: string): void {
    this.localStorageService.setItem(key,data)
    
    Storage.set({ key: 'access_token', value: this.localStorageService.getItem('access_token')});
    Storage.set({ key: 'id_token', value: this.localStorageService.getItem('id_token')});
    Storage.set({ key: 'refresh_token', value: this.localStorageService.getItem('refresh_token')});
    Storage.set({ key: 'nonce', value: this.localStorageService.getItem('nonce')});
    Storage.set({ key: 'expires_at', value: this.localStorageService.getItem('expires_at')});
    Storage.set({ key: 'id_token_claims_obj', value: this.localStorageService.getItem('id_token_claims_obj')});
    Storage.set({ key: 'id_token_expires_at', value: this.localStorageService.getItem('id_token_expires_at')});
    Storage.set({ key: 'id_token_stored_at', value: this.localStorageService.getItem('id_token_stored_at')});
    Storage.set({ key: 'access_token_stored_at', value: this.localStorageService.getItem('access_token_stored_at')});
    Storage.set({ key: 'granted_scopes', value: this.localStorageService.getItem('granted_scopes')});
    Storage.set({ key: 'session_state', value: this.localStorageService.getItem('session_state')});
    console.log('setItem:' + key);
  }


}
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  rootPage: any = DashboardPage;
 
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private oauthService: OAuthService,
    private localStorageService: LocalStorageService,
    private languageService: LanguageService
  ) {
   // registerWebPlugin(FacebookLogin);
   this.platform.ready().then(() => {
     this.statusBar.hide();
    // this.splashScreen.hide();
    this.languageService.setInitialAppLanguage();
   });
    this.initializeApp();
  }

  initializeApp() {
    //console.log("initializeApp");
    /*access_token
    id_token
    refresh_token
    nonce
    expires_at
    id_token_claims_obj
    id_token_expires_at
    id_token_stored_at
    access_token_stored_at
    granted_scopes
    session_state
    */
    Storage.get({key:"oAuth"}).then((res)=>{
      if(res){
        let oauth = JSON.parse(res.value);
        if(oauth){
          this.localStorageService.setItem('access_token',oauth.access_token);
          this.localStorageService.setItem('id_token',oauth.id_token);
          this.localStorageService.setItem('refresh_token',oauth.refresh_token);
          this.localStorageService.setItem('nonce',oauth.nonce);
          this.localStorageService.setItem('expires_at',oauth.expires_at);
          this.localStorageService.setItem('id_token_claims_obj',oauth.id_token_claims_obj);
          this.localStorageService.setItem('id_token_expires_at',oauth.id_token_expires_at);
          this.localStorageService.setItem('id_token_stored_at',oauth.id_token_stored_at);
          this.localStorageService.setItem('access_token_stored_at',oauth.access_token_stored_at);
          this.localStorageService.setItem('granted_scopes',oauth.granted_scopes);
          this.localStorageService.setItem('session_state',oauth.session_state);
        }
      }
      this.oauthService.configure(authConfig);
      this.oauthService.setStorage(new MyOAuthStorage(this.localStorageService))
      //this.oauthService.setupAutomaticSilentRefresh();
      this.oauthService.tokenValidationHandler = new JwksValidationHandler();
      this.oauthService.loadDiscoveryDocument();
  
      this.oauthService.events.pipe(filter(e => e.type === 'token_expires'))
      .subscribe(e => {
        console.log("token_expires");
        this.oauthService.refreshToken().then();
      })
  
      if (this.oauthService.hasValidIdToken()) {
        this.rootPage = DashboardPage;
      } else {
        this.rootPage = LoginAdminPage;
      }
    });
  

    /*
    this.oauthService.refreshToken().then(() => {
          console.debug('ok');
    })
*/
    
  }
}
